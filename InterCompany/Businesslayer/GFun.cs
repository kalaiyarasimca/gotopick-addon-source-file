﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OrderTracking.BussinessLayer;
using SAPbouiCOM;
using System.IO;
using System.Net;
using System.Xml;
using System.Windows.Forms;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
//using Sap.Data.Hana;
using System.Data;
using System.Globalization;

namespace OrderTracking.Bussiness_Layer
{
    class GFun
    {
        public static System.Data.DataTable DeleteColumn(string KeepColumns, System.Data.DataTable dt)
        {
            System.Data.DataTable dt1 = dt.Copy();
            foreach (System.Data.DataColumn col in dt1.Columns)
            {
                if (!KeepColumns.Contains(col.ColumnName))
                {
                    dt.Columns.Remove(col.ColumnName);
                }
            }
            return dt;
        }
        public static String getSingleValue(String strSQL)
        {
            try
            {
                SAPbobsCOM.Recordset rset = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                string strReturnVal = "";
                rset.DoQuery(strSQL);
                if (rset.RecordCount > 0)
                {
                    strReturnVal = rset.Fields.Item(0).Value.ToString();
                }
                else
                {
                    strReturnVal = "";
                }
                return strReturnVal;

            }
            catch
            {
                return null;
            }

        }

        public static String getSingleValue1(String strSQL)
        {
            try
            {
                SAPbobsCOM.Recordset rset = Global.oCompany1.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                string strReturnVal = "";
                rset.DoQuery(strSQL);
                if (rset.RecordCount > 0)
                {
                    strReturnVal = rset.Fields.Item(0).Value.ToString();
                }
                else
                {
                    strReturnVal = "";
                }
                return strReturnVal;

            }
            catch
            {
                return null;
            }
        }
        public static String getSingleValue2(String strSQL)
        {
            try
            {
                SAPbobsCOM.Recordset rset = Global.oCompany2.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                string strReturnVal = "";
                rset.DoQuery(strSQL);
                if (rset.RecordCount > 0)
                {
                    strReturnVal = rset.Fields.Item(0).Value.ToString();
                }
                else
                {
                    strReturnVal = "";
                }
                return strReturnVal;

            }
            catch
            {
                return null;
            }

        }
        #region Xml
        public static string Generate2XML(string docEntry, string DocType, string xmlstr)
        {
            DataSet ds = new DataSet();
            ds = xmlStr2Ds(xmlstr);
            switch (DocType)
            {
                case "2":
                    if (ds.Tables.Contains("CRD2"))
                    {
                        ds.Tables.Remove("CRD2");
                    }
                    if (ds.Tables.Contains("CRD1"))
                    {

                    }
                    if (ds.Tables.Contains("CRD7"))
                    {

                    }
                    break;
                case "4":
                    if (ds.Tables.Contains("ITM1"))
                    {
                        ds.Tables.Remove("ITM1");
                    }
                    if (ds.Tables.Contains("OITW"))
                    {
                        ds.Tables.Remove("OITW");
                    }
                    break;
                case "1":
                    if (ds.Tables.Contains("OACT"))
                    {

                    }
                    break;
            }
            xmlstr = ToXMLStringFromDS(DocType, ds);
            return xmlstr;
        }
        public static string ToXMLStringFromDS(string ObjType, DataSet ds, bool GetFieldEmpty = false)
        {
            try
            {
                //Dim gf As New GeneralFunctions()
                StringBuilder XmlString = new StringBuilder();
                XmlWriter writer = XmlWriter.Create(XmlString);
                writer.WriteStartDocument();
                if (true)
                {
                    writer.WriteStartElement("BOM");
                    if (true)
                    {
                        writer.WriteStartElement("BO");
                        if (true)
                        {
                            //#Region "write ADMINFO_ELEMENT"
                            writer.WriteStartElement("AdmInfo");
                            if (true)
                            {
                                writer.WriteStartElement("Object");
                                if (true)
                                {
                                    writer.WriteString(ObjType);
                                }
                                writer.WriteEndElement();
                            }
                            writer.WriteEndElement();
                            //#End Region

                            //#Region "Header&Line XML"
                            foreach (System.Data.DataTable dt in ds.Tables)
                            {
                                if (dt.Rows.Count > 0)
                                {
                                    writer.WriteStartElement(dt.TableName.ToString(CultureInfo.InvariantCulture));
                                    if (true)
                                    {
                                        foreach (DataRow row in dt.Rows)
                                        {
                                            writer.WriteStartElement("row");
                                            if (true)
                                            {
                                                foreach (System.Data.DataColumn column in dt.Columns)
                                                {
                                                    if (column.DefaultValue.ToString() != "xx_remove_xx")
                                                    {
                                                        //Force datetime format follow SQL 
                                                        if (object.ReferenceEquals(column.DataType, typeof(DateTime)) & (row[column]) != System.DBNull.Value)
                                                        {
                                                            DateTime dateTime = default(DateTime);
                                                            dateTime = Convert.ToDateTime(row[column]);
                                                            if (GetFieldEmpty)
                                                            {
                                                                writer.WriteStartElement(column.ColumnName);
                                                                writer.WriteString((string.IsNullOrEmpty(dateTime.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)) ? "" : dateTime.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)));
                                                                writer.WriteEndElement();
                                                            }
                                                            else
                                                            {
                                                                if (!string.IsNullOrEmpty(row[column].ToString()))
                                                                {
                                                                    writer.WriteStartElement(column.ColumnName);
                                                                    writer.WriteString(row[column].ToString());
                                                                    writer.WriteEndElement();
                                                                }
                                                            }
                                                        }
                                                        else if (object.ReferenceEquals(column.DataType, typeof(System.DateTime)) & !string.IsNullOrEmpty(row[column].ToString()))
                                                        {
                                                            System.DateTime d = default(System.DateTime);
                                                            d = Convert.ToDateTime(row[column]);

                                                            if (GetFieldEmpty)
                                                            {
                                                                writer.WriteStartElement(column.ColumnName);
                                                                writer.WriteString((string.IsNullOrEmpty(d.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)) ? "" : d.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)));
                                                                writer.WriteEndElement();
                                                            }
                                                            else
                                                            {
                                                                if (!string.IsNullOrEmpty(row[column].ToString()))
                                                                {
                                                                    writer.WriteStartElement(column.ColumnName);
                                                                    writer.WriteString(row[column].ToString());
                                                                    writer.WriteEndElement();
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            //Write Tag
                                                            if (GetFieldEmpty)
                                                            {
                                                                writer.WriteStartElement(column.ColumnName);
                                                                //writer.WriteString(string.IsNullOrEmpty((row[column].ToString()) ? "" : row[column].ToString()));
                                                                writer.WriteString((string.IsNullOrEmpty(row[column].ToString()) ? "" : row[column].ToString()));
                                                                writer.WriteEndElement();
                                                            }
                                                            else
                                                            {
                                                                if (!string.IsNullOrEmpty(row[column].ToString()))
                                                                {
                                                                    writer.WriteStartElement(column.ColumnName);
                                                                    writer.WriteString(row[column].ToString());
                                                                    writer.WriteEndElement();
                                                                }
                                                                //if (dt.TableName.ToString() == "CRD7")
                                                                //{
                                                                //    if (column.ColumnName == "Address" & string.IsNullOrEmpty(row[column].ToString()))
                                                                //    {
                                                                //        writer.WriteStartElement(column.ColumnName);
                                                                //        writer.WriteString(row[column].ToString());
                                                                //        writer.WriteEndElement();
                                                                //    }
                                                                //}
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            writer.WriteEndElement();
                                        }
                                    }
                                    writer.WriteEndElement();
                                }
                            }
                            //#End Region
                        }
                        writer.WriteEndElement();
                    }
                    writer.WriteEndElement();
                }
                writer.WriteEndDocument();

                writer.Flush();

                return XmlString.ToString();
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public static string ToXMLStringFromDS(string DocEntry, string CType, string ObjType, DataSet ds, bool GetFieldEmpty = false)
        {
            try
            {
                //Dim gf As New GeneralFunctions()
                StringBuilder XmlString = new StringBuilder();
                XmlWriter writer = XmlWriter.Create(XmlString);
                writer.WriteStartDocument();
                if (true)
                {
                    writer.WriteStartElement("BOM");
                    if (true)
                    {
                        writer.WriteStartElement("BO");
                        if (true)
                        {
                            //#Region "write ADMINFO_ELEMENT"
                            writer.WriteStartElement("AdmInfo");
                            if (true)
                            {
                                writer.WriteStartElement("Object");
                                if (true)
                                {
                                    writer.WriteString(ObjType);
                                }
                                writer.WriteEndElement();
                            }
                            writer.WriteEndElement();
                            //#End Region

                            //#Region "Header&Line XML"
                            foreach (System.Data.DataTable dt in ds.Tables)
                            {
                                if (dt.Rows.Count > 0)
                                {
                                    writer.WriteStartElement(dt.TableName.ToString(CultureInfo.InvariantCulture));
                                    if (true)
                                    {
                                        foreach (DataRow row in dt.Rows)
                                        {
                                            writer.WriteStartElement("row");
                                            if (true)
                                            {
                                                foreach (System.Data.DataColumn column in dt.Columns)
                                                {
                                                    if (column.DefaultValue.ToString() != "xx_remove_xx")
                                                    {
                                                        //Force datetime format follow SQL 
                                                        if (object.ReferenceEquals(column.DataType, typeof(DateTime)) & (row[column]) != System.DBNull.Value)
                                                        {
                                                            DateTime dateTime = default(DateTime);
                                                            dateTime = Convert.ToDateTime(row[column]);
                                                            if (GetFieldEmpty)
                                                            {
                                                                writer.WriteStartElement(column.ColumnName);
                                                                writer.WriteString((string.IsNullOrEmpty(dateTime.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)) ? "" : dateTime.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)));
                                                                writer.WriteEndElement();
                                                            }
                                                            else
                                                            {
                                                                if (!string.IsNullOrEmpty(row[column].ToString()))
                                                                {
                                                                    writer.WriteStartElement(column.ColumnName);
                                                                    writer.WriteString(row[column].ToString());
                                                                    writer.WriteEndElement();
                                                                }
                                                            }
                                                        }
                                                        else if (object.ReferenceEquals(column.DataType, typeof(System.DateTime)) & !string.IsNullOrEmpty(row[column].ToString()))
                                                        {
                                                            System.DateTime d = default(System.DateTime);
                                                            d = Convert.ToDateTime(row[column]);

                                                            if (GetFieldEmpty)
                                                            {
                                                                writer.WriteStartElement(column.ColumnName);
                                                                writer.WriteString((string.IsNullOrEmpty(d.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)) ? "" : d.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)));
                                                                writer.WriteEndElement();
                                                            }
                                                            else
                                                            {
                                                                if (!string.IsNullOrEmpty(row[column].ToString()))
                                                                {
                                                                    writer.WriteStartElement(column.ColumnName);
                                                                    writer.WriteString(row[column].ToString());
                                                                    writer.WriteEndElement();
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            //Write Tag
                                                            if (GetFieldEmpty)
                                                            {
                                                                writer.WriteStartElement(column.ColumnName);
                                                                //writer.WriteString(string.IsNullOrEmpty((row[column].ToString()) ? "" : row[column].ToString()));
                                                                writer.WriteString((string.IsNullOrEmpty(row[column].ToString()) ? "" : row[column].ToString()));
                                                                writer.WriteEndElement();
                                                            }
                                                            else
                                                            {
                                                                if (!string.IsNullOrEmpty(row[column].ToString()))
                                                                {
                                                                    if (ObjType == "2")
                                                                    {
                                                                        writer.WriteStartElement(column.ColumnName);
                                                                        if (column.ColumnName == "GroupCode" & CType == "1")
                                                                        {
                                                                            string GroupName = row[column].ToString();
                                                                            string GName = getSingleValue("Select \"GroupName\" from OCRG where \"GroupCode\"='" + GroupName + "'");
                                                                            string GName1 = getSingleValue1("Select \"GroupCode\" from OCRG where \"GroupName\"='" + GName + "'");
                                                                            if (GName1.ToString().Trim() == "")
                                                                            {
                                                                                try
                                                                                {
                                                                                    Global.PPType = 1;
                                                                                    string MRPQry = "";
                                                                                    SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                                                                    if (Global.oServerType == "dst_HANADB")
                                                                                    {
                                                                                        MRPQry = "Update \"OCRD\" set  \"U_EMsg\"='This " + GName + " Group Name not there' where \"CardCode\"='" + DocEntry.ToString().Trim() + "'";
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        MRPQry = "Update \"OCRD\" set  \"U_EMsg\"='This " + GName + " Group Name not there' where \"CardCode\"='" + DocEntry.ToString().Trim() + "'";
                                                                                    }
                                                                                    oRS1.DoQuery(MRPQry);
                                                                                }
                                                                                catch (Exception e)
                                                                                {

                                                                                    throw;
                                                                                }

                                                                            }
                                                                            else
                                                                            {
                                                                                writer.WriteString(GName1.ToString());
                                                                                writer.WriteEndElement();
                                                                            }
                                                                        }
                                                                        else if (column.ColumnName == "GroupCode" & CType == "2")
                                                                        {
                                                                            string GroupName = row[column].ToString();
                                                                            string GName = getSingleValue("Select \"GroupName\" from OCRG where \"GroupCode\"='" + GroupName + "'");
                                                                            string GName1 = getSingleValue2("Select \"GroupCode\" from OCRG where \"GroupName\"='" + GName + "'");
                                                                            if (GName1.ToString().Trim() == "")
                                                                            {
                                                                                try
                                                                                {
                                                                                    Global.PPType = 1;
                                                                                    string MRPQry = "";
                                                                                    SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                                                                    if (Global.oServerType == "dst_HANADB")
                                                                                    {
                                                                                        MRPQry = "Update \"OCRD\" set  \"U_EMsg1\"='This " + GName + " Group Name not there' where \"CardCode\"='" + DocEntry.ToString().Trim() + "'";
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        MRPQry = "Update \"OCRD\" set  \"U_EMsg1\"='This " + GName + " Group Name not there' where \"CardCode\"='" + DocEntry.ToString().Trim() + "'";
                                                                                    }
                                                                                    oRS1.DoQuery(MRPQry);
                                                                                }
                                                                                catch (Exception e)
                                                                                {

                                                                                    throw;
                                                                                }

                                                                            }
                                                                            else
                                                                            {
                                                                                writer.WriteString(GName1.ToString());
                                                                                writer.WriteEndElement();
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            writer.WriteString(row[column].ToString());
                                                                            writer.WriteEndElement();
                                                                        }
                                                                    }
                                                                    if (ObjType == "4")
                                                                    {
                                                                        writer.WriteStartElement(column.ColumnName);
                                                                        if (column.ColumnName == "ItmsGrpCod" & CType == "1")
                                                                        {
                                                                            string GroupName = row[column].ToString();
                                                                            string GName = getSingleValue("Select \"ItmsGrpNam\" from OITB where \"ItmsGrpCod\"='" + GroupName + "'");
                                                                            string GName1 = getSingleValue1("Select \"ItmsGrpCod\" from OITB where \"ItmsGrpNam\"='" + GName + "'");
                                                                            if (GName1.ToString().Trim() == "")
                                                                            {
                                                                                try
                                                                                {
                                                                                    Global.PPType = 1;
                                                                                    string MRPQry = "";
                                                                                    SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                                                                    if (Global.oServerType == "dst_HANADB")
                                                                                    {
                                                                                        MRPQry = "Update \"OITM\" set  \"U_EMsg\"='This " + GName + " Item Group Name not there' where \"ItemCode\"='" + DocEntry.ToString().Trim() + "'";
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        MRPQry = "Update \"OITM\" set  \"U_EMsg\"='This " + GName + " Item Group Name not there' where \"ItemCode\"='" + DocEntry.ToString().Trim() + "'";
                                                                                    }
                                                                                    oRS1.DoQuery(MRPQry);
                                                                                }
                                                                                catch (Exception e)
                                                                                {

                                                                                    throw;
                                                                                }

                                                                            }
                                                                            else
                                                                            {
                                                                                writer.WriteString(GName1.ToString());
                                                                                writer.WriteEndElement();
                                                                            }
                                                                        }
                                                                        else if (column.ColumnName == "ItmsGrpCod" & CType == "2")
                                                                        {
                                                                            string GroupName = row[column].ToString();
                                                                            string GName = getSingleValue("Select \"ItmsGrpNam\" from OITB where \"ItmsGrpCod\"='" + GroupName + "'");
                                                                            string GName1 = getSingleValue2("Select \"ItmsGrpCod\" from OITB where \"ItmsGrpNam\"='" + GName + "'");
                                                                            if (GName1.ToString().Trim() == "")
                                                                            {
                                                                                try
                                                                                {
                                                                                    Global.PPType = 1;
                                                                                    string MRPQry = "";
                                                                                    SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                                                                    if (Global.oServerType == "dst_HANADB")
                                                                                    {
                                                                                        MRPQry = "Update \"OITM\" set  \"U_EMsg1\"='This " + GName + " Item Group Name not there' where \"ItemCode\"='" + DocEntry.ToString().Trim() + "'";
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        MRPQry = "Update \"OITM\" set  \"U_EMsg1\"='This " + GName + " Item Group Name not there' where \"ItemCode\"='" + DocEntry.ToString().Trim() + "'";
                                                                                    }
                                                                                    oRS1.DoQuery(MRPQry);
                                                                                }
                                                                                catch (Exception e)
                                                                                {

                                                                                    throw;
                                                                                }

                                                                            }
                                                                            else
                                                                            {
                                                                                writer.WriteString(GName1.ToString());
                                                                                writer.WriteEndElement();
                                                                            }
                                                                        }
                                                                        else if (column.ColumnName == "UgpEntry" & CType == "1")
                                                                        {
                                                                            string GroupName = row[column].ToString();
                                                                            string GName = getSingleValue("Select \"UgpName\" from OUGP where \"UgpEntry\"='" + GroupName + "'");
                                                                            string GName1 = getSingleValue1("Select \"UgpEntry\" from OUGP where \"UgpName\"='" + GName + "'");
                                                                            if (GName1.ToString().Trim() == "")
                                                                            {
                                                                                try
                                                                                {
                                                                                    Global.PPType = 1;
                                                                                    string MRPQry = "";
                                                                                    SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                                                                    if (Global.oServerType == "dst_HANADB")
                                                                                    {
                                                                                        MRPQry = "Update \"OITM\" set  \"U_EMsg\"='This " + GName + " UOM Group Name not there' where \"ItemCode\"='" + DocEntry.ToString().Trim() + "'";
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        MRPQry = "Update \"OITM\" set  \"U_EMsg\"='This " + GName + " UOM Group Name not there' where \"ItemCode\"='" + DocEntry.ToString().Trim() + "'";
                                                                                    }
                                                                                    oRS1.DoQuery(MRPQry);
                                                                                }
                                                                                catch (Exception e)
                                                                                {

                                                                                    throw;
                                                                                }

                                                                            }
                                                                            else
                                                                            {
                                                                                writer.WriteString(GName1.ToString());
                                                                                writer.WriteEndElement();
                                                                            }
                                                                        }
                                                                        else if (column.ColumnName == "UgpEntry" & CType == "2")
                                                                        {
                                                                            string GroupName = row[column].ToString();
                                                                            string GName = getSingleValue("Select \"UgpName\" from OUGP where \"UgpEntry\"='" + GroupName + "'");
                                                                            string GName1 = getSingleValue2("Select \"UgpEntry\" from OUGP where \"UgpName\"='" + GName + "'");
                                                                            if (GName1.ToString().Trim() == "")
                                                                            {
                                                                                try
                                                                                {
                                                                                    Global.PPType = 1;
                                                                                    string MRPQry = "";
                                                                                    SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                                                                    if (Global.oServerType == "dst_HANADB")
                                                                                    {
                                                                                        MRPQry = "Update \"OITM\" set  \"U_EMsg1\"='This " + GName + " UOM Group Name not there' where \"ItemCode\"='" + DocEntry.ToString().Trim() + "'";
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        MRPQry = "Update \"OITM\" set  \"U_EMsg1\"='This " + GName + " UOM Group Name not there' where \"ItemCode\"='" + DocEntry.ToString().Trim() + "'";
                                                                                    }
                                                                                    oRS1.DoQuery(MRPQry);
                                                                                }
                                                                                catch (Exception e)
                                                                                {

                                                                                    throw;
                                                                                }

                                                                            }
                                                                            else
                                                                            {
                                                                                writer.WriteString(GName1.ToString());
                                                                                writer.WriteEndElement();
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            writer.WriteString(row[column].ToString());
                                                                            writer.WriteEndElement();
                                                                        }
                                                                    }
                                                                }
                                                                //if (dt.TableName.ToString() == "CRD7")
                                                                //{
                                                                //    if (column.ColumnName == "Address" & string.IsNullOrEmpty(row[column].ToString()))
                                                                //    {
                                                                //        writer.WriteStartElement(column.ColumnName);
                                                                //        writer.WriteString(row[column].ToString());
                                                                //        writer.WriteEndElement();
                                                                //    }
                                                                //}
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            writer.WriteEndElement();
                                        }
                                    }
                                    writer.WriteEndElement();
                                }
                            }
                            //#End Region
                        }
                        writer.WriteEndElement();
                    }
                    writer.WriteEndElement();
                }
                writer.WriteEndDocument();

                writer.Flush();

                return XmlString.ToString();
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public static string ToMultiXMLStringFromDS(int NoOfDoc, string[] ObjType, DataSet[] ds, bool GetFieldEmpty = false)
        {
            try
            {
                if (ObjType.Length != NoOfDoc | ds.Length != NoOfDoc)
                {
                    return "No. of Doc. different in length of array";
                }
                //Dim gf As New GeneralFunctions()
                StringBuilder XmlString = new StringBuilder();
                XmlWriter writer = XmlWriter.Create(XmlString);
                writer.WriteStartDocument();
                if (true)
                {
                    writer.WriteStartElement("BOM");
                    if (true)
                    {
                        for (int i = 0; i <= NoOfDoc - 1; i++)
                        {
                            writer.WriteStartElement("BO");
                            if (true)
                            {
                                //#Region "write ADMINFO_ELEMENT"
                                writer.WriteStartElement("AdmInfo");
                                if (true)
                                {
                                    writer.WriteStartElement("Object");
                                    if (true)
                                    {
                                        writer.WriteString(ObjType[i]);
                                    }
                                    writer.WriteEndElement();
                                }
                                writer.WriteEndElement();
                                //#End Region

                                //#Region "Header&Line XML"
                                foreach (System.Data.DataTable dt in ds[0].Tables)
                                {
                                    if (dt.Rows.Count > 0)
                                    {
                                        writer.WriteStartElement(dt.TableName.ToString(CultureInfo.InvariantCulture));
                                        if (true)
                                        {
                                            foreach (DataRow row in dt.Rows)
                                            {
                                                writer.WriteStartElement("row");
                                                if (true)
                                                {
                                                    foreach (System.Data.DataColumn column in dt.Columns)
                                                    {
                                                        if (column.DefaultValue.ToString() != "xx_remove_xx")
                                                        {
                                                            //Force datetime format follow SQL 
                                                            if (object.ReferenceEquals(column.DataType, typeof(DateTime)))
                                                            {
                                                                DateTime dateTime = default(DateTime);
                                                                dateTime = Convert.ToDateTime(row[column]);   // row(column);
                                                                if (GetFieldEmpty)
                                                                {
                                                                    writer.WriteStartElement(column.ColumnName);
                                                                    writer.WriteString((string.IsNullOrEmpty(dateTime.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)) ? "" : dateTime.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)));
                                                                    writer.WriteEndElement();
                                                                }
                                                                else
                                                                {
                                                                    if (!string.IsNullOrEmpty(row[column].ToString()))
                                                                    {
                                                                        writer.WriteStartElement(column.ColumnName);
                                                                        writer.WriteString(row[column].ToString());
                                                                        writer.WriteEndElement();
                                                                    }
                                                                }
                                                            }
                                                            else if (object.ReferenceEquals(column.DataType, typeof(System.DateTime)))
                                                            {
                                                                System.DateTime d = default(System.DateTime);
                                                                d = Convert.ToDateTime(row[column]);

                                                                if (GetFieldEmpty)
                                                                {
                                                                    writer.WriteStartElement(column.ColumnName);
                                                                    // writer.WriteString((string.IsNullOrEmpty(dateTime.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)) ? "" : dateTime.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)))
                                                                    writer.WriteString((string.IsNullOrEmpty(d.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)) ? "" : d.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)));
                                                                    writer.WriteEndElement();
                                                                }
                                                                else
                                                                {
                                                                    if (!string.IsNullOrEmpty(row[column].ToString()))
                                                                    {
                                                                        writer.WriteStartElement(column.ColumnName);
                                                                        writer.WriteString(row[column].ToString());
                                                                        writer.WriteEndElement();
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                //Write Tag
                                                                if (GetFieldEmpty)
                                                                {
                                                                    writer.WriteStartElement(column.ColumnName);
                                                                    //   writer.WriteString(string.IsNullOrEmpty((row[column].ToString()) ? "" : row[column].ToString()));
                                                                    writer.WriteString((string.IsNullOrEmpty(row[column].ToString()) ? "" : row[column].ToString()));
                                                                    // writer.WriteString(IIf(string.IsNullOrEmpty(row(column)), "", row(column)))
                                                                    writer.WriteEndElement();
                                                                }
                                                                else
                                                                {
                                                                    if (!string.IsNullOrEmpty(row[column].ToString()))
                                                                    {
                                                                        writer.WriteStartElement(column.ColumnName);
                                                                        writer.WriteString(row[column].ToString());
                                                                        writer.WriteEndElement();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                writer.WriteEndElement();
                                            }
                                        }
                                        writer.WriteEndElement();
                                    }
                                }
                                //#End Region
                            }
                            writer.WriteEndElement();
                        }
                    }
                    writer.WriteEndElement();
                }
                writer.WriteEndDocument();

                writer.Flush();

                return XmlString.ToString();
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        #endregion
        public static DataSet xmlStr2Ds(string xmlString, bool multipleXML = false, string ObjType = "")
        {
            DataSet dsBO = new DataSet();
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(xmlString);
            BuildDataSet(dsBO, xml.ChildNodes, multipleXML, ObjType);
            return dsBO;
        }
        private static void BuildTable(DataSet ads, XmlNode nodeTable)
        {
            System.Data.DataTable dt = new System.Data.DataTable(nodeTable.Name);
            XmlNode firstNode = nodeTable.FirstChild;
            if (firstNode != null)
            {
                foreach (XmlNode col in firstNode.ChildNodes)
                {
                    if (!dt.Columns.Contains(col.Name))
                    {
                        dt.Columns.Add(new System.Data.DataColumn(col.Name, Type.GetType("System.String")));
                    }
                }
            }

            foreach (XmlNode rowNode in nodeTable.ChildNodes)
            {
                DataRow dr = dt.NewRow();
                foreach (XmlNode colValue in rowNode.ChildNodes)
                {
                    if (!dt.Columns.Contains(colValue.Name))
                    {
                        dt.Columns.Add(colValue.Name);
                    }

                    dr[colValue.Name] = colValue.InnerText;
                }

                dt.Rows.Add(dr);
            }

            if (!ads.Tables.Contains(dt.TableName))
            {
                ads.Tables.Add(dt.Copy());
                // ads.Tables.Add(dt.Copy);
            }
        }
        private static void BuildDataSet(DataSet ds, XmlNodeList parentNode, bool multipleXML, string ObjType)
        {
            foreach (XmlNode nodeParent in parentNode)
            {
                if (nodeParent.Name.Equals("BO"))
                {
                    foreach (XmlNode node in nodeParent.ChildNodes)
                    {
                        if (node.Name.Equals("AdmInfo"))
                        {
                            continue;
                            //continue foreach
                        }
                        BuildTable(ds, node);
                    }
                    if (!multipleXML)
                    {
                        break;
                    }
                }
                else
                {
                    BuildDataSet(ds, nodeParent.ChildNodes, multipleXML, ObjType);
                }
            }
        }

        public static string MasterData(string strXml, string DocType, string DocEntry, string Seqid)
        {
            try
            {

                int lErrCode;
                string lErrStr = "";
                if (DocType == "2")
                {
                    if (!Global.oCompany1.InTransaction)
                        Global.oCompany1.StartTransaction();
                    SAPbobsCOM.BusinessPartners OCRD;
                    OCRD = Global.oCompany1.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
                    Global.oCompany1.XMLAsString = true;
                    OCRD = Global.oCompany1.GetBusinessObjectFromXML(strXml, 0);
                    if (OCRD.GetByKey(DocEntry) == true)
                    {
                        OCRD.Browser.ReadXml(strXml, 0);
                        lErrCode = OCRD.Update();
                        if (lErrCode == 0)
                        {
                            if (Global.oCompany1.InTransaction)
                                Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            // WriteLog("Business Partner '" + DocEntry + "' Update Successfully Procees Table Seqid : " + Seqid.ToString());
                            //  DBConnection();
                            if (!Global.oCompany.InTransaction)
                                Global.oCompany.StartTransaction();
                            try
                            {
                                SAPbobsCOM.Recordset rsetCode;
                                string Query = "Update \"OCRD\" set \"U_EMsg\"='',\"U_Status\"='Y' where \"CardCode\"='" + DocEntry + "'";
                                rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                rsetCode.DoQuery(Query);
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //     WriteLog("Process Table Update Successfully Seqid : " + Seqid.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                //    WriteLog("Process Table Update Error: Seqid: " + Seqid + " " + ex.ToString());
                            }

                            //int reusult = Integration_RunQuery_Update(Query);
                            //if (reusult == 0)
                            //{
                            //    WriteLog("Update Successfully: " + reusult);
                            //}
                            //else
                            //{
                            //    WriteLog("Update Error: " + reusult);
                            //}
                        }
                        else
                        {
                            lErrStr = Global.oCompany1.GetLastErrorDescription();
                            if (Global.oCompany1.InTransaction)
                                Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            //   WriteLog("Business Partner '" + DocEntry + "' Update Error Message: " + lErrStr + " : Procees Table Seqid : " + Seqid.ToString());
                            //   DBConnection();
                            if (!Global.oCompany.InTransaction)
                                Global.oCompany.StartTransaction();
                            try
                            {
                                SAPbobsCOM.Recordset rsetCode;
                                string Query = "Update \"OCRD\" set \"U_EMsg\"='" + lErrStr.ToString().Replace("'", "") + "' where \"CardCode\"='" + DocEntry + "'";
                                rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                rsetCode.DoQuery(Query);
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //     WriteLog("Process Table Update Successfully Seqid : " + Seqid.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                //    WriteLog("Process Table Update Error: Seqid: " + Seqid + " " + ex.ToString());
                            }

                            //int reusult = Integration_RunQuery_Update(Query);
                            //if (reusult == 0)
                            //{
                            //    WriteLog("Update Successfully: " + reusult);
                            //}
                            //else
                            //{
                            //    WriteLog("Update Error: " + reusult);
                            //}
                        }
                    }
                    else
                    {
                        // OCRD.Browser.ReadXml(strXml, 0);                    
                        lErrCode = OCRD.Add();
                        if (lErrCode == 0)
                        {
                            if (Global.oCompany1.InTransaction)
                                Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            //    WriteLog("Business Partner '" + DocEntry + "' Created Successfully Procees Table Seqid : " + Seqid.ToString());
                            //  DBConnection();
                            if (!Global.oCompany.InTransaction)
                                Global.oCompany.StartTransaction();
                            try
                            {
                                SAPbobsCOM.Recordset rsetCode;
                                string Query = "Update \"OCRD\" set \"U_EMsg\"='',\"U_Status\"='Y' where \"CardCode\"='" + DocEntry + "'";
                                rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                rsetCode.DoQuery(Query);
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //    WriteLog("Process Table Update Successfully Seqid : " + Seqid.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                //   WriteLog("Process Table Update Error: Seqid: " + Seqid + " " + ex.ToString());
                            }
                        }
                        else
                        {
                            lErrStr = Global.oCompany1.GetLastErrorDescription();
                            if (Global.oCompany1.InTransaction)
                                Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            //  WriteLog("Business Partner '" + DocEntry + "' Creation Error Message: " + lErrStr + " : Procees Table Seqid : " + Seqid.ToString());
                            // DBConnection();
                            if (!Global.oCompany.InTransaction)
                                Global.oCompany.StartTransaction();
                            try
                            {
                                SAPbobsCOM.Recordset rsetCode;
                                string Query = "Update \"OCRD\" set \"U_EMsg\"='" + lErrStr.ToString().Replace("'", "") + "' where \"CardCode\"='" + DocEntry + "'";
                                rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                rsetCode.DoQuery(Query);
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //    WriteLog("Process Table Update Successfully Seqid : " + Seqid.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                //   WriteLog("Process Table Update Error: Seqid: " + Seqid + " " + ex.ToString());
                            }
                        }
                    }
                }
                else if (DocType == "4")
                {

                    if (!Global.oCompany1.InTransaction)
                        Global.oCompany1.StartTransaction();
                    SAPbobsCOM.Items OCRD;
                    OCRD = Global.oCompany1.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
                    Global.oCompany1.XMLAsString = true;
                    OCRD = Global.oCompany1.GetBusinessObjectFromXML(strXml, 0);
                    if (OCRD.GetByKey(DocEntry) == true)
                    {
                        OCRD.Browser.ReadXml(strXml, 0);
                        lErrCode = OCRD.Update();
                        if (lErrCode == 0)
                        {
                            if (Global.oCompany1.InTransaction)
                                Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            // WriteLog("Business Partner '" + DocEntry + "' Update Successfully Procees Table Seqid : " + Seqid.ToString());
                            // DBConnection();
                            if (!Global.oCompany.InTransaction)
                                Global.oCompany.StartTransaction();
                            try
                            {
                                SAPbobsCOM.Recordset rsetCode;
                                string Query = "Update \"OITM\" set \"U_EMsg\"='',\"U_Status\"='Y' where \"ItemCode\"='" + DocEntry + "'";
                                rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                rsetCode.DoQuery(Query);
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //     WriteLog("Process Table Update Successfully Seqid : " + Seqid.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (Global.oCompany1.InTransaction)
                                    Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                //    WriteLog("Process Table Update Error: Seqid: " + Seqid + " " + ex.ToString());
                            }

                            //int reusult = Integration_RunQuery_Update(Query);
                            //if (reusult == 0)
                            //{
                            //    WriteLog("Update Successfully: " + reusult);
                            //}
                            //else
                            //{
                            //    WriteLog("Update Error: " + reusult);
                            //}
                        }
                        else
                        {
                            lErrStr = Global.oCompany1.GetLastErrorDescription();
                            if (Global.oCompany1.InTransaction)
                                Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            //   WriteLog("Business Partner '" + DocEntry + "' Update Error Message: " + lErrStr + " : Procees Table Seqid : " + Seqid.ToString());
                            //   DBConnection();
                            if (!Global.oCompany.InTransaction)
                                Global.oCompany.StartTransaction();
                            try
                            {
                                SAPbobsCOM.Recordset rsetCode;
                                string Query = "Update \"OITM\" set \"U_EMsg\"='" + lErrStr.ToString().Replace("'", "") + "' where \"ItemCode\"='" + DocEntry + "'";
                                rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                rsetCode.DoQuery(Query);
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //     WriteLog("Process Table Update Successfully Seqid : " + Seqid.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                //    WriteLog("Process Table Update Error: Seqid: " + Seqid + " " + ex.ToString());
                            }

                            //int reusult = Integration_RunQuery_Update(Query);
                            //if (reusult == 0)
                            //{
                            //    WriteLog("Update Successfully: " + reusult);
                            //}
                            //else
                            //{
                            //    WriteLog("Update Error: " + reusult);
                            //}
                        }
                    }
                    else
                    {
                        // OCRD.Browser.ReadXml(strXml, 0);                    
                        lErrCode = OCRD.Add();
                        if (lErrCode == 0)
                        {
                            if (Global.oCompany1.InTransaction)
                                Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            //    WriteLog("Business Partner '" + DocEntry + "' Created Successfully Procees Table Seqid : " + Seqid.ToString());
                            //  DBConnection();
                            if (!Global.oCompany.InTransaction)
                                Global.oCompany.StartTransaction();
                            try
                            {
                                SAPbobsCOM.Recordset rsetCode;
                                string Query = "Update \"OITM\" set \"U_EMsg\"='',\"U_Status\"='Y' where \"ItemCode\"='" + DocEntry + "'";
                                rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                rsetCode.DoQuery(Query);
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //    WriteLog("Process Table Update Successfully Seqid : " + Seqid.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (Global.oCompany1.InTransaction)
                                    Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                //   WriteLog("Process Table Update Error: Seqid: " + Seqid + " " + ex.ToString());
                            }
                        }
                        else
                        {
                            lErrStr = Global.oCompany1.GetLastErrorDescription();
                            if (Global.oCompany1.InTransaction)
                                Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            //  WriteLog("Business Partner '" + DocEntry + "' Creation Error Message: " + lErrStr + " : Procees Table Seqid : " + Seqid.ToString());
                            // DBConnection();
                            if (!Global.oCompany.InTransaction)
                                Global.oCompany.StartTransaction();
                            try
                            {
                                SAPbobsCOM.Recordset rsetCode;
                                string Query = "Update \"OITM\" set \"U_EMsg\"='" + lErrStr.ToString().Replace("'", "") + "' where \"ItemCode\"='" + DocEntry + "'";
                                rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                rsetCode.DoQuery(Query);
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //    WriteLog("Process Table Update Successfully Seqid : " + Seqid.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                //   WriteLog("Process Table Update Error: Seqid: " + Seqid + " " + ex.ToString());
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                //   WriteLog("System : ERROR:" + ex.ToString().Replace("'", ""));
                DBConnection();
                if (!Global.oCompany.InTransaction)
                    Global.oCompany.StartTransaction();
                try
                {
                    SAPbobsCOM.Recordset rsetCode;
                    string Query = "Update \"@CW_PROCESSLOG\" set \"U_ErrCode\"='" + ex.ToString().Replace("'", "") + "',\"U_UpdDate\"=(SELECT CURRENT_DATE from Dummy) where \"U_SeqId\"='" + Seqid + "'";
                    rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    rsetCode.DoQuery(Query);
                    if (Global.oCompany.InTransaction)
                        Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    //  WriteLog("Update Successfully Seqid : " + Seqid.ToString());
                }
                catch (Exception ex1)
                {
                    if (Global.oCompany.InTransaction)
                        Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    // WriteLog("Update Error: Seqid: " + Seqid + " " + ex1.ToString().Replace("'", ""));
                }

            }
            return "";
        }

        public static string MasterData1(string strXml, string DocType, string DocEntry, string Seqid)
        {
            try
            {

                int lErrCode;
                string lErrStr = "";
                if (DocType == "2")
                {
                    if (!Global.oCompany2.InTransaction)
                        Global.oCompany2.StartTransaction();
                    SAPbobsCOM.BusinessPartners OCRD;
                    OCRD = Global.oCompany2.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
                    Global.oCompany2.XMLAsString = true;
                    OCRD = Global.oCompany2.GetBusinessObjectFromXML(strXml, 0);
                    if (OCRD.GetByKey(DocEntry) == true)
                    {
                        OCRD.Browser.ReadXml(strXml, 0);
                        lErrCode = OCRD.Update();
                        if (lErrCode == 0)
                        {
                            if (Global.oCompany2.InTransaction)
                                Global.oCompany2.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            // WriteLog("Business Partner '" + DocEntry + "' Update Successfully Procees Table Seqid : " + Seqid.ToString());
                            DBConnection();
                            if (!Global.oCompany.InTransaction)
                                Global.oCompany.StartTransaction();
                            try
                            {
                                SAPbobsCOM.Recordset rsetCode;
                                string Query = "Update \"OCRD\" set \"U_EMsg1\"='',\"U_Status1\"='Y' where \"CardCode\"='" + DocEntry + "'";
                                rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                rsetCode.DoQuery(Query);
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //     WriteLog("Process Table Update Successfully Seqid : " + Seqid.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                //    WriteLog("Process Table Update Error: Seqid: " + Seqid + " " + ex.ToString());
                            }

                            //int reusult = Integration_RunQuery_Update(Query);
                            //if (reusult == 0)
                            //{
                            //    WriteLog("Update Successfully: " + reusult);
                            //}
                            //else
                            //{
                            //    WriteLog("Update Error: " + reusult);
                            //}
                        }
                        else
                        {
                            lErrStr = Global.oCompany2.GetLastErrorDescription();
                            if (Global.oCompany2.InTransaction)
                                Global.oCompany2.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            //   WriteLog("Business Partner '" + DocEntry + "' Update Error Message: " + lErrStr + " : Procees Table Seqid : " + Seqid.ToString());
                            //   DBConnection();
                            if (!Global.oCompany.InTransaction)
                                Global.oCompany.StartTransaction();
                            try
                            {
                                SAPbobsCOM.Recordset rsetCode;
                                string Query = "Update \"OCRD\" set \"U_EMsg1\"='" + lErrStr.ToString().Replace("'", "") + "' where \"CardCode\"='" + DocEntry + "'";
                                rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                rsetCode.DoQuery(Query);
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //     WriteLog("Process Table Update Successfully Seqid : " + Seqid.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                //    WriteLog("Process Table Update Error: Seqid: " + Seqid + " " + ex.ToString());
                            }

                            //int reusult = Integration_RunQuery_Update(Query);
                            //if (reusult == 0)
                            //{
                            //    WriteLog("Update Successfully: " + reusult);
                            //}
                            //else
                            //{
                            //    WriteLog("Update Error: " + reusult);
                            //}
                        }
                    }
                    else
                    {
                        // OCRD.Browser.ReadXml(strXml, 0);                    
                        lErrCode = OCRD.Add();
                        if (lErrCode == 0)
                        {
                            if (Global.oCompany2.InTransaction)
                                Global.oCompany2.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            //    WriteLog("Business Partner '" + DocEntry + "' Created Successfully Procees Table Seqid : " + Seqid.ToString());
                            //  DBConnection();
                            if (!Global.oCompany.InTransaction)
                                Global.oCompany.StartTransaction();
                            try
                            {
                                SAPbobsCOM.Recordset rsetCode;
                                string Query = "Update \"OCRD\" set \"U_EMsg1\"='',\"U_Status1\"='Y' where \"CardCode\"='" + DocEntry + "'";
                                rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                rsetCode.DoQuery(Query);
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //    WriteLog("Process Table Update Successfully Seqid : " + Seqid.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                //   WriteLog("Process Table Update Error: Seqid: " + Seqid + " " + ex.ToString());
                            }
                        }
                        else
                        {
                            lErrStr = Global.oCompany2.GetLastErrorDescription();
                            if (Global.oCompany2.InTransaction)
                                Global.oCompany2.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            //  WriteLog("Business Partner '" + DocEntry + "' Creation Error Message: " + lErrStr + " : Procees Table Seqid : " + Seqid.ToString());
                            // DBConnection();
                            if (!Global.oCompany.InTransaction)
                                Global.oCompany.StartTransaction();
                            try
                            {
                                SAPbobsCOM.Recordset rsetCode;
                                string Query = "Update \"OCRD\" set \"U_EMsg1\"='" + lErrStr.ToString().Replace("'", "") + "' where \"CardCode\"='" + DocEntry + "'";
                                rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                rsetCode.DoQuery(Query);
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //    WriteLog("Process Table Update Successfully Seqid : " + Seqid.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                //   WriteLog("Process Table Update Error: Seqid: " + Seqid + " " + ex.ToString());
                            }
                        }
                    }
                }
                else if (DocType == "4")
                {

                    if (!Global.oCompany2.InTransaction)
                        Global.oCompany2.StartTransaction();
                    SAPbobsCOM.Items OCRD;
                    OCRD = Global.oCompany2.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
                    Global.oCompany2.XMLAsString = true;
                    OCRD = Global.oCompany2.GetBusinessObjectFromXML(strXml, 0);
                    if (OCRD.GetByKey(DocEntry) == true)
                    {
                        OCRD.Browser.ReadXml(strXml, 0);
                        lErrCode = OCRD.Update();
                        if (lErrCode == 0)
                        {
                            if (Global.oCompany2.InTransaction)
                                Global.oCompany2.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            // WriteLog("Business Partner '" + DocEntry + "' Update Successfully Procees Table Seqid : " + Seqid.ToString());
                            // DBConnection();
                            if (!Global.oCompany.InTransaction)
                                Global.oCompany.StartTransaction();
                            try
                            {
                                SAPbobsCOM.Recordset rsetCode;
                                string Query = "Update \"OITM\" set \"U_EMsg1\"='',\"U_Status1\"='Y' where \"ItemCode\"='" + DocEntry + "'";
                                rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                rsetCode.DoQuery(Query);
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //     WriteLog("Process Table Update Successfully Seqid : " + Seqid.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                //    WriteLog("Process Table Update Error: Seqid: " + Seqid + " " + ex.ToString());
                            }

                            //int reusult = Integration_RunQuery_Update(Query);
                            //if (reusult == 0)
                            //{
                            //    WriteLog("Update Successfully: " + reusult);
                            //}
                            //else
                            //{
                            //    WriteLog("Update Error: " + reusult);
                            //}
                        }
                        else
                        {
                            lErrStr = Global.oCompany2.GetLastErrorDescription();
                            if (Global.oCompany2.InTransaction)
                                Global.oCompany2.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            //   WriteLog("Business Partner '" + DocEntry + "' Update Error Message: " + lErrStr + " : Procees Table Seqid : " + Seqid.ToString());
                            //   DBConnection();
                            if (!Global.oCompany.InTransaction)
                                Global.oCompany.StartTransaction();
                            try
                            {
                                SAPbobsCOM.Recordset rsetCode;
                                string Query = "Update \"OITM\" set \"U_EMsg1\"='" + lErrStr.ToString().Replace("'", "") + "' where \"ItemCode\"='" + DocEntry + "'";
                                rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                rsetCode.DoQuery(Query);
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //     WriteLog("Process Table Update Successfully Seqid : " + Seqid.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                //    WriteLog("Process Table Update Error: Seqid: " + Seqid + " " + ex.ToString());
                            }

                            //int reusult = Integration_RunQuery_Update(Query);
                            //if (reusult == 0)
                            //{
                            //    WriteLog("Update Successfully: " + reusult);
                            //}
                            //else
                            //{
                            //    WriteLog("Update Error: " + reusult);
                            //}
                        }
                    }
                    else
                    {
                        // OCRD.Browser.ReadXml(strXml, 0);                    
                        lErrCode = OCRD.Add();
                        if (lErrCode == 0)
                        {
                            if (Global.oCompany2.InTransaction)
                                Global.oCompany2.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            //    WriteLog("Business Partner '" + DocEntry + "' Created Successfully Procees Table Seqid : " + Seqid.ToString());
                            //  DBConnection();
                            if (!Global.oCompany.InTransaction)
                                Global.oCompany.StartTransaction();
                            try
                            {
                                SAPbobsCOM.Recordset rsetCode;
                                string Query = "Update \"OITM\" set \"U_EMsg1\"='',\"U_Status1\"='Y' where \"ItemCode\"='" + DocEntry + "'";
                                rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                rsetCode.DoQuery(Query);
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //    WriteLog("Process Table Update Successfully Seqid : " + Seqid.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                //   WriteLog("Process Table Update Error: Seqid: " + Seqid + " " + ex.ToString());
                            }
                        }
                        else
                        {
                            lErrStr = Global.oCompany2.GetLastErrorDescription();
                            if (Global.oCompany2.InTransaction)
                                Global.oCompany2.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            //  WriteLog("Business Partner '" + DocEntry + "' Creation Error Message: " + lErrStr + " : Procees Table Seqid : " + Seqid.ToString());
                            // DBConnection();
                            if (!Global.oCompany.InTransaction)
                                Global.oCompany.StartTransaction();
                            try
                            {
                                SAPbobsCOM.Recordset rsetCode;
                                string Query = "Update \"OITM\" set \"U_EMsg1\"='" + lErrStr.ToString().Replace("'", "") + "' where \"ItemCode\"='" + DocEntry + "'";
                                rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                rsetCode.DoQuery(Query);
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //    WriteLog("Process Table Update Successfully Seqid : " + Seqid.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                //   WriteLog("Process Table Update Error: Seqid: " + Seqid + " " + ex.ToString());
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                //   WriteLog("System : ERROR:" + ex.ToString().Replace("'", ""));
                DBConnection();
                if (!Global.oCompany.InTransaction)
                    Global.oCompany.StartTransaction();
                try
                {
                    SAPbobsCOM.Recordset rsetCode;
                    string Query = "Update \"@CW_PROCESSLOG\" set \"U_ErrCode\"='" + ex.ToString().Replace("'", "") + "',\"U_UpdDate\"=(SELECT CURRENT_DATE from Dummy) where \"U_SeqId\"='" + Seqid + "'";
                    rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    rsetCode.DoQuery(Query);
                    if (Global.oCompany.InTransaction)
                        Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    //  WriteLog("Update Successfully Seqid : " + Seqid.ToString());
                }
                catch (Exception ex1)
                {
                    if (Global.oCompany.InTransaction)
                        Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    // WriteLog("Update Error: Seqid: " + Seqid + " " + ex1.ToString().Replace("'", ""));
                }

            }
            return "";
        }

        public static string MasterData2(string strXml, string DocType, string DocEntry, string Seqid)
        {
            try
            {

                int lErrCode;
                string lErrStr = "";
                if (DocType == "2")
                {
                    if (!Global.oCompany3.InTransaction)
                        Global.oCompany3.StartTransaction();
                    SAPbobsCOM.BusinessPartners OCRD;
                    OCRD = Global.oCompany3.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
                    Global.oCompany3.XMLAsString = true;
                    OCRD = Global.oCompany3.GetBusinessObjectFromXML(strXml, 0);
                    if (OCRD.GetByKey(DocEntry) == true)
                    {
                        OCRD.Browser.ReadXml(strXml, 0);
                        lErrCode = OCRD.Update();
                        if (lErrCode == 0)
                        {
                            if (Global.oCompany3.InTransaction)
                                Global.oCompany3.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            // WriteLog("Business Partner '" + DocEntry + "' Update Successfully Procees Table Seqid : " + Seqid.ToString());
                            DBConnection();
                            if (!Global.oCompany.InTransaction)
                                Global.oCompany.StartTransaction();
                            try
                            {
                                SAPbobsCOM.Recordset rsetCode;
                                string Query = "Update \"OCRD\" set \"U_EMsg2\"='',\"U_Status2\"='Y' where \"CardCode\"='" + DocEntry + "'";
                                rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                rsetCode.DoQuery(Query);
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //     WriteLog("Process Table Update Successfully Seqid : " + Seqid.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                //    WriteLog("Process Table Update Error: Seqid: " + Seqid + " " + ex.ToString());
                            }

                            //int reusult = Integration_RunQuery_Update(Query);
                            //if (reusult == 0)
                            //{
                            //    WriteLog("Update Successfully: " + reusult);
                            //}
                            //else
                            //{
                            //    WriteLog("Update Error: " + reusult);
                            //}
                        }
                        else
                        {
                            lErrStr = Global.oCompany3.GetLastErrorDescription();
                            if (Global.oCompany3.InTransaction)
                                Global.oCompany3.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            //   WriteLog("Business Partner '" + DocEntry + "' Update Error Message: " + lErrStr + " : Procees Table Seqid : " + Seqid.ToString());
                            //   DBConnection();
                            if (!Global.oCompany.InTransaction)
                                Global.oCompany.StartTransaction();
                            try
                            {
                                SAPbobsCOM.Recordset rsetCode;
                                string Query = "Update \"OCRD\" set \"U_EMsg2\"='" + lErrStr.ToString().Replace("'", "") + "' where \"CardCode\"='" + DocEntry + "'";
                                rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                rsetCode.DoQuery(Query);
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //     WriteLog("Process Table Update Successfully Seqid : " + Seqid.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                //    WriteLog("Process Table Update Error: Seqid: " + Seqid + " " + ex.ToString());
                            }

                            //int reusult = Integration_RunQuery_Update(Query);
                            //if (reusult == 0)
                            //{
                            //    WriteLog("Update Successfully: " + reusult);
                            //}
                            //else
                            //{
                            //    WriteLog("Update Error: " + reusult);
                            //}
                        }
                    }
                    else
                    {
                        // OCRD.Browser.ReadXml(strXml, 0);                    
                        lErrCode = OCRD.Add();
                        if (lErrCode == 0)
                        {
                            if (Global.oCompany3.InTransaction)
                                Global.oCompany3.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            //    WriteLog("Business Partner '" + DocEntry + "' Created Successfully Procees Table Seqid : " + Seqid.ToString());
                            //  DBConnection();
                            if (!Global.oCompany.InTransaction)
                                Global.oCompany.StartTransaction();
                            try
                            {
                                SAPbobsCOM.Recordset rsetCode;
                                string Query = "Update \"OCRD\" set \"U_EMsg2\"='',\"U_Status2\"='Y' where \"CardCode\"='" + DocEntry + "'";
                                rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                rsetCode.DoQuery(Query);
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //    WriteLog("Process Table Update Successfully Seqid : " + Seqid.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                //   WriteLog("Process Table Update Error: Seqid: " + Seqid + " " + ex.ToString());
                            }
                        }
                        else
                        {
                            lErrStr = Global.oCompany3.GetLastErrorDescription();
                            if (Global.oCompany3.InTransaction)
                                Global.oCompany3.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            //  WriteLog("Business Partner '" + DocEntry + "' Creation Error Message: " + lErrStr + " : Procees Table Seqid : " + Seqid.ToString());
                            // DBConnection();
                            if (!Global.oCompany.InTransaction)
                                Global.oCompany.StartTransaction();
                            try
                            {
                                SAPbobsCOM.Recordset rsetCode;
                                string Query = "Update \"OCRD\" set \"U_EMsg2\"='" + lErrStr.ToString().Replace("'", "") + "' where \"CardCode\"='" + DocEntry + "'";
                                rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                rsetCode.DoQuery(Query);
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //    WriteLog("Process Table Update Successfully Seqid : " + Seqid.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                //   WriteLog("Process Table Update Error: Seqid: " + Seqid + " " + ex.ToString());
                            }
                        }
                    }
                }
                else if (DocType == "4")
                {

                    if (!Global.oCompany3.InTransaction)
                        Global.oCompany3.StartTransaction();
                    SAPbobsCOM.Items OCRD;
                    OCRD = Global.oCompany3.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
                    Global.oCompany3.XMLAsString = true;
                    OCRD = Global.oCompany3.GetBusinessObjectFromXML(strXml, 0);
                    if (OCRD.GetByKey(DocEntry) == true)
                    {
                        OCRD.Browser.ReadXml(strXml, 0);
                        lErrCode = OCRD.Update();
                        if (lErrCode == 0)
                        {
                            if (Global.oCompany3.InTransaction)
                                Global.oCompany3.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            // WriteLog("Business Partner '" + DocEntry + "' Update Successfully Procees Table Seqid : " + Seqid.ToString());
                            // DBConnection();
                            if (!Global.oCompany.InTransaction)
                                Global.oCompany.StartTransaction();
                            try
                            {
                                SAPbobsCOM.Recordset rsetCode;
                                string Query = "Update \"OITM\" set \"U_EMsg2\"='',\"U_Status2\"='Y' where \"ItemCode\"='" + DocEntry + "'";
                                rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                rsetCode.DoQuery(Query);
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //     WriteLog("Process Table Update Successfully Seqid : " + Seqid.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                //    WriteLog("Process Table Update Error: Seqid: " + Seqid + " " + ex.ToString());
                            }

                            //int reusult = Integration_RunQuery_Update(Query);
                            //if (reusult == 0)
                            //{
                            //    WriteLog("Update Successfully: " + reusult);
                            //}
                            //else
                            //{
                            //    WriteLog("Update Error: " + reusult);
                            //}
                        }
                        else
                        {
                            lErrStr = Global.oCompany3.GetLastErrorDescription();
                            if (Global.oCompany3.InTransaction)
                                Global.oCompany3.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            //   WriteLog("Business Partner '" + DocEntry + "' Update Error Message: " + lErrStr + " : Procees Table Seqid : " + Seqid.ToString());
                            //   DBConnection();
                            if (!Global.oCompany.InTransaction)
                                Global.oCompany.StartTransaction();
                            try
                            {
                                SAPbobsCOM.Recordset rsetCode;
                                string Query = "Update \"OITM\" set \"U_EMsg2\"='" + lErrStr.ToString().Replace("'", "") + "' where \"ItemCode\"='" + DocEntry + "'";
                                rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                rsetCode.DoQuery(Query);
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //     WriteLog("Process Table Update Successfully Seqid : " + Seqid.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                //    WriteLog("Process Table Update Error: Seqid: " + Seqid + " " + ex.ToString());
                            }

                            //int reusult = Integration_RunQuery_Update(Query);
                            //if (reusult == 0)
                            //{
                            //    WriteLog("Update Successfully: " + reusult);
                            //}
                            //else
                            //{
                            //    WriteLog("Update Error: " + reusult);
                            //}
                        }
                    }
                    else
                    {
                        // OCRD.Browser.ReadXml(strXml, 0);                    
                        lErrCode = OCRD.Add();
                        if (lErrCode == 0)
                        {
                            if (Global.oCompany3.InTransaction)
                                Global.oCompany3.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            //    WriteLog("Business Partner '" + DocEntry + "' Created Successfully Procees Table Seqid : " + Seqid.ToString());
                            //  DBConnection();
                            if (!Global.oCompany.InTransaction)
                                Global.oCompany.StartTransaction();
                            try
                            {
                                SAPbobsCOM.Recordset rsetCode;
                                string Query = "Update \"OITM\" set \"U_EMsg2\"='',\"U_Status2\"='Y' where \"ItemCode\"='" + DocEntry + "'";
                                rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                rsetCode.DoQuery(Query);
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //    WriteLog("Process Table Update Successfully Seqid : " + Seqid.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                //   WriteLog("Process Table Update Error: Seqid: " + Seqid + " " + ex.ToString());
                            }
                        }
                        else
                        {
                            lErrStr = Global.oCompany3.GetLastErrorDescription();
                            if (Global.oCompany3.InTransaction)
                                Global.oCompany3.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            //  WriteLog("Business Partner '" + DocEntry + "' Creation Error Message: " + lErrStr + " : Procees Table Seqid : " + Seqid.ToString());
                            // DBConnection();
                            if (!Global.oCompany.InTransaction)
                                Global.oCompany.StartTransaction();
                            try
                            {
                                SAPbobsCOM.Recordset rsetCode;
                                string Query = "Update \"OITM\" set \"U_EMsg2\"='" + lErrStr.ToString().Replace("'", "") + "' where \"ItemCode\"='" + DocEntry + "'";
                                rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                rsetCode.DoQuery(Query);
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //    WriteLog("Process Table Update Successfully Seqid : " + Seqid.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                // WriteLog("Process Table Update Error: Seqid: " + Seqid + " " + ex.ToString());
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                //   WriteLog("System : ERROR:" + ex.ToString().Replace("'", ""));
                DBConnection();


                if (!Global.oCompany.InTransaction)
                    Global.oCompany.StartTransaction();
                try
                {
                    SAPbobsCOM.Recordset rsetCode;
                    string Query = "Update \"@CW_PROCESSLOG\" set \"U_ErrCode\"='" + ex.ToString().Replace("'", "") + "',\"U_UpdDate\"=(SELECT CURRENT_DATE from Dummy) where \"U_SeqId\"='" + Seqid + "'";
                    rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    rsetCode.DoQuery(Query);
                    if (Global.oCompany.InTransaction)
                        Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    //  WriteLog("Update Successfully Seqid : " + Seqid.ToString());
                }
                catch (Exception ex1)
                {
                    if (Global.oCompany.InTransaction)
                        Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    // WriteLog("Update Error: Seqid: " + Seqid + " " + ex1.ToString().Replace("'", ""));
                }

            }
            return "";
        }

        public static void DBConnection()
        {
            try
            {
                string[] MyArr;
                string Str = "";
                try
                {
                    Global.oCompany.Disconnect();
                }
                catch (Exception ex)
                {
                    // WriteLog(ex.ToString());
                }
                Str = System.Configuration.ConfigurationSettings.AppSettings.Get("SAPConnectionString");
                MyArr = Str.Split(';');
                Global.SAPConnectionString = "server= " + MyArr[3].ToString() + ";database=" + MyArr[0].ToString() + " ;uid=" + MyArr[4].ToString() + "; pwd=" + MyArr[5].ToString() + ";";
                if ((Global.oCompany == null))
                {
                    Global.oCompany = new SAPbobsCOM.Company();
                }
                Global.oCompany.CompanyDB = MyArr[0].ToString();
                Global.oCompany.UserName = MyArr[1].ToString();
                Global.oCompany.Password = MyArr[2].ToString();
                Global.oCompany.Server = MyArr[3].ToString();
                Global.oCompany.DbUserName = MyArr[4].ToString();
                Global.oCompany.DbPassword = MyArr[5].ToString();
                string SQLType = MyArr[7];
                if (SQLType == "2008")
                {
                    Global.oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008;
                    Global.DBType = SQLType;
                }
                else if (SQLType == "2005")
                {
                    Global.oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2005;
                    Global.DBType = SQLType;
                }
                else if (SQLType == "2012")
                {
                    Global.oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012;
                    Global.DBType = SQLType;
                }
                else if (SQLType == "2014")
                {
                    Global.oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014;
                    Global.DBType = SQLType;
                }
                else if (SQLType == "2016")
                {
                    //Global.oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2016;
                    Global.DBType = SQLType;
                }
                else
                {
                    Global.oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;
                    Global.DBType = SQLType;
                }
                int connection = Global.oCompany.Connect();
                if (connection != 0)
                {
                    string Error = Global.oCompany.GetLastErrorDescription().ToString();
                    //  WriteLog(Error);
                }
                else
                {
                    //   WriteLog("Company Connected Successfully");
                }

            }
            catch (Exception ex)
            {
                // WriteLog("SetDB:" + ex.ToString());
            }

        }

        #region Application SetUp
        internal static void ApplicationSetUp()
        {
            SAPbouiCOM.SboGuiApi GuiApi = new SAPbouiCOM.SboGuiApi();
            try
            {
                string ConnString = null;
                ConnString = Environment.GetCommandLineArgs().GetValue(1).ToString();
                GuiApi.Connect(ConnString);
                Global.oApplication = GuiApi.GetApplication(-1);


            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("The SAP Buisness One Application could not be found");
                System.Environment.Exit(0);
            }
        }
        #endregion

        #region SAPCompany Connection

        internal static void ConnectCompany()
        {
            try
            {
                //Global.oApplication.MessageBox("ConnectionsIN", 1, "Ok", "", "");

                //Global.oApplication.StatusBar.SetText("x1", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);


                Global.count = 1;
                string sErrorMsg;
                Global.oCompany = new SAPbobsCOM.Company();
                Global.oCompany = (SAPbobsCOM.Company)Global.oApplication.Company.GetDICompany();

                //Global.oApplication.StatusBar.SetText("x2", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);

                string a = Global.oCompany.DbServerType.ToString();
                sErrorMsg = Global.oCompany.GetLastErrorDescription();
                int connection = Global.oCompany.Connect();

                //Global.oApplication.StatusBar.SetText("x3", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);

                //if (connection != 0)
                //{
               // Global._strCon = "Data Source=" + Global.oCompany.Server + ";Initial Catalog=" + Global.oCompany.CompanyDB + ";Persist Security Info=True;User ID=" + Global.oCompany.DbUserName + ";Password=" + Global.oCompany.DbPassword + "";

                Global.oApplication.StatusBar.SetText("Addon connected Successfully", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);

                Global.oServerType = Global.oCompany.DbServerType.ToString();

                //Global.oApplication.StatusBar.SetText(Global.oServerType.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);

                // }
                //else
                //{
                //    Global.oApplication.MessageBox(Global.oCompany.GetLastErrorDescription().ToString(), 1, "Ok", "", "");
                //}

            }
            catch (Exception ex)
            {
                Global.oApplication.StatusBar.SetText(ex.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                Global.oApplication.MessageBox(Global.oCompany.GetLastErrorDescription().ToString(), 1, "Ok", "", "");
            }
        }
        #endregion
        public static bool SRPosting(string DocEntry)
        {
            try
            {
                string MRPQry = "";
                SAPbobsCOM.Recordset RSMRP = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                if (Global.oServerType == "dst_HANADB")
                {
                    MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\" from \"ORDN\" \"M\" inner join \"RDN1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";
                }
                else
                {
                    MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\" from \"ORDN\" \"M\" inner join \"RDN1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";
                }
                RSMRP.DoQuery(MRPQry);
                if (RSMRP.RecordCount > 0)
                {
                    int lretcode;
                    string s = "";
                    string LTransId = "";
                    SAPbobsCOM.Documents Doc;
                    Doc = Global.oCompany1.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);
                    if (!Global.oCompany1.InTransaction)
                        Global.oCompany1.StartTransaction();
                    // Doc.CardCode = RSMRP.Fields.Item("CardCode").Value;
                    Doc.DocDate = RSMRP.Fields.Item("DocDate").Value;
                    Doc.NumAtCard = Convert.ToString(RSMRP.Fields.Item("DocNum").Value);
                    //Doc.DocDueDate = RSMRP.Fields.Item("DocDueDate").Value;
                    //Doc.TaxDate = RSMRP.Fields.Item("TaxDate").Value;
                    //double DocRate = RSMRP.Fields.Item("DiscPrcnt").Value;
                    //if (DocRate.ToString().Trim() == "")
                    //{
                    //    DocRate = 0;
                    //}
                    //if (Convert.ToDouble(DocRate) != 0)
                    //{
                    //    Doc.DiscountPercent = Convert.ToDouble(DocRate);
                    //}
                    Doc.UserFields.Fields.Item("U_POEntry").Value = Convert.ToString(RSMRP.Fields.Item("DocEntry").Value);
                    Doc.UserFields.Fields.Item("U_PODocNum").Value = Convert.ToString(RSMRP.Fields.Item("DocNum").Value);
                    Doc.UserFields.Fields.Item("U_POSeries").Value = Convert.ToString(RSMRP.Fields.Item("Series").Value);
                    //string DocEntry = Convert.ToString(RSMRP.Fields.Item("DocEntry").Value);
                    //Doc.Memo = "D JE P&L Checking";
                    //  string Months = Root.Instance.frmAccuralSheet.Items.Item("4").Specific.value;
                    //  string Years = Root.Instance.frmAccuralSheet.Items.Item("6").Specific.value                   
                    SAPbobsCOM.Recordset oRS = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    //if (Global.oServerType == "dst_HANADB")
                    //{
                    //    MRPQry = "select \"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\",\"M1\".\"OcrCode\",\"M1\".\"OcrCode2\",\"M1\".\"OcrCode3\",\"M1\".\"OcrCode4\",\"M1\".\"OcrCode5\",\"M1\".\"LocCode\",ifnull(M3.\"BaseLine\",0) as \"BaseL\",ifnull(M3.\"BaseEntry\",0) as \"BaseE\" from \"ORDN\" \"M\" inner join \"RDN1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" Left join PDN1 M3 on cast(M1.\"DocEntry\" as varchar(40))=cast(M1.\"U_POEntry\" as varchar(40)) and cast(M1.\"LineNum\" as varchar(40))=cast(M1.\"U_LineNum\" as varchar(40)) where \"M\".\"DocEntry\"='" + DocEntry + "'";
                    //}
                    //else
                    //{
                    //    MRPQry = "select \"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\",\"M1\".\"OcrCode\",\"M1\".\"OcrCode2\",\"M1\".\"OcrCode3\",\"M1\".\"OcrCode4\",\"M1\".\"OcrCode5\",\"M1\".\"LocCode\",isnull(M3.\"BaseLine\",0) as \"BaseL\",isnull(M3.\"BaseEntry\",0) as \"BaseE\"  from \"ORDN\" \"M\" inner join \"RDN1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" Left join PDN1 M3 on cast(M1.\"DocEntry\" as varchar(40))=cast(M1.\"U_POEntry\" as varchar(40)) and cast(M1.\"LineNum\" as varchar(40))=cast(M1.\"U_LineNum\" as varchar(40)) where \"M\".\"DocEntry\"='" + DocEntry + "'";
                    //}
                    if (Global.oServerType == "dst_HANADB")
                    {
                        MRPQry = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\",\"M1\".\"OcrCode\",\"M1\".\"OcrCode2\",\"M1\".\"OcrCode3\",\"M1\".\"OcrCode4\",\"M1\".\"OcrCode5\",\"M1\".\"LocCode\" from \"ORDN\" \"M\" inner join \"RDN1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\"  where \"M\".\"DocEntry\"='" + DocEntry + "'";
                    }
                    else
                    {
                        MRPQry = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\",\"M1\".\"OcrCode\",\"M1\".\"OcrCode2\",\"M1\".\"OcrCode3\",\"M1\".\"OcrCode4\",\"M1\".\"OcrCode5\",\"M1\".\"LocCode\"  from \"ORDN\" \"M\" inner join \"RDN1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\"  where \"M\".\"DocEntry\"='" + DocEntry + "'";
                    }
                    oRS.DoQuery(MRPQry);
                    if (oRS.RecordCount > 0)
                    {
                        double LabrCst = 0;
                        for (int l = 1; l <= oRS.RecordCount; l++)
                        {
                            //  LabrCst = LabrCst + Convert.ToDouble(oRS.Fields.Item("DocTotal").Value);
                            //if (LabrCst != 0)
                            //{
                            // - - - - - Line Details Started - - - -
                            Doc.Lines.ItemCode = oRS.Fields.Item("ItemCode").Value;
                            Doc.Lines.Quantity = oRS.Fields.Item("Quantity").Value;
                            Doc.Lines.UnitPrice = 0;   //oRS.Fields.Item("Price").Value;
                            Doc.Lines.WarehouseCode = oRS.Fields.Item("WhsCode").Value;
                            Doc.Lines.UoMEntry = oRS.Fields.Item("UomEntry").Value;
                            // Doc.Lines.LocationCode = oRS.Fields.Item("LocCode").Value;
                            //string POLine = Convert.ToString(oRS.Fields.Item("BaseL").Value);
                            //string PODocEntry = Convert.ToString(oRS.Fields.Item("BaseE").Value);
                            //if (POLine.ToString().Trim() != "0" & PODocEntry.ToString().Trim() != "0")
                            //{
                            //    Doc.Lines.BaseEntry = Convert.ToInt32(PODocEntry);
                            //    Doc.Lines.BaseLine = Convert.ToInt32(POLine);
                            //    Doc.Lines.BaseType = 22;
                            //}
                            Doc.Lines.UserFields.Fields.Item("U_LineNum").Value = Convert.ToString(oRS.Fields.Item("LineNum").Value);
                            Doc.Lines.UserFields.Fields.Item("U_POEntry").Value = DocEntry.ToString();// Convert.ToString(RSMRP.Fields.Item("DocEntry").Value);
                            //double LDocRate = oRS.Fields.Item("DiscPrcnt").Value;
                            //if (LDocRate.ToString().Trim() == "")
                            //{
                            //    DocRate = 0;
                            //}
                            //if (Convert.ToDouble(DocRate) != 0)
                            //{
                            //    Doc.DiscountPercent = Convert.ToDouble(DocRate);
                            //}
                            string GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode = oRS.Fields.Item("OcrCode").Value;
                            }
                            GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode2").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode2 = oRS.Fields.Item("OcrCode2").Value;
                            }
                            GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode3").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode3 = oRS.Fields.Item("OcrCode3").Value;
                            }
                            GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode4").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode4 = oRS.Fields.Item("OcrCode4").Value;
                            }
                            GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode5").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode5 = oRS.Fields.Item("OcrCode5").Value;
                            }
                            SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                            if (Global.oServerType == "dst_HANADB")
                            {
                                MRPQry = "select \"BatchNum\",\"WhsCode\",\"Quantity\" from IBT1 Where \"BaseType\"='16' and \"BaseEntry\"='" + DocEntry.ToString() + "' and \"BaseLinNum\"='" + Convert.ToString(oRS.Fields.Item("LineNum").Value) + "' ";
                            }
                            else
                            {
                                MRPQry = "select \"BatchNum\",\"WhsCode\",\"Quantity\" from IBT1 Where \"BaseType\"='16' and \"BaseEntry\"='" + DocEntry.ToString() + "' and \"BaseLinNum\"='" + Convert.ToString(oRS.Fields.Item("LineNum").Value) + "' ";
                            }
                            oRS1.DoQuery(MRPQry);
                            if (oRS1.RecordCount > 0)
                            {
                                for (int k = 1; k <= oRS1.RecordCount; k++)
                                {
                                    Doc.Lines.BatchNumbers.BatchNumber = Convert.ToString(oRS1.Fields.Item("BatchNum").Value);
                                    Doc.Lines.BatchNumbers.Quantity = Convert.ToDouble(oRS1.Fields.Item("Quantity").Value);
                                    Doc.Lines.BatchNumbers.Add();
                                    oRS1.MoveNext();
                                }
                            }
                            Doc.Lines.Add();

                            // }
                            oRS.MoveNext();
                        }
                        lretcode = Doc.Add();
                        if (lretcode != 0)
                        {
                            if (Global.oCompany1.InTransaction)
                                Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            s = Global.oCompany1.GetLastErrorDescription();
                            Global.oApplication.SetStatusBarMessage("Goods Receipt Is Not Posted Due To : " + s, SAPbouiCOM.BoMessageTime.bmt_Short, true);
                            try
                            {
                                SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                if (Global.oServerType == "dst_HANADB")
                                {
                                    MRPQry = "Update \"ORDN\" set  \"U_EMsg\"='" + s.Replace("'", "").ToString() + "' where \"DocEntry\"='" + DocEntry + "'";
                                }
                                else
                                {
                                    MRPQry = "Update ORDN set  U_EMsg='" + s.Replace("'", "").ToString() + "' where DocEntry='" + DocEntry + "'";
                                }
                                oRS1.DoQuery(MRPQry);
                            }
                            catch (Exception x)
                            {

                            }
                            // return false;
                        }
                        else
                        {
                            LTransId = Global.oCompany1.GetNewObjectKey();
                            Global.oApplication.SetStatusBarMessage("Goods Receipts Is : " + LTransId, SAPbouiCOM.BoMessageTime.bmt_Short, false);
                            // Root.Instance.frmAccuralSheet.Items.Item("14").Specific.value = LTransId.ToString();
                            if (Global.oCompany1.InTransaction)
                                Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            try
                            {

                                SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                if (Global.oServerType == "dst_HANADB")
                                {
                                    MRPQry = "Update \"ORDN\" set  \"U_Status\"='Y',\"U_EMsg\"='' where \"DocEntry\"='" + DocEntry + "'";
                                }
                                else
                                {
                                    MRPQry = "Update ORDN set  U_Status='Y',U_EMsg='' where DocEntry='" + DocEntry + "'";
                                }
                                oRS1.DoQuery(MRPQry);
                                SAPbobsCOM.Recordset RSS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                MRPQry = "SELECT T0.\"DocEntry\",\"U_Status\" FROM OIGE T0 where T0.\"U_POEntry\"='" + DocEntry + "' ";
                                RSS1.DoQuery(MRPQry);
                                if (RSS1.RecordCount == 0)
                                {
                                    GIPosting(DocEntry);
                                }

                            }
                            catch (Exception x)
                            {

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.oApplication.SetStatusBarMessage(ex.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, true);
                return false;
            }
            return true;
        }
        public static bool GIPosting(string DocEntry)
        {
            try
            {
                string MRPQry = "";
                SAPbobsCOM.Recordset RSMRP = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                if (Global.oServerType == "dst_HANADB")
                {
                    MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\" from \"ORDN\" \"M\" inner join \"RDN1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";
                }
                else
                {
                    MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\" from \"ORDN\" \"M\" inner join \"RDN1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";
                }
                RSMRP.DoQuery(MRPQry);
                if (RSMRP.RecordCount > 0)
                {
                    int lretcode;
                    string s = "";
                    string LTransId = "";
                    SAPbobsCOM.Documents Doc;
                    Doc = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenExit);
                    if (!Global.oCompany.InTransaction)
                        Global.oCompany.StartTransaction();
                    // Doc.CardCode = RSMRP.Fields.Item("CardCode").Value;
                    Doc.DocDate = RSMRP.Fields.Item("DocDate").Value;
                    Doc.NumAtCard = Convert.ToString(RSMRP.Fields.Item("DocNum").Value);
                    //Doc.DocDueDate = RSMRP.Fields.Item("DocDueDate").Value;
                    //Doc.TaxDate = RSMRP.Fields.Item("TaxDate").Value;
                    //double DocRate = RSMRP.Fields.Item("DiscPrcnt").Value;
                    //if (DocRate.ToString().Trim() == "")
                    //{
                    //    DocRate = 0;
                    //}
                    //if (Convert.ToDouble(DocRate) != 0)
                    //{
                    //    Doc.DiscountPercent = Convert.ToDouble(DocRate);
                    //}
                    Doc.UserFields.Fields.Item("U_POEntry").Value = Convert.ToString(RSMRP.Fields.Item("DocEntry").Value);
                    Doc.UserFields.Fields.Item("U_PODocNum").Value = Convert.ToString(RSMRP.Fields.Item("DocNum").Value);
                    Doc.UserFields.Fields.Item("U_POSeries").Value = Convert.ToString(RSMRP.Fields.Item("Series").Value);
                    //string DocEntry = Convert.ToString(RSMRP.Fields.Item("DocEntry").Value);
                    //Doc.Memo = "D JE P&L Checking";
                    //  string Months = Root.Instance.frmAccuralSheet.Items.Item("4").Specific.value;
                    //  string Years = Root.Instance.frmAccuralSheet.Items.Item("6").Specific.value                   
                    SAPbobsCOM.Recordset oRS = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    //if (Global.oServerType == "dst_HANADB")
                    //{
                    //    MRPQry = "select \"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\",\"M1\".\"OcrCode\",\"M1\".\"OcrCode2\",\"M1\".\"OcrCode3\",\"M1\".\"OcrCode4\",\"M1\".\"OcrCode5\",\"M1\".\"LocCode\",ifnull(M3.\"BaseLine\",0) as \"BaseL\",ifnull(M3.\"BaseEntry\",0) as \"BaseE\" from \"ORDN\" \"M\" inner join \"RDN1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" Left join PDN1 M3 on cast(M1.\"DocEntry\" as varchar(40))=cast(M1.\"U_POEntry\" as varchar(40)) and cast(M1.\"LineNum\" as varchar(40))=cast(M1.\"U_LineNum\" as varchar(40)) where \"M\".\"DocEntry\"='" + DocEntry + "'";
                    //}
                    //else
                    //{
                    //    MRPQry = "select \"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\",\"M1\".\"OcrCode\",\"M1\".\"OcrCode2\",\"M1\".\"OcrCode3\",\"M1\".\"OcrCode4\",\"M1\".\"OcrCode5\",\"M1\".\"LocCode\",isnull(M3.\"BaseLine\",0) as \"BaseL\",isnull(M3.\"BaseEntry\",0) as \"BaseE\"  from \"ORDN\" \"M\" inner join \"RDN1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" Left join PDN1 M3 on cast(M1.\"DocEntry\" as varchar(40))=cast(M1.\"U_POEntry\" as varchar(40)) and cast(M1.\"LineNum\" as varchar(40))=cast(M1.\"U_LineNum\" as varchar(40)) where \"M\".\"DocEntry\"='" + DocEntry + "'";
                    //}
                    if (Global.oServerType == "dst_HANADB")
                    {
                        MRPQry = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\",\"M1\".\"OcrCode\",\"M1\".\"OcrCode2\",\"M1\".\"OcrCode3\",\"M1\".\"OcrCode4\",\"M1\".\"OcrCode5\",\"M1\".\"LocCode\" from \"ORDN\" \"M\" inner join \"RDN1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\"  where \"M\".\"DocEntry\"='" + DocEntry + "'";
                    }
                    else
                    {
                        MRPQry = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\",\"M1\".\"OcrCode\",\"M1\".\"OcrCode2\",\"M1\".\"OcrCode3\",\"M1\".\"OcrCode4\",\"M1\".\"OcrCode5\",\"M1\".\"LocCode\"  from \"ORDN\" \"M\" inner join \"RDN1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\"  where \"M\".\"DocEntry\"='" + DocEntry + "'";
                    }
                    oRS.DoQuery(MRPQry);
                    if (oRS.RecordCount > 0)
                    {
                        double LabrCst = 0;
                        for (int l = 1; l <= oRS.RecordCount; l++)
                        {
                            //  LabrCst = LabrCst + Convert.ToDouble(oRS.Fields.Item("DocTotal").Value);
                            //if (LabrCst != 0)
                            //{
                            // - - - - - Line Details Started - - - -
                            Doc.Lines.ItemCode = oRS.Fields.Item("ItemCode").Value;
                            Doc.Lines.Quantity = oRS.Fields.Item("Quantity").Value;
                            // Doc.Lines.UnitPrice = 0;   //oRS.Fields.Item("Price").Value;
                            Doc.Lines.WarehouseCode = oRS.Fields.Item("WhsCode").Value;
                            Doc.Lines.UoMEntry = oRS.Fields.Item("UomEntry").Value;
                            // Doc.Lines.LocationCode = oRS.Fields.Item("LocCode").Value;
                            //string POLine = Convert.ToString(oRS.Fields.Item("BaseL").Value);
                            //string PODocEntry = Convert.ToString(oRS.Fields.Item("BaseE").Value);
                            //if (POLine.ToString().Trim() != "0" & PODocEntry.ToString().Trim() != "0")
                            //{
                            //    Doc.Lines.BaseEntry = Convert.ToInt32(PODocEntry);
                            //    Doc.Lines.BaseLine = Convert.ToInt32(POLine);
                            //    Doc.Lines.BaseType = 22;
                            //}
                            Doc.Lines.UserFields.Fields.Item("U_LineNum").Value = Convert.ToString(oRS.Fields.Item("LineNum").Value);
                            Doc.Lines.UserFields.Fields.Item("U_POEntry").Value = DocEntry.ToString();// Convert.ToString(RSMRP.Fields.Item("DocEntry").Value);
                            //double LDocRate = oRS.Fields.Item("DiscPrcnt").Value;
                            //if (LDocRate.ToString().Trim() == "")
                            //{
                            //    DocRate = 0;
                            //}
                            //if (Convert.ToDouble(DocRate) != 0)
                            //{
                            //    Doc.DiscountPercent = Convert.ToDouble(DocRate);
                            //}
                            string GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode = oRS.Fields.Item("OcrCode").Value;
                            }
                            GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode2").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode2 = oRS.Fields.Item("OcrCode2").Value;
                            }
                            GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode3").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode3 = oRS.Fields.Item("OcrCode3").Value;
                            }
                            GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode4").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode4 = oRS.Fields.Item("OcrCode4").Value;
                            }
                            GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode5").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode5 = oRS.Fields.Item("OcrCode5").Value;
                            }
                            SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                            if (Global.oServerType == "dst_HANADB")
                            {
                                MRPQry = "select \"BatchNum\",\"WhsCode\",\"Quantity\" from IBT1 Where \"BaseType\"='16' and \"BaseEntry\"='" + DocEntry.ToString() + "' and \"BaseLinNum\"='" + Convert.ToString(oRS.Fields.Item("LineNum").Value) + "' ";
                            }
                            else
                            {
                                MRPQry = "select \"BatchNum\",\"WhsCode\",\"Quantity\" from IBT1 Where \"BaseType\"='16' and \"BaseEntry\"='" + DocEntry.ToString() + "' and \"BaseLinNum\"='" + Convert.ToString(oRS.Fields.Item("LineNum").Value) + "' ";
                            }
                            oRS1.DoQuery(MRPQry);
                            if (oRS1.RecordCount > 0)
                            {
                                for (int k = 1; k <= oRS1.RecordCount; k++)
                                {
                                    Doc.Lines.BatchNumbers.BatchNumber = Convert.ToString(oRS1.Fields.Item("BatchNum").Value);
                                    Doc.Lines.BatchNumbers.Quantity = Convert.ToDouble(oRS1.Fields.Item("Quantity").Value);
                                    Doc.Lines.BatchNumbers.Add();
                                    oRS1.MoveNext();
                                }
                            }
                            Doc.Lines.Add();

                            // }
                            oRS.MoveNext();
                        }
                        lretcode = Doc.Add();
                        if (lretcode != 0)
                        {
                            if (Global.oCompany.InTransaction)
                                Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            s = Global.oCompany.GetLastErrorDescription();
                            Global.oApplication.SetStatusBarMessage("Goods Issue Is Not Posted Due To : " + s, SAPbouiCOM.BoMessageTime.bmt_Short, true);
                            try
                            {
                                SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                if (Global.oServerType == "dst_HANADB")
                                {
                                    MRPQry = "Update \"ORDN\" set  \"U_EMsg1\"='" + s.Replace("'", "").ToString() + "' where \"DocEntry\"='" + DocEntry + "'";
                                }
                                else
                                {
                                    MRPQry = "Update ORDN set  U_EMsg1='" + s.Replace("'", "").ToString() + "' where DocEntry='" + DocEntry + "'";
                                }
                                oRS1.DoQuery(MRPQry);
                            }
                            catch (Exception x)
                            {

                            }
                            // return false;
                        }
                        else
                        {
                            LTransId = Global.oCompany.GetNewObjectKey();
                            Global.oApplication.SetStatusBarMessage("Goods Issue Is : " + LTransId, SAPbouiCOM.BoMessageTime.bmt_Short, false);
                            // Root.Instance.frmAccuralSheet.Items.Item("14").Specific.value = LTransId.ToString();
                            if (Global.oCompany.InTransaction)
                                Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            try
                            {
                                SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                if (Global.oServerType == "dst_HANADB")
                                {
                                    MRPQry = "Update \"ORDN\" set  \"U_Status1\"='Y',\"U_EMsg1\"='' where \"DocEntry\"='" + DocEntry + "'";
                                }
                                else
                                {
                                    MRPQry = "Update ORDN set  U_Status1='Y',U_EMsg1='' where DocEntry='" + DocEntry + "'";
                                }
                                oRS1.DoQuery(MRPQry);
                            }
                            catch (Exception x)
                            {

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.oApplication.SetStatusBarMessage(ex.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, true);
                return false;
            }
            return true;
        }
        public static bool GRPOPosting(string DocEntry)
        {
            try
            {
                string MRPQry = "";
                SAPbobsCOM.Recordset RSMRP = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                if (Global.oServerType == "dst_HANADB")
                {
                    MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\" from \"OPDN\" \"M\" inner join \"PDN1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";
                }
                else
                {
                    MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\" from \"OPDN\" \"M\" inner join \"PDN1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";
                }
                RSMRP.DoQuery(MRPQry);
                if (RSMRP.RecordCount > 0)
                {
                    int lretcode;
                    string s = "";
                    string LTransId = "";
                    SAPbobsCOM.Documents Doc;
                    Doc = Global.oCompany1.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseDeliveryNotes);
                    if (!Global.oCompany1.InTransaction)
                        Global.oCompany1.StartTransaction();
                    Doc.CardCode = RSMRP.Fields.Item("CardCode").Value;
                    Doc.DocDate = RSMRP.Fields.Item("DocDate").Value;
                    Doc.DocDueDate = RSMRP.Fields.Item("DocDueDate").Value;
                    Doc.TaxDate = RSMRP.Fields.Item("TaxDate").Value;
                    double DocRate = RSMRP.Fields.Item("DiscPrcnt").Value;
                    if (DocRate.ToString().Trim() == "")
                    {
                        DocRate = 0;
                    }
                    if (Convert.ToDouble(DocRate) != 0)
                    {
                        Doc.DiscountPercent = Convert.ToDouble(DocRate);
                    }
                    Doc.UserFields.Fields.Item("U_POEntry").Value = Convert.ToString(RSMRP.Fields.Item("DocEntry").Value);
                    Doc.UserFields.Fields.Item("U_PODocNum").Value = Convert.ToString(RSMRP.Fields.Item("DocNum").Value);
                    Doc.UserFields.Fields.Item("U_POSeries").Value = Convert.ToString(RSMRP.Fields.Item("Series").Value);
                    //Doc.Memo = "D JE P&L Checking";
                    //  string Months = Root.Instance.frmAccuralSheet.Items.Item("4").Specific.value;
                    //  string Years = Root.Instance.frmAccuralSheet.Items.Item("6").Specific.value                   
                    SAPbobsCOM.Recordset oRS = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    if (Global.oServerType == "dst_HANADB")
                    {
                        MRPQry = "select \"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\",\"M1\".\"OcrCode\",\"M1\".\"OcrCode2\",\"M1\".\"OcrCode3\",\"M1\".\"OcrCode4\",\"M1\".\"OcrCode5\",\"M1\".\"LocCode\",M3.\"BaseLine\" as \"BaseL\",M3.\"BaseEntry\" as \"BaseE\" from \"OPDN\" \"M\" inner join \"PDN1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" Left join RDR1 M3 on cast(M1.\"DocEntry\" as varchar(40))=cast(M1.\"U_POEntry\" as varchar(40)) and cast(M1.\"LineNum\" as varchar(40))=cast(M1.\"U_LineNum\" as varchar(40)) where \"M\".\"DocEntry\"='" + DocEntry + "'";
                    }
                    else
                    {
                        MRPQry = "select \"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\",\"M1\".\"OcrCode\",\"M1\".\"OcrCode2\",\"M1\".\"OcrCode3\",\"M1\".\"OcrCode4\",\"M1\".\"OcrCode5\",\"M1\".\"LocCode\",M3.\"BaseLine\" as \"BaseL\",M3.\"BaseEntry\" as \"BaseE\"  from \"OPDN\" \"M\" inner join \"PDN1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" Left join RDR1 M3 on cast(M1.\"DocEntry\" as varchar(40))=cast(M1.\"U_POEntry\" as varchar(40)) and cast(M1.\"LineNum\" as varchar(40))=cast(M1.\"U_LineNum\" as varchar(40)) where \"M\".\"DocEntry\"='" + DocEntry + "'";
                    }
                    oRS.DoQuery(MRPQry);
                    if (oRS.RecordCount > 0)
                    {
                        double LabrCst = 0;
                        for (int l = 1; l <= oRS.RecordCount; l++)
                        {
                            //  LabrCst = LabrCst + Convert.ToDouble(oRS.Fields.Item("DocTotal").Value);
                            //if (LabrCst != 0)
                            //{
                            // - - - - - Line Details Started - - - -
                            Doc.Lines.ItemCode = oRS.Fields.Item("ItemCode").Value;
                            Doc.Lines.Quantity = oRS.Fields.Item("Quantity").Value;
                            Doc.Lines.UnitPrice = oRS.Fields.Item("Price").Value;
                            Doc.Lines.WarehouseCode = oRS.Fields.Item("WhsCode").Value;
                            Doc.Lines.LocationCode = oRS.Fields.Item("LocCode").Value;
                            string POLine = Convert.ToString(oRS.Fields.Item("BaseL").Value);
                            string PODocEntry = Convert.ToString(oRS.Fields.Item("BaseE").Value);
                            if (POLine.ToString().Trim() != "" & PODocEntry.ToString().Trim() != "")
                            {
                                Doc.Lines.BaseEntry = Convert.ToInt32(PODocEntry);
                                Doc.Lines.BaseLine = Convert.ToInt32(POLine);
                                Doc.Lines.BaseType = 22;
                            }
                            Doc.Lines.UserFields.Fields.Item("U_LineNum").Value = Convert.ToString(oRS.Fields.Item("LineNum").Value);
                            Doc.UserFields.Fields.Item("U_POEntry").Value = Convert.ToString(RSMRP.Fields.Item("DocEntry").Value);
                            double LDocRate = oRS.Fields.Item("DiscPrcnt").Value;
                            if (LDocRate.ToString().Trim() == "")
                            {
                                DocRate = 0;
                            }
                            if (Convert.ToDouble(DocRate) != 0)
                            {
                                Doc.DiscountPercent = Convert.ToDouble(DocRate);
                            }
                            string GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode = oRS.Fields.Item("OcrCode").Value;
                            }
                            GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode2").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode2 = oRS.Fields.Item("OcrCode2").Value;
                            }
                            GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode3").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode3 = oRS.Fields.Item("OcrCode3").Value;
                            }
                            GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode4").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode4 = oRS.Fields.Item("OcrCode4").Value;
                            }
                            GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode5").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode5 = oRS.Fields.Item("OcrCode5").Value;
                            }
                            Doc.Lines.Add();

                            // }
                            oRS.MoveNext();
                        }
                        lretcode = Doc.Add();
                        if (lretcode != 0)
                        {
                            if (Global.oCompany1.InTransaction)
                                Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            s = Global.oCompany1.GetLastErrorDescription();
                            Global.oApplication.SetStatusBarMessage("Delivery Is Not Posted Due To : " + s, SAPbouiCOM.BoMessageTime.bmt_Short, true);
                            try
                            {
                                SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                if (Global.oServerType == "dst_HANADB")
                                {
                                    MRPQry = "Update \"OPDN\" set  \"U_EMsg\"='" + s.Replace("'", "").ToString() + "' where \"DocEntry\"='" + DocEntry + "'";
                                }
                                else
                                {
                                    MRPQry = "Update OPDN set  U_EMsg='" + s.Replace("'", "").ToString() + "' where DocEntry='" + DocEntry + "'";
                                }
                                oRS1.DoQuery(MRPQry);
                            }
                            catch (Exception x)
                            {

                            }
                            // return false;
                        }
                        else
                        {
                            LTransId = Global.oCompany1.GetNewObjectKey();
                            Global.oApplication.SetStatusBarMessage("Sales Order Is : " + LTransId, SAPbouiCOM.BoMessageTime.bmt_Short, false);
                            // Root.Instance.frmAccuralSheet.Items.Item("14").Specific.value = LTransId.ToString();
                            if (Global.oCompany1.InTransaction)
                                Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            try
                            {
                                SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                if (Global.oServerType == "dst_HANADB")
                                {
                                    MRPQry = "Update \"OPDN\" set  \"U_Status\"='Y',\"U_EMsg\"='' where \"DocEntry\"='" + DocEntry + "'";
                                }
                                else
                                {
                                    MRPQry = "Update OPDN set  U_Status='Y',U_EMsg='' where DocEntry='" + DocEntry + "'";
                                }
                                oRS1.DoQuery(MRPQry);
                            }
                            catch (Exception x)
                            {

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.oApplication.SetStatusBarMessage(ex.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, true);
                return false;
            }
            return true;
        }
        public static bool SO2POPosting(string DocEntry, string NumAtCard, string VCode, string ToWarehouse)
        {
            try
            {
                string MRPQry = "";
                SAPbobsCOM.Recordset RSMRP = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                if (Global.oServerType == "dst_HANADB")
                {
                    MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\",\"M\".\"Comments\" from \"ODRF\" \"M\" inner join \"DRF1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";

                }
                else
                {
                    MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\",\"M\".\"Comments\" from \"ODRF\" \"M\" inner join \"DRF1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";

                    // MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\",\"M\".\"Comments\" from \"ODRF\" \"M\" inner join \"DRF1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";
                }
                RSMRP.DoQuery(MRPQry);
                if (RSMRP.RecordCount > 0)
                {
                    int lretcode;
                    string s = "";
                    string LTransId = "";
                    string Intercompany = getSingleValue("Select \"U_VSPLIC\" from \"OCRD\" where \"CardCode\"='" + RSMRP.Fields.Item("CardCode").Value + "'");
                    string InterBranch = getSingleValue("Select \"U_VSPLIB\" from \"OCRD\" where \"CardCode\"='" + RSMRP.Fields.Item("CardCode").Value + "'");
                    if (InterBranch == "No" && Intercompany == "Yes")
                    {
                        SAPbobsCOM.Documents Doc;
                        Doc = Global.oCompany1.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts);
                        if (!Global.oCompany1.InTransaction)
                            Global.oCompany1.StartTransaction();
                        Doc.DocObjectCode = SAPbobsCOM.BoObjectTypes.oPurchaseOrders;

                        Doc.CardCode = VCode;//RSMRP.Fields.Item("CardCode").Value;
                        Doc.NumAtCard = NumAtCard; //Convert.ToString(RSMRP.Fields.Item("DocNum").Value);
                        Doc.DocDate = RSMRP.Fields.Item("DocDate").Value;
                        Doc.DocDueDate = RSMRP.Fields.Item("DocDueDate").Value;
                        Doc.TaxDate = RSMRP.Fields.Item("TaxDate").Value;
                        Doc.Comments = RSMRP.Fields.Item("Comments").Value;
                        double DocRate = RSMRP.Fields.Item("DiscPrcnt").Value;
                        Doc.UserFields.Fields.Item("U_SOEntry").Value = DocEntry;
                        if (DocRate.ToString().Trim() == "")
                        {
                            DocRate = 0;
                        }
                        if (Convert.ToDouble(DocRate) != 0)
                        {
                            Doc.DiscountPercent = Convert.ToDouble(DocRate);
                        }

                        SAPbobsCOM.Recordset oRS = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                        if (Global.oServerType == "dst_HANADB")
                        {

                            MRPQry = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\"   from \"ODRF\" \"M\" inner join \"DRF1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" inner join \"OITM\" \"M2\" on \"M1\".\"ItemCode\"=\"M2\".\"ItemCode\" Inner join OITB M3 on \"M2\".\"ItmsGrpCod\"=\"M3\".\"ItmsGrpCod\" where \"M\".\"DocEntry\"='" + DocEntry + "' ";

                            // MRPQry = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\"   from \"ODRF\" \"M\" inner join \"DRF1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" inner join \"OITM\" \"M2\" on \"M1\".\"ItemCode\"=\"M2\".\"ItemCode\" Inner join OITB M3 on \"M2\".\"ItmsGrpCod\"=\"M3\".\"ItmsGrpCod\" where \"M\".\"DocEntry\"='" + DocEntry + "' ";
                        }
                        else
                        {

                            MRPQry = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\"   from \"ODRF\" \"M\" inner join \"DRF1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" inner join \"OITM\" \"M2\" on \"M1\".\"ItemCode\"=\"M2\".\"ItemCode\" Inner join OITB M3 on \"M2\".\"ItmsGrpCod\"=\"M3\".\"ItmsGrpCod\" where \"M\".\"DocEntry\"='" + DocEntry + "' ";

                            // MRPQry = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\"  from \"ODRF\" \"M\" inner join \"DRF1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" inner join \"OITM\" \"M2\" on \"M1\".\"ItemCode\"=\"M2\".\"ItemCode\" Inner join OITB M3 on \"M2\".\"ItmsGrpCod\"=\"M3\".\"ItmsGrpCod\" where \"M\".\"DocEntry\"='" + DocEntry + "' ";
                        }
                        oRS.DoQuery(MRPQry);
                        if (oRS.RecordCount > 0)
                        {
                            double LabrCst = 0;
                            for (int l = 1; l <= oRS.RecordCount; l++)
                            {
                                string Barcode = getSingleValue("SELECT T0.\"CodeBars\" FROM OITM T0 WHERE T0.\"ItemCode\" ='" + oRS.Fields.Item("ItemCode").Value + "'");
                                string StockCode = getSingleValue1("SELECT T0.\"ItemCode\" FROM OITM T0 WHERE T0.\"CodeBars\" ='" + Barcode + "'");

                                Doc.Lines.ItemCode = StockCode; //oRS.Fields.Item("ItemCode").Value;
                                Doc.Lines.Quantity = oRS.Fields.Item("Quantity").Value;
                                Doc.Lines.UnitPrice = oRS.Fields.Item("Price").Value;
                                Doc.Lines.WarehouseCode = ToWarehouse;


                                string LineId = oRS.Fields.Item("LineNum").Value;  // getSingleValue1("SELECT T0.\"LineNum\" FROM DRF1 T0 WHERE T0.\"ItemCode\" ='" + oRS.Fields.Item("ItemCode").Value + "' and T0.\"DocEntry\" ='" + DocEntry + "'");
                                Doc.Lines.UserFields.Fields.Item("U_BaseLine").Value = LineId;
                                Doc.Lines.UserFields.Fields.Item("U_BaseEntry").Value = DocEntry;
                                double LDocRate = oRS.Fields.Item("DiscPrcnt").Value;
                                if (LDocRate.ToString().Trim() == "")
                                {
                                    LDocRate = 0;
                                }
                                if (Convert.ToDouble(LDocRate) != 0)
                                {
                                    Doc.Lines.DiscountPercent = Convert.ToDouble(LDocRate);
                                }

                                Doc.Lines.Add();

                                // }
                                oRS.MoveNext();
                            }
                            string FrightAmt = getSingleValue("SELECT T0.\"LineTotal\" FROM DRF3 T0 WHERE T0.\"DocEntry\"='" + DocEntry + "' and ifnull(T0.\"LineTotal\",0)>0 ");

                            //string FrightAmt = getSingleValue("SELECT T0.\"LineTotal\" FROM DRF2 T0 WHERE T0\"DocEntry\"='" + oRS.Fields.Item("ItemCode").Value + "' where ifnull(T0.\"ExpnsCode\",'')='' ");
                            string TaxCode = getSingleValue("SELECT T0.\"TaxCode\" FROM DRF3 T0 WHERE T0.\"DocEntry\"='" + DocEntry + "' and ifnull(T0.\"LineTotal\",0)>0 ");


                            if (FrightAmt != "" && FrightAmt != null)
                            {
                                Doc.Expenses.ExpenseCode = 1;
                                Doc.Expenses.LineTotal = Convert.ToDouble(FrightAmt);
                                Doc.Expenses.TaxCode = TaxCode;
                                Doc.Expenses.Add();
                            }
                            string BPLID = getSingleValue1("SELECT T0.\"BPLid\" FROM OWHS T0 WHERE T0.\"WhsCode\" ='" + ToWarehouse + "'");
                            Doc.BPL_IDAssignedToInvoice = Convert.ToInt32(BPLID);
                            lretcode = Doc.Add();
                            if (lretcode != 0)
                            {
                                if (Global.oCompany1.InTransaction)
                                    Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                s = Global.oCompany1.GetLastErrorDescription();
                                Global.oApplication.SetStatusBarMessage("Purchase Order Is Not Posted Due To : " + s, SAPbouiCOM.BoMessageTime.bmt_Short, true);
                                try
                                {
                                    SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                    if (Global.oServerType == "dst_HANADB")
                                    {

                                        MRPQry = "Update \"ODRF\" set  \"U_EMsg\"='" + s.Replace("'", "").ToString() + "' where \"DocEntry\"='" + DocEntry + "'";

                                    }
                                    else
                                    {

                                        MRPQry = "Update \"ODRF\" set  \"U_EMsg\"='" + s.Replace("'", "").ToString() + "' where \"DocEntry\"='" + DocEntry + "'";

                                        //MRPQry = "Update ORDR set  U_EMsg='" + s.Replace("'", "").ToString() + "' where DocEntry='" + DocEntry + "'";
                                    }
                                    oRS1.DoQuery(MRPQry);
                                }
                                catch (Exception x)
                                {

                                }
                                // return false;
                            }
                            else
                            {
                                LTransId = Global.oCompany1.GetNewObjectKey();
                                Global.oApplication.SetStatusBarMessage("Purchase Order Is : " + LTransId, SAPbouiCOM.BoMessageTime.bmt_Short, false);
                                // Root.Instance.frmAccuralSheet.Items.Item("14").Specific.value = LTransId.ToString();
                                if (Global.oCompany1.InTransaction)
                                    Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                try
                                {
                                    SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                    if (Global.oServerType == "dst_HANADB")
                                    {

                                        MRPQry = "Update \"ODRF\" set  \"U_Status\"='Y',\"U_EMsg\"='' where \"DocEntry\"='" + DocEntry + "'";


                                    }
                                    else
                                    {
                                        MRPQry = "Update \"ODRF\" set  \"U_Status\"='Y',\"U_EMsg\"='' where \"DocEntry\"='" + DocEntry + "'";

                                        //MRPQry = "Update ORDR set  U_Status='Y',U_EMsg='' where DocEntry='" + DocEntry + "'";
                                    }
                                    oRS1.DoQuery(MRPQry);
                                }
                                catch (Exception x)
                                {

                                }

                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Global.oApplication.SetStatusBarMessage(ex.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, true);
                return false;
            }
            return true;
        }
        public static bool SO2POBranchPosting(string DocEntry, string NumAtCard, string VCode, string ToWarehouse)
        {
            try
            {
                string MRPQry = "";
                SAPbobsCOM.Recordset RSMRP = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                if (Global.oServerType == "dst_HANADB")
                {
                    MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\",\"M\".\"Comments\" from \"ODRF\" \"M\" inner join \"DRF1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";

                }
                else
                {
                    MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\",\"M\".\"Comments\" from \"ODRF\" \"M\" inner join \"DRF1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";

                    // MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\",\"M\".\"Comments\" from \"ODRF\" \"M\" inner join \"DRF1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";
                }
                RSMRP.DoQuery(MRPQry);
                if (RSMRP.RecordCount > 0)
                {
                    int lretcode;
                    string s = "";
                    string LTransId = "";
                    string Intercompany = getSingleValue("Select \"U_VSPLIC\" from \"OCRD\" where \"CardCode\"='" + RSMRP.Fields.Item("CardCode").Value + "'");
                    string InterBranch = getSingleValue("Select \"U_VSPLIB\" from \"OCRD\" where \"CardCode\"='" + RSMRP.Fields.Item("CardCode").Value + "'");
                    if (InterBranch == "Yes" && Intercompany == "No")
                    {
                        SAPbobsCOM.Documents Doc;
                        Doc = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts);
                        if (!Global.oCompany.InTransaction)
                            Global.oCompany.StartTransaction();
                        Doc.DocObjectCode = SAPbobsCOM.BoObjectTypes.oPurchaseOrders;

                        Doc.CardCode = VCode;//RSMRP.Fields.Item("CardCode").Value;
                        Doc.NumAtCard = NumAtCard; //Convert.ToString(RSMRP.Fields.Item("DocNum").Value);
                        Doc.DocDate = RSMRP.Fields.Item("DocDate").Value;
                        Doc.DocDueDate = RSMRP.Fields.Item("DocDueDate").Value;
                        Doc.TaxDate = RSMRP.Fields.Item("TaxDate").Value;
                        Doc.Comments = RSMRP.Fields.Item("Comments").Value;
                        double DocRate = RSMRP.Fields.Item("DiscPrcnt").Value;
                        Doc.UserFields.Fields.Item("U_SOEntry").Value = DocEntry;
                        if (DocRate.ToString().Trim() == "")
                        {
                            DocRate = 0;
                        }
                        if (Convert.ToDouble(DocRate) != 0)
                        {
                            Doc.DiscountPercent = Convert.ToDouble(DocRate);
                        }

                        SAPbobsCOM.Recordset oRS = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                        if (Global.oServerType == "dst_HANADB")
                        {

                            MRPQry = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\"   from \"ODRF\" \"M\" inner join \"DRF1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" inner join \"OITM\" \"M2\" on \"M1\".\"ItemCode\"=\"M2\".\"ItemCode\" Inner join OITB M3 on \"M2\".\"ItmsGrpCod\"=\"M3\".\"ItmsGrpCod\" where \"M\".\"DocEntry\"='" + DocEntry + "' ";

                            // MRPQry = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\"   from \"ODRF\" \"M\" inner join \"DRF1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" inner join \"OITM\" \"M2\" on \"M1\".\"ItemCode\"=\"M2\".\"ItemCode\" Inner join OITB M3 on \"M2\".\"ItmsGrpCod\"=\"M3\".\"ItmsGrpCod\" where \"M\".\"DocEntry\"='" + DocEntry + "' ";
                        }
                        else
                        {

                            MRPQry = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\"   from \"ODRF\" \"M\" inner join \"DRF1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" inner join \"OITM\" \"M2\" on \"M1\".\"ItemCode\"=\"M2\".\"ItemCode\" Inner join OITB M3 on \"M2\".\"ItmsGrpCod\"=\"M3\".\"ItmsGrpCod\" where \"M\".\"DocEntry\"='" + DocEntry + "' ";

                            // MRPQry = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\"  from \"ODRF\" \"M\" inner join \"DRF1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" inner join \"OITM\" \"M2\" on \"M1\".\"ItemCode\"=\"M2\".\"ItemCode\" Inner join OITB M3 on \"M2\".\"ItmsGrpCod\"=\"M3\".\"ItmsGrpCod\" where \"M\".\"DocEntry\"='" + DocEntry + "' ";
                        }
                        oRS.DoQuery(MRPQry);
                        if (oRS.RecordCount > 0)
                        {
                            double LabrCst = 0;
                            for (int l = 1; l <= oRS.RecordCount; l++)
                            {
                                //string Barcode = getSingleValue("SELECT T0.\"CodeBars\" FROM OITM T0 WHERE T0.\"ItemCode\" ='" + oRS.Fields.Item("ItemCode").Value + "'");
                                //string StockCode = getSingleValue1("SELECT T0.\"ItemCode\" FROM OITM T0 WHERE T0.\"CodeBars\" ='" + Barcode + "'");

                                Doc.Lines.ItemCode = oRS.Fields.Item("ItemCode").Value;
                                Doc.Lines.Quantity = oRS.Fields.Item("Quantity").Value;
                                Doc.Lines.UnitPrice = oRS.Fields.Item("Price").Value;
                                Doc.Lines.WarehouseCode = ToWarehouse;


                                string LineId = oRS.Fields.Item("LineNum").Value;  // getSingleValue1("SELECT T0.\"LineNum\" FROM DRF1 T0 WHERE T0.\"ItemCode\" ='" + oRS.Fields.Item("ItemCode").Value + "' and T0.\"DocEntry\" ='" + DocEntry + "'");
                                Doc.Lines.UserFields.Fields.Item("U_BaseLine").Value = LineId;
                                Doc.Lines.UserFields.Fields.Item("U_BaseEntry").Value = DocEntry;
                                double LDocRate = oRS.Fields.Item("DiscPrcnt").Value;
                                if (LDocRate.ToString().Trim() == "")
                                {
                                    LDocRate = 0;
                                }
                                if (Convert.ToDouble(LDocRate) != 0)
                                {
                                    Doc.Lines.DiscountPercent = Convert.ToDouble(LDocRate);
                                }

                                Doc.Lines.Add();

                                // }
                                oRS.MoveNext();
                            }
                            string FrightAmt = getSingleValue("SELECT T0.\"LineTotal\" FROM DRF3 T0 WHERE T0.\"DocEntry\"='" + DocEntry + "' and ifnull(T0.\"LineTotal\",0)>0 ");

                            //string FrightAmt = getSingleValue("SELECT T0.\"LineTotal\" FROM DRF2 T0 WHERE T0\"DocEntry\"='" + oRS.Fields.Item("ItemCode").Value + "' where ifnull(T0.\"ExpnsCode\",'')='' ");
                            string TaxCode = getSingleValue("SELECT T0.\"TaxCode\" FROM DRF3 T0 WHERE T0.\"DocEntry\"='" + DocEntry + "' and ifnull(T0.\"LineTotal\",0)>0 ");


                            if (FrightAmt != "" && FrightAmt != null)
                            {
                                Doc.Expenses.ExpenseCode = 1;
                                Doc.Expenses.LineTotal = Convert.ToDouble(FrightAmt);
                                Doc.Expenses.TaxCode = TaxCode;
                                Doc.Expenses.Add();
                            }
                            string BPLID = getSingleValue1("SELECT T0.\"BPLid\" FROM OWHS T0 WHERE T0.\"WhsCode\" ='" + ToWarehouse + "'");
                            Doc.BPL_IDAssignedToInvoice = Convert.ToInt32(BPLID);
                            lretcode = Doc.Add();
                            if (lretcode != 0)
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                s = Global.oCompany.GetLastErrorDescription();
                                Global.oApplication.SetStatusBarMessage("Purchase Order Is Not Posted Due To : " + s, SAPbouiCOM.BoMessageTime.bmt_Short, true);
                                try
                                {
                                    SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                    if (Global.oServerType == "dst_HANADB")
                                    {

                                        MRPQry = "Update \"ODRF\" set  \"U_EMsg\"='" + s.Replace("'", "").ToString() + "' where \"DocEntry\"='" + DocEntry + "'";

                                    }
                                    else
                                    {

                                        MRPQry = "Update \"ODRF\" set  \"U_EMsg\"='" + s.Replace("'", "").ToString() + "' where \"DocEntry\"='" + DocEntry + "'";

                                        //MRPQry = "Update ORDR set  U_EMsg='" + s.Replace("'", "").ToString() + "' where DocEntry='" + DocEntry + "'";
                                    }
                                    oRS1.DoQuery(MRPQry);
                                }
                                catch (Exception x)
                                {

                                }
                                // return false;
                            }
                            else
                            {
                                LTransId = Global.oCompany.GetNewObjectKey();
                                Global.oApplication.SetStatusBarMessage("Purchase Order Is : " + LTransId, SAPbouiCOM.BoMessageTime.bmt_Short, false);
                                // Root.Instance.frmAccuralSheet.Items.Item("14").Specific.value = LTransId.ToString();
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                try
                                {
                                    SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                    if (Global.oServerType == "dst_HANADB")
                                    {

                                        MRPQry = "Update \"ODRF\" set  \"U_Status\"='Y',\"U_EMsg\"='' where \"DocEntry\"='" + DocEntry + "'";


                                    }
                                    else
                                    {
                                        MRPQry = "Update \"ODRF\" set  \"U_Status\"='Y',\"U_EMsg\"='' where \"DocEntry\"='" + DocEntry + "'";

                                        //MRPQry = "Update ORDR set  U_Status='Y',U_EMsg='' where DocEntry='" + DocEntry + "'";
                                    }
                                    oRS1.DoQuery(MRPQry);
                                }
                                catch (Exception x)
                                {

                                }

                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Global.oApplication.SetStatusBarMessage(ex.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, true);
                return false;
            }
            return true;
        }      
       
        public static bool SO2POPosting1(string DocEntry, string ItemGroup, string VCode, string CCode)
        {
            try
            {
                string MRPQry = "";
                SAPbobsCOM.Recordset RSMRP = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                string MRPQry1 = "";
                SAPbobsCOM.Recordset RSMRP1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                if (Global.oServerType == "dst_HANADB")
                {
                    MRPQry1 = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\",\"M\".\"Comments\" from \"OPOR\" \"M\" inner join \"POR1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";
                }
                else
                {
                    MRPQry1 = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\",\"M\".\"Comments\" from \"OPOR\" \"M\" inner join \"POR1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";
                }
                RSMRP1.DoQuery(MRPQry1);
                if (RSMRP1.RecordCount > 0)
                {

                    int lretcode1;
                    string s1 = "";
                    string LTransId1 = "";
                    SAPbobsCOM.Documents Doc1;
                    Doc1 = Global.oCompany1.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders);
                    if (!Global.oCompany1.InTransaction)
                        Global.oCompany1.StartTransaction();
                    Doc1.CardCode = CCode; //"SGCSFB";//RSMRP.Fields.Item("CardCode").Value;
                    Doc1.NumAtCard = Convert.ToString(RSMRP1.Fields.Item("DocNum").Value);
                    Doc1.DocDate = RSMRP1.Fields.Item("DocDate").Value;
                    Doc1.DocDueDate = RSMRP1.Fields.Item("DocDueDate").Value;
                    Doc1.TaxDate = RSMRP1.Fields.Item("TaxDate").Value;
                    Doc1.Comments = RSMRP1.Fields.Item("Comments").Value;
                    double DocRate1 = RSMRP1.Fields.Item("DiscPrcnt").Value;
                    if (DocRate1.ToString().Trim() == "")
                    {
                        DocRate1 = 0;
                    }
                    if (Convert.ToDouble(DocRate1) != 0)
                    {
                        Doc1.DiscountPercent = Convert.ToDouble(DocRate1);
                    }
                    Doc1.UserFields.Fields.Item("U_POEntry").Value = Convert.ToString(RSMRP1.Fields.Item("DocEntry").Value);
                    Doc1.UserFields.Fields.Item("U_PODocNum").Value = Convert.ToString(RSMRP1.Fields.Item("DocNum").Value);
                    Doc1.UserFields.Fields.Item("U_POSeries").Value = Convert.ToString(RSMRP1.Fields.Item("Series").Value);
                    //Doc.Memo = "D JE P&L Checking";
                    //  string Months = Root.Instance.frmAccuralSheet.Items.Item("4").Specific.value;
                    //  string Years = Root.Instance.frmAccuralSheet.Items.Item("6").Specific.value                   
                    SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    if (Global.oServerType == "dst_HANADB")
                    {
                        MRPQry1 = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\",\"M1\".\"OcrCode\",\"M1\".\"OcrCode2\",\"M1\".\"OcrCode3\",\"M1\".\"OcrCode4\",\"M1\".\"OcrCode5\",\"M1\".\"LocCode\"   from \"OPOR\" \"M\" inner join \"POR1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" inner join \"OITM\" \"M2\" on \"M1\".\"ItemCode\"=\"M2\".\"ItemCode\" Inner join OITB M3 on \"M2\".\"ItmsGrpCod\"=\"M3\".\"ItmsGrpCod\" where \"M\".\"DocEntry\"='" + DocEntry + "' and \"M3\".\"ItmsGrpNam\" Like '%" + ItemGroup + "%'";
                    }
                    else
                    {
                        MRPQry1 = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\",\"M1\".\"OcrCode\",\"M1\".\"OcrCode2\",\"M1\".\"OcrCode3\",\"M1\".\"OcrCode4\",\"M1\".\"OcrCode5\",\"M1\".\"LocCode\"  from \"OPOR\" \"M\" inner join \"POR1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" inner join \"OITM\" \"M2\" on \"M1\".\"ItemCode\"=\"M2\".\"ItemCode\" Inner join OITB M3 on \"M2\".\"ItmsGrpCod\"=\"M3\".\"ItmsGrpCod\" where \"M\".\"DocEntry\"='" + DocEntry + "' and \"M3\".\"ItmsGrpCod\" Like'%" + ItemGroup + "%'";
                    }
                    oRS1.DoQuery(MRPQry1);
                    if (oRS1.RecordCount > 0)
                    {
                        double LabrCst1 = 0;
                        for (int l = 1; l <= oRS1.RecordCount; l++)
                        {
                            //  LabrCst = LabrCst + Convert.ToDouble(oRS.Fields.Item("DocTotal").Value);
                            //if (LabrCst != 0)
                            //{
                            // - - - - - Line Details Started - - - -
                            Doc1.Lines.ItemCode = oRS1.Fields.Item("ItemCode").Value;
                            Doc1.Lines.Quantity = oRS1.Fields.Item("Quantity").Value;
                            Doc1.Lines.UnitPrice = oRS1.Fields.Item("Price").Value;
                            Doc1.Lines.WarehouseCode = oRS1.Fields.Item("WhsCode").Value;
                            Doc1.Lines.LocationCode = oRS1.Fields.Item("LocCode").Value;
                            Doc1.Lines.UoMEntry = oRS1.Fields.Item("UomEntry").Value;

                            Doc1.Lines.UserFields.Fields.Item("U_LineNum").Value = Convert.ToString(oRS1.Fields.Item("LineNum").Value);
                            Doc1.Lines.UserFields.Fields.Item("U_POEntry").Value = Convert.ToString(RSMRP1.Fields.Item("DocEntry").Value);
                            double LDocRate = oRS1.Fields.Item("DiscPrcnt").Value;
                            double DocRate = 0;
                            if (LDocRate.ToString().Trim() == "")
                            {
                                LDocRate = 0;
                            }
                            if (Convert.ToDouble(LDocRate) != 0)
                            {
                                Doc1.Lines.DiscountPercent = Convert.ToDouble(LDocRate);
                            }
                            string GroupMask = Convert.ToString(oRS1.Fields.Item("OcrCode").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc1.Lines.CostingCode = oRS1.Fields.Item("OcrCode").Value;
                            }
                            GroupMask = Convert.ToString(oRS1.Fields.Item("OcrCode2").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc1.Lines.CostingCode2 = oRS1.Fields.Item("OcrCode2").Value;
                            }
                            GroupMask = Convert.ToString(oRS1.Fields.Item("OcrCode3").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc1.Lines.CostingCode3 = oRS1.Fields.Item("OcrCode3").Value;
                            }
                            GroupMask = Convert.ToString(oRS1.Fields.Item("OcrCode4").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc1.Lines.CostingCode4 = oRS1.Fields.Item("OcrCode4").Value;
                            }
                            GroupMask = Convert.ToString(oRS1.Fields.Item("OcrCode5").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc1.Lines.CostingCode5 = oRS1.Fields.Item("OcrCode5").Value;
                            }
                            Doc1.Lines.Add();

                            // }
                            oRS1.MoveNext();
                        }

                        lretcode1 = Doc1.Add();
                        if (lretcode1 != 0)
                        {
                            if (Global.oCompany1.InTransaction)
                                Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            s1 = Global.oCompany1.GetLastErrorDescription();
                            Global.oApplication.SetStatusBarMessage("Sales Order Is Not Posted Due To : " + s1, SAPbouiCOM.BoMessageTime.bmt_Short, true);
                            try
                            {
                                SAPbobsCOM.Recordset oRS2 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                if (Global.oServerType == "dst_HANADB")
                                {
                                    MRPQry1 = "Update \"OPOR\" set  \"U_EMsg\"='" + s1.Replace("'", "").ToString() + "' where \"DocEntry\"='" + DocEntry + "'";
                                }
                                else
                                {
                                    MRPQry1 = "Update OPOR set  U_EMsg='" + s1.Replace("'", "").ToString() + "' where DocEntry='" + DocEntry + "'";
                                }
                                oRS2.DoQuery(MRPQry1);
                            }
                            catch (Exception x)
                            {

                            }
                            // return false;
                        }
                        else
                        {
                            LTransId1 = Global.oCompany1.GetNewObjectKey();
                            Global.oApplication.SetStatusBarMessage("Sales Order Is : " + LTransId1, SAPbouiCOM.BoMessageTime.bmt_Short, false);
                            // Root.Instance.frmAccuralSheet.Items.Item("14").Specific.value = LTransId.ToString();
                            if (Global.oCompany1.InTransaction)
                                Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            try
                            {
                                SAPbobsCOM.Recordset oRS2 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                if (Global.oServerType == "dst_HANADB")
                                {
                                    MRPQry1 = "Update \"OPOR\" set  \"U_Status\"='Y',\"U_EMsg\"='' where \"DocEntry\"='" + DocEntry + "'";
                                }
                                else
                                {
                                    MRPQry1 = "Update OPOR set  U_Status='Y',U_EMsg='' where DocEntry='" + DocEntry + "'";
                                }
                                oRS2.DoQuery(MRPQry1);
                            }
                            catch (Exception x)
                            {

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.oApplication.SetStatusBarMessage(ex.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, true);
                return false;
            }
            return true;
        }
        public static bool DO2GRPOPosting(string DocEntry, string DB)
        {
            try
            {
                string MRPQry = "";
                SAPbobsCOM.Recordset RSMRP = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                if (Global.oServerType == "dst_HANADB")
                {
                    MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\",\"M1\".\"BaseEntry\" from \"ODLN\" \"M\" inner join \"DLN1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";
                }
                else
                {
                    MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\",\"M1\".\"BaseEntry\" from \"ODLN\" \"M\" inner join \"DLN1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";
                }
                RSMRP.DoQuery(MRPQry);
                if (RSMRP.RecordCount > 0)
                {
                    int lretcode;
                    string s = "";
                    string LTransId = "";
                    SAPbobsCOM.Documents Doc;
                    Doc = Global.oCompany1.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts);
                    if (!Global.oCompany1.InTransaction)
                        Global.oCompany1.StartTransaction();
                    SAPbobsCOM.Recordset RS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    MRPQry = "SELECT T2.\"DocEntry\" FROM ORDR T0 Inner join RDR1 T1 on T0.\"DocEntry\"=T1.\"DocEntry\" Inner join ODRF T2 on T2.\"DocEntry\"=T0.\"draftKey\" where T0.\"DocEntry\"='" + RSMRP.Fields.Item("BaseEntry").Value + "' ";
                    RS1.DoQuery(MRPQry);
                    if (RS1.RecordCount > 0)
                    {
                        SAPbobsCOM.Recordset RS2 = (SAPbobsCOM.Recordset)Global.oCompany1.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                        MRPQry = "SELECT T0.\"CardCode\",T0.\"BPLId\" FROM OPOR T0 where T0.\"U_SOEntry\"='" + RS1.Fields.Item("DocEntry").Value + "' ";
                        RS2.DoQuery(MRPQry);
                        if (RS1.RecordCount > 0)
                        {
                            Doc.CardCode = RS2.Fields.Item("CardCode").Value;
                            Doc.BPL_IDAssignedToInvoice = Convert.ToInt32(RS2.Fields.Item("BPLId").Value);
                        }
                    }
                    Doc.NumAtCard = Convert.ToString(RSMRP.Fields.Item("DocNum").Value);
                    Doc.DocObjectCode = SAPbobsCOM.BoObjectTypes.oPurchaseDeliveryNotes;
                    Doc.DocDate = RSMRP.Fields.Item("DocDate").Value;
                    Doc.DocDueDate = RSMRP.Fields.Item("DocDueDate").Value;
                    Doc.TaxDate = RSMRP.Fields.Item("TaxDate").Value;
                    double DocRate = RSMRP.Fields.Item("DiscPrcnt").Value;


                    if (DocRate.ToString().Trim() == "")
                    {
                        DocRate = 0;
                    }
                    if (Convert.ToDouble(DocRate) != 0)
                    {
                        Doc.DiscountPercent = Convert.ToDouble(DocRate);
                    }
                    Doc.UserFields.Fields.Item("U_DOEntry").Value = Convert.ToString(RSMRP.Fields.Item("DocEntry").Value);
                    SAPbobsCOM.Recordset oRS = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    if (Global.oServerType == "dst_HANADB")
                    {
                        MRPQry = "SELECT T4.\"DocEntry\", T4.\"LineNum\",T1.\"LineNum\" as \"DLine\",T1.\"Price\",T1.\"Quantity\",T1.\"DiscPrcnt\",T1.\"U_BaseLine\",T1.\"U_BaseEntry\" FROM ODLN T0  INNER JOIN DLN1 T1 ON T0.\"DocEntry\" = T1.\"DocEntry\" Inner join RDR1 T2 on T1.\"BaseEntry\"=T2.\"DocEntry\" and T1.\"BaseLine\"=T2.\"LineNum\" and T1.\"BaseType\"=17 Inner join ORDR T3 on T2.\"DocEntry\"=T3.\"DocEntry\" Inner join DRF1 T4 on T3.\"draftKey\"=T4.\"DocEntry\" and T4.\"LineNum\"=T2.\"LineNum\" where T0.\"DocEntry\"='" + DocEntry + "'";
                    }
                    else
                    {
                        MRPQry = "SELECT T4.\"DocEntry\", T4.\"LineNum\",T1.\"LineNum\" as \"DLine\",T1.\"Price\",T1.\"Quantity\",T1.\"DiscPrcnt\",T1.\"U_BaseLine\",T1.\"U_BaseEntry\" FROM ODLN T0  INNER JOIN DLN1 T1 ON T0.\"DocEntry\" = T1.\"DocEntry\" Inner join RDR1 T2 on T1.\"BaseEntry\"=T2.\"DocEntry\" and T1.\"BaseLine\"=T2.\"LineNum\" and T1.\"BaseType\"=17 Inner join ORDR T3 on T2.\"DocEntry\"=T3.\"DocEntry\" Inner join DRF1 T4 on T3.\"draftKey\"=T4.\"DocEntry\" and T4.\"LineNum\"=T2.\"LineNum\" where T0.\"DocEntry\"='" + DocEntry + "'";
                    }
                    oRS.DoQuery(MRPQry);
                    if (oRS.RecordCount > 0)
                    {
                        double LabrCst = 0;
                        for (int l = 1; l <= oRS.RecordCount; l++)
                        {
                            SAPbobsCOM.Recordset oRSc1 = (SAPbobsCOM.Recordset)Global.oCompany1.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                            if (Global.oServerType == "dst_HANADB")
                            {
                                if (DB == "")
                                {
                                    MRPQry = "SELECT T1.\"DocEntry\",T1.\"LineNum\",T1.\"ItemCode\",T1.\"Price\",T1.\"WhsCode\" FROM OPOR T0  INNER JOIN POR1 T1 ON T0.\"DocEntry\" = T1.\"DocEntry\" where T1.\"DocEntry\"='" + oRS.Fields.Item("U_BaseEntry").Value + "' and T1.\"LineNum\"='" + oRS.Fields.Item("U_BaseLine").Value + "'";

                                }
                                else
                                {
                                    MRPQry = "SELECT T1.\"DocEntry\",T1.\"LineNum\",T1.\"ItemCode\",T1.\"Price\",T1.\"WhsCode\" FROM OPOR T0  INNER JOIN POR1 T1 ON T0.\"DocEntry\" = T1.\"DocEntry\" where T1.\"U_BaseEntry\"='" + oRS.Fields.Item("DocEntry").Value + "' and T1.\"U_BaseLine\"='" + oRS.Fields.Item("LineNum").Value + "'";

                                }
                            }
                            else
                            {
                                if (DB == "")
                                {
                                    MRPQry = "SELECT T1.\"DocEntry\",T1.\"LineNum\",T1.\"ItemCode\",T1.\"Price\",T1.\"WhsCode\" FROM OPOR T0  INNER JOIN POR1 T1 ON T0.\"DocEntry\" = T1.\"DocEntry\" where T1.\"DocEntry\"='" + oRS.Fields.Item("U_BaseEntry").Value + "' and T1.\"LineNum\"='" + oRS.Fields.Item("U_BaseLine").Value + "'";

                                }
                                else
                                {
                                    MRPQry = "SELECT T1.\"DocEntry\",T1.\"LineNum\",T1.\"ItemCode\",T1.\"Price\",T1.\"WhsCode\" FROM OPOR T0  INNER JOIN POR1 T1 ON T0.\"DocEntry\" = T1.\"DocEntry\" where T1.\"U_BaseEntry\"='" + oRS.Fields.Item("DocEntry").Value + "' and T1.\"U_BaseLine\"='" + oRS.Fields.Item("LineNum").Value + "'";

                                }
                            }
                            oRSc1.DoQuery(MRPQry);
                            if (oRSc1.RecordCount > 0)
                            {
                                // - - - - - Line Details Started - - - -
                                Doc.Lines.ItemCode = oRSc1.Fields.Item("ItemCode").Value;
                                Doc.Lines.Quantity = oRS.Fields.Item("Quantity").Value;
                                Doc.Lines.UnitPrice = oRS.Fields.Item("Price").Value;
                                Doc.Lines.WarehouseCode = oRSc1.Fields.Item("WhsCode").Value;
                                Doc.Lines.UserFields.Fields.Item("U_DOLineNum").Value = Convert.ToString(oRS.Fields.Item("DLine").Value);
                                Doc.Lines.UserFields.Fields.Item("U_DOEntry").Value = DocEntry;
                                double LDocRate = oRS.Fields.Item("DiscPrcnt").Value;
                                string POLine = Convert.ToString(oRSc1.Fields.Item("LineNum").Value);
                                string PODocEntry = Convert.ToString(oRSc1.Fields.Item("DocEntry").Value);
                                if (PODocEntry.ToString().Trim() != "0")
                                {
                                    Doc.Lines.BaseEntry = Convert.ToInt32(PODocEntry);
                                    Doc.Lines.BaseLine = Convert.ToInt32(POLine);
                                    Doc.Lines.BaseType = 22;
                                }
                                if (LDocRate.ToString().Trim() == "")
                                {
                                    DocRate = 0;
                                }
                                if (Convert.ToDouble(DocRate) != 0)
                                {
                                    Doc.DiscountPercent = Convert.ToDouble(DocRate);
                                }

                                SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                if (Global.oServerType == "dst_HANADB")
                                {
                                    MRPQry = "select \"BatchNum\",\"WhsCode\",\"Quantity\" from IBT1 Where \"BaseType\"='15' and \"BaseEntry\"='" + DocEntry + "' and \"BaseLinNum\"='" + Convert.ToString(oRS.Fields.Item("DLine").Value) + "' ";
                                }
                                else
                                {
                                    MRPQry = "select \"BatchNum\",\"WhsCode\",\"Quantity\" from IBT1 Where \"BaseType\"='15' and \"BaseEntry\"='" + DocEntry + "' and \"BaseLinNum\"='" + Convert.ToString(oRS.Fields.Item("DLine").Value) + "' ";
                                }
                                oRS1.DoQuery(MRPQry);
                                if (oRS1.RecordCount > 0)
                                {
                                    for (int k = 1; k <= oRS1.RecordCount; k++)
                                    {
                                        Doc.Lines.BatchNumbers.BatchNumber = Convert.ToString(oRS1.Fields.Item("BatchNum").Value);
                                        Doc.Lines.BatchNumbers.Quantity = Convert.ToDouble(oRS1.Fields.Item("Quantity").Value);
                                        SAPbobsCOM.Recordset oRS2 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                        if (Global.oServerType == "dst_HANADB")
                                        {
                                            MRPQry = "select ifnull(\"MnfSerial\",'') as \"MnfSerial\",ifnull(\"LotNumber\",'') as \"LotNumber\",ifnull(\"MnfDate\",'') as \"MnfDate\",ifnull(\"ExpDate\",'') as \"ExpDate\",ifnull(\"GrntStart\",'') as \"GrntStart\",ifnull(\"GrntExp\",'') as \"GrntExp\",ifnull(\"Notes\",'') as \"Notes\" from OBTN Where \"DistNumber\"='" + Convert.ToString(oRS1.Fields.Item("BatchNum").Value) + "' ";
                                        }
                                        else
                                        {
                                            MRPQry = "select ifnull(\"MnfSerial\",'') as \"MnfSerial\",ifnull(\"LotNumber\",'') as \"LotNumber\",ifnull(\"MnfDate\",'') as \"MnfDate\",ifnull(\"ExpDate\",'') as \"ExpDate\",ifnull(\"GrntStart\",'') as \"GrntStart\",ifnull(\"GrntExp\",'') as \"GrntExp\",ifnull(\"Notes\",'') as \"Notes\" from OBTN Where \"DistNumber\"='" + Convert.ToString(oRS1.Fields.Item("BatchNum").Value) + "' ";
                                        }
                                        oRS2.DoQuery(MRPQry);
                                        if (oRS2.RecordCount > 0)
                                        {
                                            if (Convert.ToString(oRS2.Fields.Item("MnfSerial").Value) != "")
                                                Doc.Lines.BatchNumbers.ManufacturerSerialNumber = oRS2.Fields.Item("MnfSerial").Value;
                                            //if (Convert.ToString(oRS1.Fields.Item("LotNumber").Value) != "")
                                            //    Doc.Lines.BatchNumbers.InternalSerialNumber = oRS1.Fields.Item("LotNumber").Value;
                                            if (Convert.ToString(oRS2.Fields.Item("MnfDate").Value) != "")
                                                Doc.Lines.BatchNumbers.ManufacturingDate = oRS2.Fields.Item("MnfDate").Value;
                                            if (Convert.ToString(oRS2.Fields.Item("ExpDate").Value) != "")
                                                Doc.Lines.BatchNumbers.ManufacturingDate = oRS2.Fields.Item("ExpDate").Value;
                                            if (Convert.ToString(oRS2.Fields.Item("Notes").Value) != "")
                                                Doc.Lines.BatchNumbers.Notes = oRS2.Fields.Item("Notes").Value;
                                        }
                                        Doc.Lines.BatchNumbers.Add();
                                        oRS1.MoveNext();
                                    }
                                }
                                Doc.Lines.Add();
                            }
                            else
                            {
                                if (Global.oCompany1.InTransaction)
                                    Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);

                                s = "This SO No: " + oRS.Fields.Item("DocEntry").Value + " Not Convert to PO, So Please Create PO Draft to PO";
                                Global.oApplication.SetStatusBarMessage(s, SAPbouiCOM.BoMessageTime.bmt_Short, true);
                                try
                                {
                                    SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                    if (Global.oServerType == "dst_HANADB")
                                    {
                                        MRPQry = "Update \"ODLN\" set  \"U_EMsg\"='" + s.Replace("'", "").ToString() + "' where \"DocEntry\"='" + DocEntry + "'";
                                    }
                                    else
                                    {
                                        MRPQry = "Update ODLN set  U_EMsg='" + s.Replace("'", "").ToString() + "' where DocEntry='" + DocEntry + "'";
                                    }
                                    oRS1.DoQuery(MRPQry);
                                }
                                catch (Exception x)
                                {
                                }
                                return false;
                            }
                            oRS.MoveNext();
                        }

                        lretcode = Doc.Add();
                        if (lretcode != 0)
                        {
                            if (Global.oCompany1.InTransaction)
                                Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            s = Global.oCompany1.GetLastErrorDescription();
                            Global.oApplication.SetStatusBarMessage("GRPO Is Not Posted Due To : " + s, SAPbouiCOM.BoMessageTime.bmt_Short, true);
                            try
                            {
                                SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                if (Global.oServerType == "dst_HANADB")
                                {
                                    MRPQry = "Update \"ODLN\" set  \"U_EMsg\"='" + s.Replace("'", "").ToString() + "' where \"DocEntry\"='" + DocEntry + "'";
                                }
                                else
                                {
                                    MRPQry = "Update ODLN set  U_EMsg='" + s.Replace("'", "").ToString() + "' where DocEntry='" + DocEntry + "'";
                                }
                                oRS1.DoQuery(MRPQry);
                            }
                            catch (Exception x)
                            {

                            }
                            // return false;
                        }
                        else
                        {
                            LTransId = Global.oCompany1.GetNewObjectKey();
                            Global.oApplication.SetStatusBarMessage("GRPO Is : " + LTransId, SAPbouiCOM.BoMessageTime.bmt_Short, false);
                            // Root.Instance.frmAccuralSheet.Items.Item("14").Specific.value = LTransId.ToString();
                            if (Global.oCompany1.InTransaction)
                                Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            try
                            {
                                SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                if (Global.oServerType == "dst_HANADB")
                                {
                                    MRPQry = "Update \"ODLN\" set  \"U_Status\"='Y',\"U_EMsg\"='' where \"DocEntry\"='" + DocEntry + "'";
                                }
                                else
                                {
                                    MRPQry = "Update ODLN set  U_Status='Y',U_EMsg='' where DocEntry='" + DocEntry + "'";
                                }
                                oRS1.DoQuery(MRPQry);
                                Root.Instance.frmDelivery.Items.Item("RePost").Enabled = false;
                            }
                            catch (Exception x)
                            {

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.oApplication.SetStatusBarMessage(ex.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, true);
                string s = ex.ToString();
                Global.oApplication.SetStatusBarMessage("GRPO Is Not Posted Due To : " + s, SAPbouiCOM.BoMessageTime.bmt_Short, true);
                try
                {
                    string MRPQry = "";
                    SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    if (Global.oServerType == "dst_HANADB")
                    {
                        MRPQry = "Update \"ODLN\" set  \"U_EMsg\"='" + s.Replace("'", "").ToString() + "' where \"DocEntry\"='" + DocEntry + "'";
                    }
                    else
                    {
                        MRPQry = "Update ODLN set  U_EMsg='" + s.Replace("'", "").ToString() + "' where DocEntry='" + DocEntry + "'";
                    }
                    oRS1.DoQuery(MRPQry);
                }
                catch (Exception x)
                {

                }
                return false;
            }
            return true;
        }
        public static bool DO2GRPOBranchPosting(string DocEntry, string DB)
        {
            try
            {
                string MRPQry = "";
                SAPbobsCOM.Recordset RSMRP = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                if (Global.oServerType == "dst_HANADB")
                {
                    MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\",\"M1\".\"BaseEntry\" from \"ODLN\" \"M\" inner join \"DLN1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";
                }
                else
                {
                    MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\",\"M1\".\"BaseEntry\" from \"ODLN\" \"M\" inner join \"DLN1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";
                }
                RSMRP.DoQuery(MRPQry);
                if (RSMRP.RecordCount > 0)
                {
                    int lretcode;
                    string s = "";
                    string LTransId = "";
                    SAPbobsCOM.Documents Doc;
                    Doc = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts);
                    if (!Global.oCompany.InTransaction)
                        Global.oCompany.StartTransaction();
                    SAPbobsCOM.Recordset RS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    MRPQry = "SELECT T2.\"DocEntry\" FROM ORDR T0 Inner join RDR1 T1 on T0.\"DocEntry\"=T1.\"DocEntry\" Inner join ODRF T2 on T2.\"DocEntry\"=T0.\"draftKey\" where T0.\"DocEntry\"='" + RSMRP.Fields.Item("BaseEntry").Value + "' ";
                    RS1.DoQuery(MRPQry);
                    if (RS1.RecordCount > 0)
                    {
                        SAPbobsCOM.Recordset RS2 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                        MRPQry = "SELECT T0.\"CardCode\",T0.\"BPLId\" FROM OPOR T0 where T0.\"U_SOEntry\"='" + RS1.Fields.Item("DocEntry").Value + "' ";
                        RS2.DoQuery(MRPQry);
                        if (RS1.RecordCount > 0)
                        {
                            Doc.CardCode = RS2.Fields.Item("CardCode").Value;
                            Doc.BPL_IDAssignedToInvoice = Convert.ToInt32(RS2.Fields.Item("BPLId").Value);
                        }
                    }
                    Doc.NumAtCard = Convert.ToString(RSMRP.Fields.Item("DocNum").Value);                    
                    Doc.DocObjectCode = SAPbobsCOM.BoObjectTypes.oPurchaseDeliveryNotes;
                    Doc.DocDate = RSMRP.Fields.Item("DocDate").Value;
                    Doc.DocDueDate = RSMRP.Fields.Item("DocDueDate").Value;
                    Doc.TaxDate = RSMRP.Fields.Item("TaxDate").Value;
                    double DocRate = RSMRP.Fields.Item("DiscPrcnt").Value;


                    if (DocRate.ToString().Trim() == "")
                    {
                        DocRate = 0;
                    }
                    if (Convert.ToDouble(DocRate) != 0)
                    {
                        Doc.DiscountPercent = Convert.ToDouble(DocRate);
                    }
                    Doc.UserFields.Fields.Item("U_DOEntry").Value = Convert.ToString(RSMRP.Fields.Item("DocEntry").Value);
                    SAPbobsCOM.Recordset oRS = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    if (Global.oServerType == "dst_HANADB")
                    {
                        MRPQry = "SELECT T4.\"DocEntry\", T4.\"LineNum\",T1.\"LineNum\" as \"DLine\",T1.\"Price\",T1.\"Quantity\",T1.\"DiscPrcnt\",T1.\"U_BaseEntry\",T1.\"U_BaseLine\" FROM ODLN T0  INNER JOIN DLN1 T1 ON T0.\"DocEntry\" = T1.\"DocEntry\" Inner join RDR1 T2 on T1.\"BaseEntry\"=T2.\"DocEntry\" and T1.\"BaseLine\"=T2.\"LineNum\" and T1.\"BaseType\"=17 Inner join ORDR T3 on T2.\"DocEntry\"=T3.\"DocEntry\" Inner join DRF1 T4 on T3.\"draftKey\"=T4.\"DocEntry\" and T4.\"LineNum\"=T2.\"LineNum\" where T0.\"DocEntry\"='" + DocEntry + "'";
                    }
                    else
                    {
                        MRPQry = "SELECT T4.\"DocEntry\", T4.\"LineNum\",T1.\"LineNum\" as \"DLine\",T1.\"Price\",T1.\"Quantity\",T1.\"DiscPrcnt\",T1.\"U_BaseEntry\",T1.\"U_BaseLine\" FROM ODLN T0  INNER JOIN DLN1 T1 ON T0.\"DocEntry\" = T1.\"DocEntry\" Inner join RDR1 T2 on T1.\"BaseEntry\"=T2.\"DocEntry\" and T1.\"BaseLine\"=T2.\"LineNum\" and T1.\"BaseType\"=17 Inner join ORDR T3 on T2.\"DocEntry\"=T3.\"DocEntry\" Inner join DRF1 T4 on T3.\"draftKey\"=T4.\"DocEntry\" and T4.\"LineNum\"=T2.\"LineNum\" where T0.\"DocEntry\"='" + DocEntry + "'";
                    }
                    oRS.DoQuery(MRPQry);
                    if (oRS.RecordCount > 0)
                    {
                        double LabrCst = 0;
                        for (int l = 1; l <= oRS.RecordCount; l++)
                        {
                            SAPbobsCOM.Recordset oRSc1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                            if (Global.oServerType == "dst_HANADB")
                            {
                                if (DB == "")
                                {
                                    MRPQry = "SELECT T1.\"DocEntry\",T1.\"LineNum\",T1.\"ItemCode\",T1.\"Price\",T1.\"WhsCode\" FROM OPOR T0  INNER JOIN POR1 T1 ON T0.\"DocEntry\" = T1.\"DocEntry\" where T1.\"DocEntry\"='" + oRS.Fields.Item("U_BaseEntry").Value + "' and T1.\"LineNum\"='" + oRS.Fields.Item("U_BaseLine").Value + "'";

                                }
                                else
                                {
                                    MRPQry = "SELECT T1.\"DocEntry\",T1.\"LineNum\",T1.\"ItemCode\",T1.\"Price\",T1.\"WhsCode\" FROM OPOR T0  INNER JOIN POR1 T1 ON T0.\"DocEntry\" = T1.\"DocEntry\" where T1.\"U_BaseEntry\"='" + oRS.Fields.Item("DocEntry").Value + "' and T1.\"U_BaseLine\"='" + oRS.Fields.Item("LineNum").Value + "'";
                                                                  
                                }
                            }
                            else
                            {
                                if (DB == "")
                                {
                                    MRPQry = "SELECT T1.\"DocEntry\",T1.\"LineNum\",T1.\"ItemCode\",T1.\"Price\",T1.\"WhsCode\" FROM OPOR T0  INNER JOIN POR1 T1 ON T0.\"DocEntry\" = T1.\"DocEntry\" where T1.\"DocEntry\"='" + oRS.Fields.Item("U_BaseEntry").Value + "' and T1.\"LineNum\"='" + oRS.Fields.Item("U_BaseLine").Value + "'";

                                }
                                else
                                {
                                    MRPQry = "SELECT T1.\"DocEntry\",T1.\"LineNum\",T1.\"ItemCode\",T1.\"Price\",T1.\"WhsCode\" FROM OPOR T0  INNER JOIN POR1 T1 ON T0.\"DocEntry\" = T1.\"DocEntry\" where T1.\"U_BaseEntry\"='" + oRS.Fields.Item("DocEntry").Value + "' and T1.\"U_BaseLine\"='" + oRS.Fields.Item("LineNum").Value + "'";
                                }
                            }
                            oRSc1.DoQuery(MRPQry);
                            if (oRSc1.RecordCount > 0)
                            {
                                // - - - - - Line Details Started - - - -
                                Doc.Lines.ItemCode = oRSc1.Fields.Item("ItemCode").Value;
                                Doc.Lines.Quantity = oRS.Fields.Item("Quantity").Value;
                                Doc.Lines.UnitPrice = oRS.Fields.Item("Price").Value;
                                Doc.Lines.WarehouseCode = oRSc1.Fields.Item("WhsCode").Value;
                                Doc.Lines.UserFields.Fields.Item("U_DOLineNum").Value = Convert.ToString(oRS.Fields.Item("DLine").Value);
                                Doc.Lines.UserFields.Fields.Item("U_DOEntry").Value = DocEntry;
                                double LDocRate = oRS.Fields.Item("DiscPrcnt").Value;
                                string POLine = Convert.ToString(oRSc1.Fields.Item("LineNum").Value);
                                string PODocEntry = Convert.ToString(oRSc1.Fields.Item("DocEntry").Value);
                                if (PODocEntry.ToString().Trim() != "0")
                                {
                                    Doc.Lines.BaseEntry = Convert.ToInt32(PODocEntry);
                                    Doc.Lines.BaseLine = Convert.ToInt32(POLine);
                                    Doc.Lines.BaseType = 22;
                                }
                                if (LDocRate.ToString().Trim() == "")
                                {
                                    DocRate = 0;
                                }
                                if (Convert.ToDouble(DocRate) != 0)
                                {
                                    Doc.DiscountPercent = Convert.ToDouble(DocRate);
                                }

                                SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                if (Global.oServerType == "dst_HANADB")
                                {
                                    MRPQry = "select \"BatchNum\",\"WhsCode\",\"Quantity\" from IBT1 Where \"BaseType\"='15' and \"BaseEntry\"='" + DocEntry + "' and \"BaseLinNum\"='" + Convert.ToString(oRS.Fields.Item("DLine").Value) + "' ";
                                }
                                else
                                {
                                    MRPQry = "select \"BatchNum\",\"WhsCode\",\"Quantity\" from IBT1 Where \"BaseType\"='15' and \"BaseEntry\"='" + DocEntry + "' and \"BaseLinNum\"='" + Convert.ToString(oRS.Fields.Item("DLine").Value) + "' ";
                                }
                                oRS1.DoQuery(MRPQry);
                                if (oRS1.RecordCount > 0)
                                {
                                    for (int k = 1; k <= oRS1.RecordCount; k++)
                                    {
                                        Doc.Lines.BatchNumbers.BatchNumber = Convert.ToString(oRS1.Fields.Item("BatchNum").Value);
                                        Doc.Lines.BatchNumbers.Quantity = Convert.ToDouble(oRS1.Fields.Item("Quantity").Value);
                                        SAPbobsCOM.Recordset oRS2 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                        if (Global.oServerType == "dst_HANADB")
                                        {
                                            MRPQry = "select ifnull(\"MnfSerial\",'') as \"MnfSerial\",ifnull(\"LotNumber\",'') as \"LotNumber\",ifnull(\"MnfDate\",'') as \"MnfDate\",ifnull(\"ExpDate\",'') as \"ExpDate\",ifnull(\"GrntStart\",'') as \"GrntStart\",ifnull(\"GrntExp\",'') as \"GrntExp\",ifnull(\"Notes\",'') as \"Notes\" from OBTN Where \"DistNumber\"='" + Convert.ToString(oRS1.Fields.Item("BatchNum").Value) + "' ";
                                        }
                                        else
                                        {
                                            MRPQry = "select ifnull(\"MnfSerial\",'') as \"MnfSerial\",ifnull(\"LotNumber\",'') as \"LotNumber\",ifnull(\"MnfDate\",'') as \"MnfDate\",ifnull(\"ExpDate\",'') as \"ExpDate\",ifnull(\"GrntStart\",'') as \"GrntStart\",ifnull(\"GrntExp\",'') as \"GrntExp\",ifnull(\"Notes\",'') as \"Notes\" from OBTN Where \"DistNumber\"='" + Convert.ToString(oRS1.Fields.Item("BatchNum").Value) + "' ";
                                        }
                                        oRS2.DoQuery(MRPQry);
                                        if (oRS2.RecordCount > 0)
                                        {
                                            if (Convert.ToString(oRS2.Fields.Item("MnfSerial").Value) != "")
                                                Doc.Lines.BatchNumbers.ManufacturerSerialNumber = oRS2.Fields.Item("MnfSerial").Value;
                                            //if (Convert.ToString(oRS1.Fields.Item("LotNumber").Value) != "")
                                            //    Doc.Lines.BatchNumbers.InternalSerialNumber = oRS1.Fields.Item("LotNumber").Value;
                                            if (Convert.ToString(oRS2.Fields.Item("MnfDate").Value) != "")
                                                Doc.Lines.BatchNumbers.ManufacturingDate = oRS2.Fields.Item("MnfDate").Value;
                                            if (Convert.ToString(oRS2.Fields.Item("ExpDate").Value) != "")
                                                Doc.Lines.BatchNumbers.ManufacturingDate = oRS2.Fields.Item("ExpDate").Value;
                                            if (Convert.ToString(oRS2.Fields.Item("Notes").Value) != "")
                                                Doc.Lines.BatchNumbers.Notes = oRS2.Fields.Item("Notes").Value;
                                        }
                                        Doc.Lines.BatchNumbers.Add();
                                        oRS1.MoveNext();
                                    }
                                }
                                Doc.Lines.Add();
                            }
                            else
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);

                                s = "This SO No: " + oRS.Fields.Item("DocEntry").Value + " Not Convert to PO, So Please Create PO Draft to PO";
                                Global.oApplication.SetStatusBarMessage(s, SAPbouiCOM.BoMessageTime.bmt_Short, true);
                                try
                                {
                                    SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                    if (Global.oServerType == "dst_HANADB")
                                    {
                                        MRPQry = "Update \"ODLN\" set  \"U_EMsg\"='" + s.Replace("'", "").ToString() + "' where \"DocEntry\"='" + DocEntry + "'";
                                    }
                                    else
                                    {
                                        MRPQry = "Update ODLN set  U_EMsg='" + s.Replace("'", "").ToString() + "' where DocEntry='" + DocEntry + "'";
                                    }
                                    oRS1.DoQuery(MRPQry);
                                }
                                catch (Exception x)
                                {
                                }
                                return false;
                            }
                            oRS.MoveNext();
                        }

                        lretcode = Doc.Add();
                        if (lretcode != 0)
                        {
                            if (Global.oCompany.InTransaction)
                                Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            s = Global.oCompany.GetLastErrorDescription();
                            Global.oApplication.SetStatusBarMessage("GRPO Is Not Posted Due To : " + s, SAPbouiCOM.BoMessageTime.bmt_Short, true);
                            try
                            {
                                SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                if (Global.oServerType == "dst_HANADB")
                                {
                                    MRPQry = "Update \"ODLN\" set  \"U_EMsg\"='" + s.Replace("'", "").ToString() + "' where \"DocEntry\"='" + DocEntry + "'";
                                }
                                else
                                {
                                    MRPQry = "Update ODLN set  U_EMsg='" + s.Replace("'", "").ToString() + "' where DocEntry='" + DocEntry + "'";
                                }
                                oRS1.DoQuery(MRPQry);
                                Root.Instance.frmDelivery.Items.Item("RePost").Enabled = false;
                            }
                            catch (Exception x)
                            {

                            }
                            // return false;
                        }
                        else
                        {
                            LTransId = Global.oCompany.GetNewObjectKey();
                            Global.oApplication.SetStatusBarMessage("GRPO Is : " + LTransId, SAPbouiCOM.BoMessageTime.bmt_Short, false);
                            // Root.Instance.frmAccuralSheet.Items.Item("14").Specific.value = LTransId.ToString();
                            if (Global.oCompany.InTransaction)
                                Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            try
                            {
                                SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                if (Global.oServerType == "dst_HANADB")
                                {
                                    MRPQry = "Update \"ODLN\" set  \"U_Status\"='Y',\"U_EMsg\"='' where \"DocEntry\"='" + DocEntry + "'";
                                }
                                else
                                {
                                    MRPQry = "Update ODLN set  U_Status='Y',U_EMsg='' where DocEntry='" + DocEntry + "'";
                                }
                                oRS1.DoQuery(MRPQry);
                            }
                            catch (Exception x)
                            {

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.oApplication.SetStatusBarMessage(ex.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, true);
                string s = ex.ToString();
                Global.oApplication.SetStatusBarMessage("GRPO Is Not Posted Due To : " + s, SAPbouiCOM.BoMessageTime.bmt_Short, true);
                try
                {
                    string MRPQry = "";
                    SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    if (Global.oServerType == "dst_HANADB")
                    {
                        MRPQry = "Update \"ODLN\" set  \"U_EMsg\"='" + s.Replace("'", "").ToString() + "' where \"DocEntry\"='" + DocEntry + "'";
                    }
                    else
                    {
                        MRPQry = "Update ODLN set  U_EMsg='" + s.Replace("'", "").ToString() + "' where DocEntry='" + DocEntry + "'";
                    }
                    oRS1.DoQuery(MRPQry);
                }
                catch (Exception x)
                {

                }
                return false;
            }
            return true;
        }
       
        public static bool AR2APPosting(string DocEntry, string DB)
        {
            try
            {
                string MRPQry = "";
                SAPbobsCOM.Recordset RSMRP = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                if (Global.oServerType == "dst_HANADB")
                {
                    MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\" from \"OINV\" \"M\" inner join \"INV1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";
                }
                else
                {
                    MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\" from \"OINV\" \"M\" inner join \"INV1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";
                }
                RSMRP.DoQuery(MRPQry);
                if (RSMRP.RecordCount > 0)
                {
                    int lretcode;
                    string s = "";
                    string LTransId = "";
                    SAPbobsCOM.Documents Doc;
                    Doc = Global.oCompany1.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseInvoices);
                    if (!Global.oCompany1.InTransaction)
                        Global.oCompany1.StartTransaction();
                    Doc.CardCode = "SGCSFB";//RSMRP.Fields.Item("CardCode").Value;
                    Doc.DocDate = RSMRP.Fields.Item("DocDate").Value;
                    Doc.DocDueDate = RSMRP.Fields.Item("DocDueDate").Value;
                    Doc.TaxDate = RSMRP.Fields.Item("TaxDate").Value;
                    double DocRate = RSMRP.Fields.Item("DiscPrcnt").Value;
                    if (DocRate.ToString().Trim() == "")
                    {
                        DocRate = 0;
                    }
                    if (Convert.ToDouble(DocRate) != 0)
                    {
                        Doc.DiscountPercent = Convert.ToDouble(DocRate);
                    }
                    Doc.UserFields.Fields.Item("U_POEntry").Value = Convert.ToString(RSMRP.Fields.Item("DocEntry").Value);
                    Doc.UserFields.Fields.Item("U_PODocNum").Value = Convert.ToString(RSMRP.Fields.Item("DocNum").Value);
                    Doc.UserFields.Fields.Item("U_POSeries").Value = Convert.ToString(RSMRP.Fields.Item("Series").Value);
                    //Doc.Memo = "D JE P&L Checking";
                    //  string Months = Root.Instance.frmAccuralSheet.Items.Item("4").Specific.value;
                    //  string Years = Root.Instance.frmAccuralSheet.Items.Item("6").Specific.value                   
                    SAPbobsCOM.Recordset oRS = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    //if (Global.oServerType == "dst_HANADB")
                    //{
                    //    MRPQry = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\",\"M1\".\"OcrCode\",\"M1\".\"OcrCode2\",\"M1\".\"OcrCode3\",\"M1\".\"OcrCode4\",\"M1\".\"OcrCode5\",\"M1\".\"LocCode\"   from \"ORDR\" \"M\" inner join \"RDR1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";
                    //}
                    //else
                    //{
                    //    MRPQry = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\",\"M1\".\"OcrCode\",\"M1\".\"OcrCode2\",\"M1\".\"OcrCode3\",\"M1\".\"OcrCode4\",\"M1\".\"OcrCode5\",\"M1\".\"LocCode\"  from \"ORDR\" \"M\" inner join \"RDR1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";
                    //}
                    if (Global.oServerType == "dst_HANADB")
                    {
                        MRPQry = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\",\"M1\".\"OcrCode\",\"M1\".\"OcrCode2\",\"M1\".\"OcrCode3\",\"M1\".\"OcrCode4\",\"M1\".\"OcrCode5\",\"M1\".\"LocCode\",ifnull(M3.\"LineNum\",0) as \"BaseL\",ifnull(M3.\"DocEntry\",0) as \"BaseE\" from \"OINV\" \"M\" inner join \"INV1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" Left join " + DB.ToString().Trim() + ".PDN1 M3 on cast(M1.\"BaseEntry\" as varchar(40))=cast(M3.\"U_POEntry\" as varchar(40)) and cast(M1.\"BaseLine\" as varchar(40))=cast(M3.\"U_LineNum\" as varchar(40)) where \"M\".\"DocEntry\"='" + DocEntry + "'";
                    }
                    else
                    {
                        MRPQry = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\",\"M1\".\"OcrCode\",\"M1\".\"OcrCode2\",\"M1\".\"OcrCode3\",\"M1\".\"OcrCode4\",\"M1\".\"OcrCode5\",\"M1\".\"LocCode\",isnull(M3.\"LineNum\",0) as \"BaseL\",isnull(M3.\"DocEntry\",0) as \"BaseE\"  from \"OINV\" \"M\" inner join \"INV1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" Left join " + DB.ToString().Trim() + ".PDN1 M3 on cast(M1.\"BaseEntry\" as varchar(40))=cast(M3.\"U_POEntry\" as varchar(40)) and cast(M1.\"BaseLine\" as varchar(40))=cast(M3.\"U_LineNum\" as varchar(40)) where \"M\".\"DocEntry\"='" + DocEntry + "'";
                    }
                    oRS.DoQuery(MRPQry);
                    if (oRS.RecordCount > 0)
                    {
                        double LabrCst = 0;
                        for (int l = 1; l <= oRS.RecordCount; l++)
                        {
                            //  LabrCst = LabrCst + Convert.ToDouble(oRS.Fields.Item("DocTotal").Value);
                            //if (LabrCst != 0)
                            //{
                            // - - - - - Line Details Started - - - -
                            Doc.Lines.ItemCode = oRS.Fields.Item("ItemCode").Value;
                            Doc.Lines.Quantity = oRS.Fields.Item("Quantity").Value;
                            Doc.Lines.UnitPrice = oRS.Fields.Item("Price").Value;
                            Doc.Lines.WarehouseCode = oRS.Fields.Item("WhsCode").Value;
                            Doc.Lines.LocationCode = oRS.Fields.Item("LocCode").Value;
                            Doc.Lines.UoMEntry = oRS.Fields.Item("UomEntry").Value;

                            Doc.Lines.UserFields.Fields.Item("U_LineNum").Value = Convert.ToString(oRS.Fields.Item("LineNum").Value);
                            Doc.Lines.UserFields.Fields.Item("U_POEntry").Value = Convert.ToString(RSMRP.Fields.Item("DocEntry").Value);
                            double LDocRate = oRS.Fields.Item("DiscPrcnt").Value;
                            string POLine = Convert.ToString(oRS.Fields.Item("BaseL").Value);
                            string PODocEntry = Convert.ToString(oRS.Fields.Item("BaseE").Value);
                            if (PODocEntry.ToString().Trim() != "0")
                            {
                                Doc.Lines.BaseEntry = Convert.ToInt32(PODocEntry);
                                Doc.Lines.BaseLine = Convert.ToInt32(POLine);
                                Doc.Lines.BaseType = 20;
                            }
                            if (LDocRate.ToString().Trim() == "")
                            {
                                DocRate = 0;
                            }
                            if (Convert.ToDouble(DocRate) != 0)
                            {
                                Doc.DiscountPercent = Convert.ToDouble(DocRate);
                            }
                            string GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode = oRS.Fields.Item("OcrCode").Value;
                            }
                            GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode2").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode2 = oRS.Fields.Item("OcrCode2").Value;
                            }
                            GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode3").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode3 = oRS.Fields.Item("OcrCode3").Value;
                            }
                            GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode4").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode4 = oRS.Fields.Item("OcrCode4").Value;
                            }
                            GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode5").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode5 = oRS.Fields.Item("OcrCode5").Value;
                            }
                            Doc.Lines.Add();

                            // }
                            oRS.MoveNext();
                        }
                        lretcode = Doc.Add();
                        if (lretcode != 0)
                        {
                            if (Global.oCompany1.InTransaction)
                                Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            s = Global.oCompany1.GetLastErrorDescription();
                            Global.oApplication.SetStatusBarMessage("AP Invoice Is Not Posted Due To : " + s, SAPbouiCOM.BoMessageTime.bmt_Short, true);
                            try
                            {
                                SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                if (Global.oServerType == "dst_HANADB")
                                {
                                    MRPQry = "Update \"OINV\" set  \"U_EMsg\"='" + s.Replace("'", "").ToString() + "' where \"DocEntry\"='" + DocEntry + "'";
                                }
                                else
                                {
                                    MRPQry = "Update OINV set  U_EMsg='" + s.Replace("'", "").ToString() + "' where DocEntry='" + DocEntry + "'";
                                }
                                oRS1.DoQuery(MRPQry);
                            }
                            catch (Exception x)
                            {

                            }
                            // return false;
                        }
                        else
                        {
                            LTransId = Global.oCompany1.GetNewObjectKey();
                            Global.oApplication.SetStatusBarMessage("AP Invoice Is : " + LTransId, SAPbouiCOM.BoMessageTime.bmt_Short, false);
                            // Root.Instance.frmAccuralSheet.Items.Item("14").Specific.value = LTransId.ToString();
                            if (Global.oCompany1.InTransaction)
                                Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            try
                            {
                                SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                if (Global.oServerType == "dst_HANADB")
                                {
                                    MRPQry = "Update \"OINV\" set  \"U_Status\"='Y',\"U_EMsg\"='' where \"DocEntry\"='" + DocEntry + "'";
                                }
                                else
                                {
                                    MRPQry = "Update OINV set  U_Status='Y',U_EMsg='' where DocEntry='" + DocEntry + "'";
                                }
                                oRS1.DoQuery(MRPQry);
                            }
                            catch (Exception x)
                            {

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.oApplication.SetStatusBarMessage(ex.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, true);
                return false;
            }
            return true;
        }
        public static bool PO2SOPosting(string DocEntry, string NumAtCard, string VCode, string ToWarehouse)
        {
            try
            {
                string MRPQry = "";
                SAPbobsCOM.Recordset RSMRP = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                if (Global.oServerType == "dst_HANADB")
                {
                    MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\",\"M\".\"Comments\" from \"OPOR\" \"M\" inner join \"POR1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";

                }
                else
                {
                    MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\",\"M\".\"Comments\" from \"OPOR\" \"M\" inner join \"POR1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";

                    // MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\",\"M\".\"Comments\" from \"ODRF\" \"M\" inner join \"DRF1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";
                }
                RSMRP.DoQuery(MRPQry);
                if (RSMRP.RecordCount > 0)
                {
                    //Global.oApplication.SetStatusBarMessage("Access 7 ", SAPbouiCOM.BoMessageTime.bmt_Short, false);
                                                   
                    int lretcode;
                    string s = "";
                    string LTransId = "";
                    string Intercompany = getSingleValue("Select \"U_VSPLIC\" from \"OCRD\" where \"CardCode\"='" + RSMRP.Fields.Item("CardCode").Value + "'");
                    string InterBranch = getSingleValue("Select \"U_VSPLIB\" from \"OCRD\" where \"CardCode\"='" + RSMRP.Fields.Item("CardCode").Value + "'");
                    if (InterBranch == "No" && Intercompany == "Yes")
                    {
                        SAPbobsCOM.Documents Doc;
                        Doc = Global.oCompany1.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts);
                        if (!Global.oCompany1.InTransaction)
                            Global.oCompany1.StartTransaction();
                        Doc.DocObjectCode = SAPbobsCOM.BoObjectTypes.oOrders;
                        //Global.oApplication.SetStatusBarMessage("Access 8 ", SAPbouiCOM.BoMessageTime.bmt_Short, false);
                                                   
                        Doc.CardCode = VCode;//RSMRP.Fields.Item("CardCode").Value;
                        Doc.NumAtCard =Convert.ToString(RSMRP.Fields.Item("DocNum").Value);
                        Doc.DocDate = RSMRP.Fields.Item("DocDate").Value;
                        Doc.DocDueDate = RSMRP.Fields.Item("DocDueDate").Value;
                        Doc.TaxDate = RSMRP.Fields.Item("TaxDate").Value;
                        Doc.Comments = RSMRP.Fields.Item("Comments").Value;
                        double DocRate = RSMRP.Fields.Item("DiscPrcnt").Value;
                        Doc.UserFields.Fields.Item("U_SOEntry").Value = DocEntry;
                        //Global.oApplication.SetStatusBarMessage("Access 8.1 ", SAPbouiCOM.BoMessageTime.bmt_Short, false);
                                                   
                        if (DocRate.ToString().Trim() == "")
                        {
                            DocRate = 0;
                        }
                        if (Convert.ToDouble(DocRate) != 0)
                        {
                            Doc.DiscountPercent = Convert.ToDouble(DocRate);
                        }
                        //Global.oApplication.SetStatusBarMessage("Access 8.2 ", SAPbouiCOM.BoMessageTime.bmt_Short, false);
                       
                        SAPbobsCOM.Recordset oRS = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                        if (Global.oServerType == "dst_HANADB")
                        {

                            MRPQry = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\"   from \"OPOR\" \"M\" inner join \"POR1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" inner join \"OITM\" \"M2\" on \"M1\".\"ItemCode\"=\"M2\".\"ItemCode\" Inner join OITB M3 on \"M2\".\"ItmsGrpCod\"=\"M3\".\"ItmsGrpCod\" where \"M\".\"DocEntry\"='" + DocEntry + "' ";

                            // MRPQry = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\"   from \"ODRF\" \"M\" inner join \"DRF1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" inner join \"OITM\" \"M2\" on \"M1\".\"ItemCode\"=\"M2\".\"ItemCode\" Inner join OITB M3 on \"M2\".\"ItmsGrpCod\"=\"M3\".\"ItmsGrpCod\" where \"M\".\"DocEntry\"='" + DocEntry + "' ";
                        }
                        else
                        {

                            MRPQry = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\"   from \"OPOR\" \"M\" inner join \"POR1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" inner join \"OITM\" \"M2\" on \"M1\".\"ItemCode\"=\"M2\".\"ItemCode\" Inner join OITB M3 on \"M2\".\"ItmsGrpCod\"=\"M3\".\"ItmsGrpCod\" where \"M\".\"DocEntry\"='" + DocEntry + "' ";

                            // MRPQry = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\"  from \"ODRF\" \"M\" inner join \"DRF1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" inner join \"OITM\" \"M2\" on \"M1\".\"ItemCode\"=\"M2\".\"ItemCode\" Inner join OITB M3 on \"M2\".\"ItmsGrpCod\"=\"M3\".\"ItmsGrpCod\" where \"M\".\"DocEntry\"='" + DocEntry + "' ";
                        }
                        oRS.DoQuery(MRPQry);
                        if (oRS.RecordCount > 0)
                        {
                           // Global.oApplication.SetStatusBarMessage("Access 8.3 ", SAPbouiCOM.BoMessageTime.bmt_Short, false);
                       
                            double LabrCst = 0;
                            for (int l = 1; l <= oRS.RecordCount; l++)
                            {
                                string Barcode = getSingleValue("SELECT T0.\"CodeBars\" FROM OITM T0 WHERE T0.\"ItemCode\" ='" + oRS.Fields.Item("ItemCode").Value + "'");
                                string StockCode = getSingleValue1("SELECT T0.\"ItemCode\" FROM OITM T0 WHERE T0.\"CodeBars\" ='" + Barcode + "'");
                               /// Global.oApplication.SetStatusBarMessage("Access 8.4 ", SAPbouiCOM.BoMessageTime.bmt_Short, false);
                       
                                Doc.Lines.ItemCode = StockCode; //oRS.Fields.Item("ItemCode").Value;
                                Doc.Lines.Quantity = oRS.Fields.Item("Quantity").Value;
                                Doc.Lines.UnitPrice = oRS.Fields.Item("Price").Value;
                                Doc.Lines.WarehouseCode = ToWarehouse;
                                string TxCode = getSingleValue1("call ARGIT_SO_TAXCODE ('" + VCode + "' ,'" + StockCode + "', '" + Convert.ToDouble(oRS.Fields.Item("Price").Value) + "','" + ToWarehouse + "')");
                                if (TxCode != "")
                                    Doc.Lines.TaxCode = TxCode;
                               // Global.oApplication.SetStatusBarMessage("Access 8.5 ", SAPbouiCOM.BoMessageTime.bmt_Short, false);
                                string LineId = Convert.ToString(oRS.Fields.Item("LineNum").Value);  // getSingleValue1("SELECT T0.\"LineNum\" FROM DRF1 T0 WHERE T0.\"ItemCode\" ='" + oRS.Fields.Item("ItemCode").Value + "' and T0.\"DocEntry\" ='" + DocEntry + "'");
                                Doc.Lines.UserFields.Fields.Item("U_BaseLine").Value = LineId;
                                Doc.Lines.UserFields.Fields.Item("U_BaseEntry").Value = DocEntry;
                                double LDocRate = oRS.Fields.Item("DiscPrcnt").Value;
                               // Global.oApplication.SetStatusBarMessage("Access 8.6 ", SAPbouiCOM.BoMessageTime.bmt_Short, false);
                                if (LDocRate.ToString().Trim() == "")
                                {
                                    LDocRate = 0;
                                }
                                if (Convert.ToDouble(LDocRate) != 0)
                                {
                                    Doc.Lines.DiscountPercent = Convert.ToDouble(LDocRate);
                                }

                                Doc.Lines.Add();
                               // Global.oApplication.SetStatusBarMessage("Access 8.7 ", SAPbouiCOM.BoMessageTime.bmt_Short, false);
                                // }
                                oRS.MoveNext();
                            }
                           // Global.oApplication.SetStatusBarMessage("Access 9 ", SAPbouiCOM.BoMessageTime.bmt_Short, false);
                       
                            string FrightAmt = getSingleValue("SELECT T0.\"LineTotal\" FROM POR3 T0 WHERE T0.\"DocEntry\"='" + DocEntry + "' and ifnull(T0.\"LineTotal\",0)>0 ");

                            //string FrightAmt = getSingleValue("SELECT T0.\"LineTotal\" FROM DRF2 T0 WHERE T0\"DocEntry\"='" + oRS.Fields.Item("ItemCode").Value + "' where ifnull(T0.\"ExpnsCode\",'')='' ");
                            string TaxCode = getSingleValue("SELECT T0.\"TaxCode\" FROM POR3 T0 WHERE T0.\"DocEntry\"='" + DocEntry + "' and ifnull(T0.\"LineTotal\",0)>0 ");


                            if (FrightAmt != "" && FrightAmt != null)
                            {
                                //Global.oApplication.SetStatusBarMessage("Access 10 ", SAPbouiCOM.BoMessageTime.bmt_Short, false);
                       
                                Doc.Expenses.ExpenseCode = 1;
                                Doc.Expenses.LineTotal = Convert.ToDouble(FrightAmt);
                                Doc.Expenses.TaxCode = TaxCode;
                                Doc.Expenses.Add();
                            }
                            //Global.oApplication.SetStatusBarMessage("Access 11 ", SAPbouiCOM.BoMessageTime.bmt_Short, false);
                       
                            string BPLID = getSingleValue1("SELECT T0.\"BPLid\" FROM OWHS T0 WHERE T0.\"WhsCode\" ='" + ToWarehouse + "'");
                            Doc.BPL_IDAssignedToInvoice = Convert.ToInt32(BPLID);
                            lretcode = Doc.Add();
                            //Global.oApplication.SetStatusBarMessage("Access 12 ", SAPbouiCOM.BoMessageTime.bmt_Short, false);
                       
                            if (lretcode != 0)
                            {
                                if (Global.oCompany1.InTransaction)
                                    Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                s = Global.oCompany1.GetLastErrorDescription();
                                Global.oApplication.SetStatusBarMessage("Sales Order Is Not Posted Due To : " + s, SAPbouiCOM.BoMessageTime.bmt_Short, true);
                                try
                                {
                                    SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                    if (Global.oServerType == "dst_HANADB")
                                    {

                                        MRPQry = "Update \"OPOR\" set  \"U_EMsg\"='" + s.Replace("'", "").ToString() + "' where \"DocEntry\"='" + DocEntry + "'";

                                    }
                                    else
                                    {

                                        MRPQry = "Update \"OPOR\" set  \"U_EMsg\"='" + s.Replace("'", "").ToString() + "' where \"DocEntry\"='" + DocEntry + "'";

                                        //MRPQry = "Update ORDR set  U_EMsg='" + s.Replace("'", "").ToString() + "' where DocEntry='" + DocEntry + "'";
                                    }
                                    oRS1.DoQuery(MRPQry);
                                }
                                catch (Exception x)
                                {

                                }
                                // return false;
                            }
                            else
                            {
                                LTransId = Global.oCompany1.GetNewObjectKey();
                                Global.oApplication.SetStatusBarMessage("Sales Order Is : " + LTransId, SAPbouiCOM.BoMessageTime.bmt_Short, false);
                                // Root.Instance.frmAccuralSheet.Items.Item("14").Specific.value = LTransId.ToString();
                                if (Global.oCompany1.InTransaction)
                                    Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                try
                                {
                                    SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                    if (Global.oServerType == "dst_HANADB")
                                    {

                                        MRPQry = "Update \"OPOR\" set \"U_SOEntry\"='" + LTransId + "',\"U_Status\"='Y',\"U_EMsg\"='' where \"DocEntry\"='" + DocEntry + "'";


                                    }
                                    else
                                    {
                                        MRPQry = "Update \"OPOR\" set  \"U_SOEntry\"='" + LTransId + "',\"U_Status\"='Y',\"U_EMsg\"='' where \"DocEntry\"='" + DocEntry + "'";

                                        //MRPQry = "Update ORDR set  U_Status='Y',U_EMsg='' where DocEntry='" + DocEntry + "'";
                                    }
                                    oRS1.DoQuery(MRPQry);
                                    Root.Instance.frmPurchaseOrder.Items.Item("RePost").Enabled = false;
                                }
                                catch (Exception x)
                                {

                                }

                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Global.oApplication.SetStatusBarMessage(ex.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, true);
                return false;
            }
            return true;
        }
        public static bool PO2SOBranchPosting(string DocEntry, string NumAtCard, string VCode, string ToWarehouse)
        {
            try
            {
                string MRPQry = "";
                SAPbobsCOM.Recordset RSMRP = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                if (Global.oServerType == "dst_HANADB")
                {
                    MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\",\"M\".\"Comments\" from \"OPOR\" \"M\" inner join \"POR1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";

                }
                else
                {
                    MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\",\"M\".\"Comments\" from \"OPOR\" \"M\" inner join \"POR1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";

                    // MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\",\"M\".\"Comments\" from \"ODRF\" \"M\" inner join \"DRF1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";
                }
                RSMRP.DoQuery(MRPQry);
                if (RSMRP.RecordCount > 0)
                {
                    int lretcode;
                    string s = "";
                    string LTransId = "";
                    string Intercompany = getSingleValue("Select \"U_VSPLIC\" from \"OCRD\" where \"CardCode\"='" + RSMRP.Fields.Item("CardCode").Value + "'");
                    string InterBranch = getSingleValue("Select \"U_VSPLIB\" from \"OCRD\" where \"CardCode\"='" + RSMRP.Fields.Item("CardCode").Value + "'");
                    if (InterBranch == "Yes" && Intercompany == "No")
                    {
                        SAPbobsCOM.Documents Doc;
                        Doc = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts);
                        if (!Global.oCompany.InTransaction)
                            Global.oCompany.StartTransaction();
                        Doc.DocObjectCode = SAPbobsCOM.BoObjectTypes.oOrders;

                        Doc.CardCode = VCode;//RSMRP.Fields.Item("CardCode").Value;
                        Doc.NumAtCard = Convert.ToString(RSMRP.Fields.Item("DocNum").Value);
                        Doc.DocDate = RSMRP.Fields.Item("DocDate").Value;
                        Doc.DocDueDate = RSMRP.Fields.Item("DocDueDate").Value;
                        Doc.TaxDate = RSMRP.Fields.Item("TaxDate").Value;
                        Doc.Comments = RSMRP.Fields.Item("Comments").Value;
                        double DocRate = RSMRP.Fields.Item("DiscPrcnt").Value;
                        Doc.UserFields.Fields.Item("U_SOEntry").Value = DocEntry;
                        if (DocRate.ToString().Trim() == "")
                        {
                            DocRate = 0;
                        }
                        if (Convert.ToDouble(DocRate) != 0)
                        {
                            Doc.DiscountPercent = Convert.ToDouble(DocRate);
                        }

                        SAPbobsCOM.Recordset oRS = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                        if (Global.oServerType == "dst_HANADB")
                        {

                            MRPQry = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\"   from \"OPOR\" \"M\" inner join \"POR1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" inner join \"OITM\" \"M2\" on \"M1\".\"ItemCode\"=\"M2\".\"ItemCode\" Inner join OITB M3 on \"M2\".\"ItmsGrpCod\"=\"M3\".\"ItmsGrpCod\" where \"M\".\"DocEntry\"='" + DocEntry + "' ";

                            // MRPQry = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\"   from \"ODRF\" \"M\" inner join \"DRF1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" inner join \"OITM\" \"M2\" on \"M1\".\"ItemCode\"=\"M2\".\"ItemCode\" Inner join OITB M3 on \"M2\".\"ItmsGrpCod\"=\"M3\".\"ItmsGrpCod\" where \"M\".\"DocEntry\"='" + DocEntry + "' ";
                        }
                        else
                        {

                            MRPQry = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\"   from \"OPOR\" \"M\" inner join \"POR1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" inner join \"OITM\" \"M2\" on \"M1\".\"ItemCode\"=\"M2\".\"ItemCode\" Inner join OITB M3 on \"M2\".\"ItmsGrpCod\"=\"M3\".\"ItmsGrpCod\" where \"M\".\"DocEntry\"='" + DocEntry + "' ";

                            // MRPQry = "select \"M1\".\"UomEntry\",\"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\"  from \"ODRF\" \"M\" inner join \"DRF1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" inner join \"OITM\" \"M2\" on \"M1\".\"ItemCode\"=\"M2\".\"ItemCode\" Inner join OITB M3 on \"M2\".\"ItmsGrpCod\"=\"M3\".\"ItmsGrpCod\" where \"M\".\"DocEntry\"='" + DocEntry + "' ";
                        }
                        oRS.DoQuery(MRPQry);
                        if (oRS.RecordCount > 0)
                        {
                            double LabrCst = 0;
                            for (int l = 1; l <= oRS.RecordCount; l++)
                            {
                                //string Barcode = getSingleValue("SELECT T0.\"CodeBars\" FROM OITM T0 WHERE T0.\"ItemCode\" ='" + oRS.Fields.Item("ItemCode").Value + "'");
                                //string StockCode = getSingleValue1("SELECT T0.\"ItemCode\" FROM OITM T0 WHERE T0.\"CodeBars\" ='" + oRS.Fields.Item("ItemCode").Value + "'");

                                Doc.Lines.ItemCode = oRS.Fields.Item("ItemCode").Value;
                                Doc.Lines.Quantity = oRS.Fields.Item("Quantity").Value;
                                Doc.Lines.UnitPrice = oRS.Fields.Item("Price").Value;
                                Doc.Lines.WarehouseCode = ToWarehouse;

                                string TxCode = getSingleValue1("call ARGIT_SO_TAXCODE ('" + VCode + "' ,'" + oRS.Fields.Item("ItemCode").Value + "', '" + Convert.ToDouble(oRS.Fields.Item("Price").Value) + "','" + ToWarehouse + "')");
                                if (TxCode != "")
                                    Doc.Lines.TaxCode = TxCode;
                                string LineId = Convert.ToString(oRS.Fields.Item("LineNum").Value);  // getSingleValue1("SELECT T0.\"LineNum\" FROM DRF1 T0 WHERE T0.\"ItemCode\" ='" + oRS.Fields.Item("ItemCode").Value + "' and T0.\"DocEntry\" ='" + DocEntry + "'");
                                Doc.Lines.UserFields.Fields.Item("U_BaseLine").Value = LineId;
                                Doc.Lines.UserFields.Fields.Item("U_BaseEntry").Value = DocEntry;
                                double LDocRate = oRS.Fields.Item("DiscPrcnt").Value;
                                if (LDocRate.ToString().Trim() == "")
                                {
                                    LDocRate = 0;
                                }
                                if (Convert.ToDouble(LDocRate) != 0)
                                {
                                    Doc.Lines.DiscountPercent = Convert.ToDouble(LDocRate);
                                }

                                Doc.Lines.Add();

                                // }
                                oRS.MoveNext();
                            }
                            string FrightAmt = getSingleValue("SELECT T0.\"LineTotal\" FROM POR3 T0 WHERE T0.\"DocEntry\"='" + DocEntry + "' and ifnull(T0.\"LineTotal\",0)>0 ");

                            //string FrightAmt = getSingleValue("SELECT T0.\"LineTotal\" FROM DRF2 T0 WHERE T0\"DocEntry\"='" + oRS.Fields.Item("ItemCode").Value + "' where ifnull(T0.\"ExpnsCode\",'')='' ");
                            string TaxCode = getSingleValue("SELECT T0.\"TaxCode\" FROM POR3 T0 WHERE T0.\"DocEntry\"='" + DocEntry + "' and ifnull(T0.\"LineTotal\",0)>0 ");


                            if (FrightAmt != "" && FrightAmt != null)
                            {
                                Doc.Expenses.ExpenseCode = 1;
                                Doc.Expenses.LineTotal = Convert.ToDouble(FrightAmt);
                                Doc.Expenses.TaxCode = TaxCode;
                                Doc.Expenses.Add();
                            }
                            string BPLID = getSingleValue1("SELECT T0.\"BPLid\" FROM OWHS T0 WHERE T0.\"WhsCode\" ='" + ToWarehouse + "'");
                            Doc.BPL_IDAssignedToInvoice = Convert.ToInt32(BPLID);
                            lretcode = Doc.Add();
                            if (lretcode != 0)
                            {
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                s = Global.oCompany.GetLastErrorDescription();
                                Global.oApplication.SetStatusBarMessage("Sales Order Is Not Posted Due To : " + s, SAPbouiCOM.BoMessageTime.bmt_Short, true);
                                try
                                {
                                    SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                    if (Global.oServerType == "dst_HANADB")
                                    {

                                        MRPQry = "Update \"OPOR\" set  \"U_EMsg\"='" + s.Replace("'", "").ToString() + "' where \"DocEntry\"='" + DocEntry + "'";

                                    }
                                    else
                                    {

                                        MRPQry = "Update \"OPOR\" set  \"U_EMsg\"='" + s.Replace("'", "").ToString() + "' where \"DocEntry\"='" + DocEntry + "'";

                                        //MRPQry = "Update ORDR set  U_EMsg='" + s.Replace("'", "").ToString() + "' where DocEntry='" + DocEntry + "'";
                                    }
                                    oRS1.DoQuery(MRPQry);
                                }
                                catch (Exception x)
                                {

                                }
                                // return false;
                            }
                            else
                            {
                                LTransId = Global.oCompany.GetNewObjectKey();
                                Global.oApplication.SetStatusBarMessage("Sales Order Is : " + LTransId, SAPbouiCOM.BoMessageTime.bmt_Short, false);
                                // Root.Instance.frmAccuralSheet.Items.Item("14").Specific.value = LTransId.ToString();
                                if (Global.oCompany.InTransaction)
                                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                try
                                {
                                    SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                    if (Global.oServerType == "dst_HANADB")
                                    {

                                        MRPQry = "Update \"OPOR\" set  \"U_SOEntry\"='" + LTransId + "',\"U_Status\"='Y',\"U_EMsg\"='' where \"DocEntry\"='" + DocEntry + "'";


                                    }
                                    else
                                    {
                                        MRPQry = "Update \"OPOR\" set  \"U_SOEntry\"='" + LTransId + "',\"U_Status\"='Y',\"U_EMsg\"='' where \"DocEntry\"='" + DocEntry + "'";

                                        //MRPQry = "Update ORDR set  U_Status='Y',U_EMsg='' where DocEntry='" + DocEntry + "'";
                                    }
                                    oRS1.DoQuery(MRPQry);
                                    Root.Instance.frmPurchaseOrder.Items.Item("RePost").Enabled = false;
                                }
                                catch (Exception x)
                                {

                                }

                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Global.oApplication.SetStatusBarMessage(ex.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, true);
                return false;
            }
            return true;
        }      
      
        public static bool POPosting(string DocEntry, string ItemGroup, string VCode, string CCode)
        {
            try
            {
                string MRPQry = "";
                SAPbobsCOM.Recordset RSMRP = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                if (Global.oServerType == "dst_HANADB")
                {
                    MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\" from \"OPOR\" \"M\" inner join \"POR1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";
                }
                else
                {
                    MRPQry = "select \"M\".\"Series\",\"M\".\"DocEntry\",\"M\".\"DocNum\",\"M\".\"CardCode\",\"M\".\"CardName\",\"M\".\"DocDate\",\"M\".\"DocDueDate\",\"M\".\"TaxDate\",\"M\".\"DiscPrcnt\" from \"OPOR\" \"M\" inner join \"POR1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" where \"M\".\"DocEntry\"='" + DocEntry + "'";
                }
                RSMRP.DoQuery(MRPQry);
                if (RSMRP.RecordCount > 0)
                {
                    int lretcode;
                    string s = "";
                    string LTransId = "";
                    SAPbobsCOM.Documents Doc;
                    Doc = Global.oCompany1.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders);
                    if (!Global.oCompany1.InTransaction)
                        Global.oCompany1.StartTransaction();
                    Doc.CardCode = CCode;
                    // Doc.CardCode = RSMRP.Fields.Item("CardCode").Value;
                    Doc.NumAtCard = Convert.ToString(RSMRP.Fields.Item("DocNum").Value);
                    Doc.DocDate = RSMRP.Fields.Item("DocDate").Value;
                    Doc.DocDueDate = RSMRP.Fields.Item("DocDueDate").Value;
                    Doc.TaxDate = RSMRP.Fields.Item("TaxDate").Value;
                    double DocRate = RSMRP.Fields.Item("DiscPrcnt").Value;
                    if (DocRate.ToString().Trim() == "")
                    {
                        DocRate = 0;
                    }
                    if (Convert.ToDouble(DocRate) != 0)
                    {
                        Doc.DiscountPercent = Convert.ToDouble(DocRate);
                    }
                    Doc.UserFields.Fields.Item("U_POEntry").Value = Convert.ToString(RSMRP.Fields.Item("DocEntry").Value);
                    Doc.UserFields.Fields.Item("U_PODocNum").Value = Convert.ToString(RSMRP.Fields.Item("DocNum").Value);
                    Doc.UserFields.Fields.Item("U_POSeries").Value = Convert.ToString(RSMRP.Fields.Item("Series").Value);
                    //Doc.Memo = "D JE P&L Checking";
                    //  string Months = Root.Instance.frmAccuralSheet.Items.Item("4").Specific.value;
                    //  string Years = Root.Instance.frmAccuralSheet.Items.Item("6").Specific.value                   
                    SAPbobsCOM.Recordset oRS = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    if (Global.oServerType == "dst_HANADB")
                    {
                        MRPQry = "select \"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\",\"M1\".\"OcrCode\",\"M1\".\"OcrCode2\",\"M1\".\"OcrCode3\",\"M1\".\"OcrCode4\",\"M1\".\"OcrCode5\",\"M1\".\"LocCode\"   from \"OPOR\" \"M\" inner join \"POR1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" inner join \"OITM\" \"M2\" on \"M1\".\"ItemCode\"=\"M2\".\"ItemCode\" where \"M\".\"DocEntry\"='" + DocEntry + "' and \"M2\".\"ItmsGrpCod\"='" + ItemGroup + "' ";
                    }
                    else
                    {
                        MRPQry = "select \"M1\".\"LineNum\",\"M1\".\"ItemCode\",\"M1\".\"Quantity\",\"M1\".\"Price\",\"M1\".\"DiscPrcnt\",\"M1\".\"WhsCode\",\"M1\".\"OcrCode\",\"M1\".\"OcrCode2\",\"M1\".\"OcrCode3\",\"M1\".\"OcrCode4\",\"M1\".\"OcrCode5\",\"M1\".\"LocCode\"  from \"OPOR\" \"M\" inner join \"POR1\" \"M1\" on \"M\".\"DocEntry\"=\"M1\".\"DocEntry\" inner join \"OITM\" \"M2\" on \"M1\".\"ItemCode\"=\"M2\".\"ItemCode\" where \"M\".\"DocEntry\"='" + DocEntry + "' and \"M2\".\"ItmsGrpCod\"='" + ItemGroup + "' ";
                    }
                    oRS.DoQuery(MRPQry);
                    if (oRS.RecordCount > 0)
                    {
                        double LabrCst = 0;
                        for (int l = 1; l <= oRS.RecordCount; l++)
                        {
                            //  LabrCst = LabrCst + Convert.ToDouble(oRS.Fields.Item("DocTotal").Value);
                            //if (LabrCst != 0)
                            //{
                            // - - - - - Line Details Started - - - -
                            Doc.Lines.ItemCode = oRS.Fields.Item("ItemCode").Value;
                            Doc.Lines.Quantity = oRS.Fields.Item("Quantity").Value;
                            Doc.Lines.UnitPrice = oRS.Fields.Item("Price").Value;
                            Doc.Lines.WarehouseCode = oRS.Fields.Item("WhsCode").Value;
                            Doc.Lines.LocationCode = oRS.Fields.Item("LocCode").Value;
                            Doc.Lines.UserFields.Fields.Item("U_LineNum").Value = Convert.ToString(oRS.Fields.Item("LineNum").Value);
                            Doc.Lines.UserFields.Fields.Item("U_POEntry").Value = Convert.ToString(RSMRP.Fields.Item("DocEntry").Value);
                            double LDocRate = oRS.Fields.Item("DiscPrcnt").Value;
                            if (LDocRate.ToString().Trim() == "")
                            {
                                DocRate = 0;
                            }
                            if (Convert.ToDouble(DocRate) != 0)
                            {
                                Doc.DiscountPercent = Convert.ToDouble(DocRate);
                            }
                            string GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode = oRS.Fields.Item("OcrCode").Value;
                            }
                            GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode2").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode2 = oRS.Fields.Item("OcrCode2").Value;
                            }
                            GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode3").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode3 = oRS.Fields.Item("OcrCode3").Value;
                            }
                            GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode4").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode4 = oRS.Fields.Item("OcrCode4").Value;
                            }
                            GroupMask = Convert.ToString(oRS.Fields.Item("OcrCode5").Value);
                            if (GroupMask.ToString().Trim() != "")
                            {
                                Doc.Lines.CostingCode5 = oRS.Fields.Item("OcrCode5").Value;
                            }
                            Doc.Lines.Add();

                            // }
                            oRS.MoveNext();
                        }
                        lretcode = Doc.Add();
                        if (lretcode != 0)
                        {
                            if (Global.oCompany1.InTransaction)
                                Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            s = Global.oCompany1.GetLastErrorDescription();
                            Global.oApplication.SetStatusBarMessage("Sales Order Is Not Posted Due To : " + s, SAPbouiCOM.BoMessageTime.bmt_Short, true);
                            try
                            {
                                SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                if (Global.oServerType == "dst_HANADB")
                                {
                                    MRPQry = "Update \"OPOR\" set  \"U_EMsg\"='" + s.Replace("'", "").ToString() + "' where \"DocEntry\"='" + DocEntry + "'";
                                }
                                else
                                {
                                    MRPQry = "Update OPOR set  U_EMsg='" + s.Replace("'", "").ToString() + "' where DocEntry='" + DocEntry + "'";
                                }
                                oRS1.DoQuery(MRPQry);
                            }
                            catch (Exception x)
                            {

                            }
                            // return false;
                        }
                        else
                        {
                            LTransId = Global.oCompany1.GetNewObjectKey();
                            Global.oApplication.SetStatusBarMessage("Sales Order Is : " + LTransId, SAPbouiCOM.BoMessageTime.bmt_Short, false);
                            // Root.Instance.frmAccuralSheet.Items.Item("14").Specific.value = LTransId.ToString();
                            if (Global.oCompany1.InTransaction)
                                Global.oCompany1.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            try
                            {
                                SAPbobsCOM.Recordset oRS1 = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                if (Global.oServerType == "dst_HANADB")
                                {
                                    MRPQry = "Update \"OPOR\" set  \"U_Status\"='Y',\"U_EMsg\"='' where \"DocEntry\"='" + DocEntry + "'";
                                }
                                else
                                {
                                    MRPQry = "Update OPOR set  U_Status='Y',U_EMsg='' where DocEntry='" + DocEntry + "'";
                                }
                                oRS1.DoQuery(MRPQry);
                            }
                            catch (Exception x)
                            {

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.oApplication.SetStatusBarMessage(ex.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, true);
                return false;
            }
            return true;
        }
        public static void ConnectSAPDB(string servername, string dbname, string DBUserName, string DBPwd, string UserName, string Password)
        {

            try
            {
                string[] MyArr;
                string Str = "";
                try
                {
                    Global.oCompany1.Disconnect();
                }
                catch (Exception ex)
                {
                    //  WriteLog(ex.ToString());
                }
                if ((Global.oCompany1 == null))
                {
                    Global.oCompany1 = new SAPbobsCOM.Company();
                }
                Global.oCompany1.CompanyDB = dbname;
                Global.oCompany1.UserName = UserName;
                Global.oCompany1.Password = Password;
                Global.oCompany1.Server = servername;
                Global.oCompany1.DbUserName = DBUserName;
                Global.oCompany1.DbPassword = DBPwd;
                if (Global.oServerType == "dst_HANADB")
                {
                    Global.oCompany1.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;
                }
                else if (Global.oServerType == "2005")
                {
                    Global.oCompany1.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2005;
                }
                else if (Global.oServerType == "2012")
                {
                    Global.oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012;
                }
                else if (Global.oServerType == "2014")
                {
                    Global.oCompany1.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014;
                }
                else if (Global.oServerType == "2016")
                {
                    Global.oCompany1.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014;
                }
                else
                {
                    Global.oCompany1.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008;
                }
                int connection = Global.oCompany1.Connect();
                if (connection != 0)
                {
                    string Error = Global.oCompany1.GetLastErrorDescription().ToString();
                    Global.oApplication.SetStatusBarMessage(Error, SAPbouiCOM.BoMessageTime.bmt_Short, false);
                }
                else
                {
                    Global.oApplication.SetStatusBarMessage("This " + dbname + " SAP DataBase Connected Successfully", SAPbouiCOM.BoMessageTime.bmt_Short, false);
                    //WriteLog("This " + dbname + " SAP DataBase Connected Successfully");
                }

            }
            catch (Exception ex)
            {
                Global.oApplication.SetStatusBarMessage(ex.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, true);
            }

        }
        public static void ConnectSAPDB2(string servername, string dbname, string DBUserName, string DBPwd, string UserName, string Password)
        {

            try
            {
                string[] MyArr;
                string Str = "";
                try
                {
                    Global.oCompany3.Disconnect();
                }
                catch (Exception ex)
                {
                    //  WriteLog(ex.ToString());
                }
                if ((Global.oCompany3 == null))
                {
                    Global.oCompany3 = new SAPbobsCOM.Company();
                }
                Global.oCompany3.CompanyDB = dbname;
                Global.oCompany3.UserName = UserName;
                Global.oCompany3.Password = Password;
                Global.oCompany3.Server = servername;
                Global.oCompany3.DbUserName = DBUserName;
                Global.oCompany3.DbPassword = DBPwd;
                if (Global.oServerType == "dst_HANADB")
                {
                    Global.oCompany3.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;
                }
                else if (Global.oServerType == "2005")
                {
                    Global.oCompany3.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2005;
                }
                else if (Global.oServerType == "2012")
                {
                    Global.oCompany3.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012;
                }
                else if (Global.oServerType == "2014")
                {
                    Global.oCompany3.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014;
                }
                else if (Global.oServerType == "2016")
                {
                    Global.oCompany3.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014;
                }
                else
                {
                    Global.oCompany3.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008;
                }
                int connection = Global.oCompany3.Connect();
                if (connection != 0)
                {
                    string Error = Global.oCompany3.GetLastErrorDescription().ToString();
                    Global.oApplication.SetStatusBarMessage(Error, SAPbouiCOM.BoMessageTime.bmt_Short, false);
                }
                else
                {
                    Global.oApplication.SetStatusBarMessage("This " + dbname + " SAP DataBase Connected Successfully", SAPbouiCOM.BoMessageTime.bmt_Short, false);
                    //WriteLog("This " + dbname + " SAP DataBase Connected Successfully");
                }

            }
            catch (Exception ex)
            {
                Global.oApplication.SetStatusBarMessage(ex.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, true);
            }

        }
        public static void ConnectSAPDB1(string servername, string dbname, string DBUserName, string DBPwd, string UserName, string Password)
        {

            try
            {
                string[] MyArr;
                string Str = "";
                try
                {
                    Global.oCompany2.Disconnect();
                }
                catch (Exception ex)
                {
                    //  WriteLog(ex.ToString());
                }
                if ((Global.oCompany2 == null))
                {
                    Global.oCompany2 = new SAPbobsCOM.Company();
                }
                Global.oCompany2.CompanyDB = dbname;
                Global.oCompany2.UserName = UserName;
                Global.oCompany2.Password = Password;
                Global.oCompany2.Server = servername;
                Global.oCompany2.DbUserName = DBUserName;
                Global.oCompany2.DbPassword = DBPwd;
                if (Global.oServerType == "dst_HANADB")
                {
                    Global.oCompany2.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;
                }
                else if (Global.oServerType == "2005")
                {
                    Global.oCompany2.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2005;
                }
                else if (Global.oServerType == "2012")
                {
                    Global.oCompany2.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012;
                }
                else if (Global.oServerType == "2014")
                {
                    Global.oCompany2.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014;
                }
                else if (Global.oServerType == "2016")
                {
                    Global.oCompany2.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014;
                }
                else
                {
                    Global.oCompany2.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008;
                }
                int connection = Global.oCompany2.Connect();
                if (connection != 0)
                {
                    string Error = Global.oCompany2.GetLastErrorDescription().ToString();
                    Global.oApplication.SetStatusBarMessage(Error, SAPbouiCOM.BoMessageTime.bmt_Short, false);
                }
                else
                {
                    Global.oApplication.SetStatusBarMessage("This " + dbname + " SAP DataBase Connected Successfully", SAPbouiCOM.BoMessageTime.bmt_Short, false);
                    //WriteLog("This " + dbname + " SAP DataBase Connected Successfully");
                }

            }
            catch (Exception ex)
            {
                Global.oApplication.SetStatusBarMessage(ex.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, true);
            }

        }
        #region Load XML File
        public static void LoadXMLForm(string fileName)
        {

            try
            {
                System.Xml.XmlDocument oXmlDoc = null;

                oXmlDoc = new System.Xml.XmlDocument();

                // load the content of the XML Files
                string sPath = null;

                sPath = System.Windows.Forms.Application.StartupPath.ToString();
                //sPath = System.IO.Directory.GetParent(sPath).ToString();

                oXmlDoc.Load(sPath + "\\" + fileName);

                // load the form to the SBO application in one batch
                string sXML = oXmlDoc.InnerXml.ToString();
                Global.oApplication.LoadBatchActions(ref sXML);

            }
            catch (Exception ex)
            {
                Global.oApplication.MessageBox(ex.Message, 1, "Ok", "", "");
            }
        }
        #endregion
        #region Query Excution
        public static SAPbobsCOM.Recordset DoQuery(string strSql)
        {
            try
            {
                SAPbobsCOM.Recordset rsetCode = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rsetCode.DoQuery(strSql);
                return rsetCode;
            }
            catch (Exception ex)
            {
                Global.oApplication.StatusBar.SetText(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                // ExceptionHandler(ex);
                return null;
            }
            finally
            {

            }
        }
        #endregion
        #region "       Common For Data Base Creation ...   "

        public static bool UDOExists(string code)
        {
            GC.Collect();
            SAPbobsCOM.UserObjectsMD v_UDOMD = default(SAPbobsCOM.UserObjectsMD);
            bool v_ReturnCode = false;
            v_UDOMD = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD);
            v_ReturnCode = v_UDOMD.GetByKey(code);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(v_UDOMD);
            v_UDOMD = null;
            return v_ReturnCode;
        }

        public static bool CreateTable(string TableName, string TableDesc, SAPbobsCOM.BoUTBTableType TableType)
        {
            bool functionReturnValue = false;
            functionReturnValue = false;
            int v_RetVal = 0;
            int v_ErrCode = 0;
            string v_ErrMsg = "";
            try
            {
                if (!TableExists(TableName))
                {
                    SAPbobsCOM.UserTablesMD v_UserTableMD = default(SAPbobsCOM.UserTablesMD);
                    Global.oApplication.StatusBar.SetText("Creating Table " + TableName + " ...................", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
                    v_UserTableMD = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserTables);
                    v_UserTableMD.TableName = TableName;
                    v_UserTableMD.TableDescription = TableDesc;
                    v_UserTableMD.TableType = TableType;
                    Global.v_RetVal = v_UserTableMD.Add();
                    if (Global.v_RetVal != 0)
                    {
                        //Global.oCompany.GetLastError(v_ErrCode, v_ErrMsg)
                        v_ErrCode = Global.oCompany.GetLastErrorCode();
                        v_ErrMsg = Global.oCompany.GetLastErrorDescription();
                        Global.oApplication.StatusBar.SetText("Failed to Create Table " + TableDesc + v_ErrCode + " " + v_ErrMsg, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(v_UserTableMD);
                        v_UserTableMD = null;
                        return false;
                    }
                    else
                    {
                        Global.oApplication.StatusBar.SetText("[" + TableName + "] - " + TableDesc + " Created Successfully!!!", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(v_UserTableMD);
                        v_UserTableMD = null;
                        return true;
                    }
                }
                else
                {
                    GC.Collect();
                    return false;
                }
            }
            catch (Exception ex)
            {
                // Global.oApplication.StatusBar.SetText(Global.addonName + ":> " + ex.Message + " @ " + ex.Source);
            }
            return functionReturnValue;
        }

        public static bool ColumnExists(string TableName, string FieldID)
        {
            bool oFlag = true;
            try
            {
                SAPbobsCOM.Recordset rs = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                //rs.DoQuery("Select 1 from [CUFD] Where TableID='" + TableName.Trim() + "' and AliasID='" + FieldID.Trim() + "'");
                rs.DoQuery("Select * from CUFD where \"TableID\" = '" + TableName + "' and \"AliasID\" = '" + FieldID + "'");

                if (rs.EoF)
                    oFlag = false;
                System.Runtime.InteropServices.Marshal.ReleaseComObject(rs);
                rs = null;
                GC.Collect();

            }
            catch (Exception ex)
            {
                // Global.oApplication.StatusBar.SetText(ex.Message);
            }
            return oFlag;
        }

        public static bool UDFExists(string TableName, string FieldID)
        {
            bool oFlag = true;
            try
            {
                SAPbobsCOM.Recordset rs = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                dynamic aa = "Select * from CUFD where \"TableID\" = '" + TableName + "' and \"AliasID\" = '" + FieldID + "'";
                rs.DoQuery("Select * from CUFD where \"TableID\" = '" + TableName + "' and \"AliasID\" = '" + FieldID + "'");
                if (rs.EoF)
                    oFlag = false;
                System.Runtime.InteropServices.Marshal.ReleaseComObject(rs);
                rs = null;
                GC.Collect();
                return oFlag;
            }
            catch (Exception ex)
            {
                // Global.oApplication.StatusBar.SetText(ex.Message);
            }
            return oFlag;
        }

        public static bool TableExists(string TableName)
        {
            SAPbobsCOM.UserTablesMD oTables = default(SAPbobsCOM.UserTablesMD);
            bool oFlag = false;
            oTables = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserTables);
            oFlag = oTables.GetByKey(TableName);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oTables);
            return oFlag;
        }

        public static bool CreateUserFieldsComboBox(string TableName, string FieldName, string FieldDescription, SAPbobsCOM.BoFieldTypes type, int size = 0, SAPbobsCOM.BoFldSubTypes subType = SAPbobsCOM.BoFldSubTypes.st_None, string LinkedTable = "", string[,] ComboValidValues = null, string DefaultValidValues = "")
        {
            bool oFlag = true;
            try
            {
                //If TableName.StartsWith("@") = False Then
                if (!UDFExists(TableName, FieldName))
                {
                    SAPbobsCOM.UserFieldsMD v_UserField = default(SAPbobsCOM.UserFieldsMD);
                    v_UserField = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields);
                    v_UserField.TableName = TableName;
                    v_UserField.Name = FieldName;
                    v_UserField.Description = FieldDescription;
                    v_UserField.Type = type;
                    if (type != SAPbobsCOM.BoFieldTypes.db_Date)
                    {
                        if (size != 0)
                        {
                            v_UserField.Size = size;
                        }
                    }
                    if (subType != SAPbobsCOM.BoFldSubTypes.st_None)
                    {
                        v_UserField.SubType = subType;
                    }

                    for (Int16 i = 0; i <= ComboValidValues.GetLength(0) - 1; i++)
                    {
                        if (i > 0)
                            v_UserField.ValidValues.Add();
                        v_UserField.ValidValues.Value = ComboValidValues[i, 0];
                        v_UserField.ValidValues.Description = ComboValidValues[i, 1];
                    }
                    if (!string.IsNullOrEmpty(DefaultValidValues))
                        v_UserField.DefaultValue = DefaultValidValues;

                    if (!string.IsNullOrEmpty(LinkedTable))
                        v_UserField.LinkedTable = LinkedTable;
                    Global.v_RetVal = v_UserField.Add();
                    if (Global.v_RetVal != 0)
                    {
                        int v_ErrCode = Global.oCompany.GetLastErrorCode();
                        string v_ErrMsg = Global.oCompany.GetLastErrorDescription();

                        //venki 29032017
                        //Global.oApplication.StatusBar.SetText("Failed to add UserField " + FieldDescription + " - " + v_ErrCode + " " + v_ErrMsg, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(v_UserField);
                        v_UserField = null;
                        //  return false;
                        oFlag = false;
                    }
                    else
                    {
                        Global.oApplication.StatusBar.SetText("[" + TableName + "] - " + FieldDescription + " added successfully!!!", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(v_UserField);
                        v_UserField = null;
                        oFlag = true;

                    }

                }
                else
                {
                    oFlag = false;
                }
                // End If
            }
            catch (Exception ex)
            {
                //  Global.oApplication.MessageBox(ex.Message);
            }
            return oFlag;
        }

        public static bool CreateUserFields(string TableName, string FieldName, string FieldDescription, SAPbobsCOM.BoFieldTypes type, int size = 0, SAPbobsCOM.BoFldSubTypes subType = SAPbobsCOM.BoFldSubTypes.st_None, string LinkedTable = "", string DefaultValue = "")
        {
            bool oFlag = true;
            try
            {
                if (TableName.StartsWith("@") == true)
                {
                    if (!ColumnExists(TableName, FieldName))
                    {
                        SAPbobsCOM.UserFieldsMD v_UserField = default(SAPbobsCOM.UserFieldsMD);
                        v_UserField = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields);
                        v_UserField.TableName = TableName;
                        v_UserField.Name = FieldName;
                        v_UserField.Description = FieldDescription;
                        v_UserField.Type = type;
                        if (type != SAPbobsCOM.BoFieldTypes.db_Date)
                        {
                            if (size != 0)
                            {
                                v_UserField.Size = size;
                            }
                        }
                        if (subType != SAPbobsCOM.BoFldSubTypes.st_None)
                        {
                            v_UserField.SubType = subType;
                        }
                        if (!string.IsNullOrEmpty(LinkedTable))
                            v_UserField.LinkedTable = LinkedTable;
                        if (!string.IsNullOrEmpty(DefaultValue))
                            v_UserField.DefaultValue = DefaultValue;

                        Global.v_RetVal = v_UserField.Add();
                        if (Global.v_RetVal != 0)
                        {
                            int v_ErrCode = Global.oCompany.GetLastErrorCode();
                            string v_ErrMsg = Global.oCompany.GetLastErrorDescription();
                            //venki29032017
                            // Global.oApplication.StatusBar.SetText("Failed to add UserField masterid" + v_ErrCode + " " + v_ErrMsg, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(v_UserField);
                            v_UserField = null;
                            oFlag = false;
                        }
                        else
                        {
                            Global.oApplication.StatusBar.SetText("[" + TableName + "] - " + FieldDescription + " added successfully!!!", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(v_UserField);
                            v_UserField = null;
                            oFlag = true;
                        }
                    }
                    else
                    {
                        oFlag = false;
                    }
                }

                if (TableName.StartsWith("@") == false)
                {
                    if (!UDFExists(TableName, FieldName))
                    {
                        SAPbobsCOM.UserFieldsMD v_UserField = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields);
                        v_UserField.TableName = TableName;
                        v_UserField.Name = FieldName;
                        v_UserField.Description = FieldDescription;
                        v_UserField.Type = type;
                        if (type != SAPbobsCOM.BoFieldTypes.db_Date)
                        {
                            if (size != 0)
                            {
                                v_UserField.Size = size;
                            }
                        }
                        if (subType != SAPbobsCOM.BoFldSubTypes.st_None)
                        {
                            v_UserField.SubType = subType;
                        }
                        if (!string.IsNullOrEmpty(LinkedTable))
                            v_UserField.LinkedTable = LinkedTable;
                        Global.v_RetVal = v_UserField.Add();
                        if (Global.v_RetVal != 0)
                        {
                            int v_ErrCode = Global.oCompany.GetLastErrorCode();
                            string v_ErrMsg = Global.oCompany.GetLastErrorDescription();
                            //commented on 29032017 venki
                            // Global.oApplication.StatusBar.SetText("Failed to add UserField " + FieldDescription + " - " + v_ErrCode + " " + v_ErrMsg, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(v_UserField);
                            v_UserField = null;
                            oFlag = false;
                        }
                        else
                        {
                            Global.oApplication.StatusBar.SetText("[" + TableName + "] - " + FieldDescription + " added successfully!!!", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(v_UserField);
                            v_UserField = null;
                            oFlag = true;
                        }

                    }
                    else
                    {
                        oFlag = false;
                    }
                }
            }
            catch (Exception ex)
            {
                //  Global.oApplication.MessageBox(ex.Message);
            }
            return oFlag;
        }

        public static bool RegisterUDO(string UDOCode, string UDOName, SAPbobsCOM.BoUDOObjType UDOType, string[,] FindField, string UDOHTableName, string UDODTableName = "", string ChildTable = "", string ChildTable1 = "", string ChildTable2 = "", string ChildTable3 = "",
        string ChildTable4 = "", string ChildTable5 = "", string ChildTable6 = "", string ChildTable7 = "", string ChildTable8 = "", string ChildTable9 = "", SAPbobsCOM.BoYesNoEnum LogOption = SAPbobsCOM.BoYesNoEnum.tNO)
        {
            bool functionReturnValue = false;
            bool ActionSuccess = false;
            try
            {
                functionReturnValue = false;
                SAPbobsCOM.UserObjectsMD v_udoMD = default(SAPbobsCOM.UserObjectsMD);
                v_udoMD = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD);
                v_udoMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tYES;
                v_udoMD.CanClose = SAPbobsCOM.BoYesNoEnum.tYES;
                v_udoMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tNO;
                v_udoMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tYES;
                v_udoMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES;
                v_udoMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES;
                v_udoMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tYES;
                v_udoMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES;
                v_udoMD.Code = UDOCode;
                v_udoMD.Name = UDOName;
                v_udoMD.TableName = UDOHTableName;
                if (!string.IsNullOrEmpty(UDODTableName))
                {
                    v_udoMD.ChildTables.TableName = UDODTableName;
                    v_udoMD.ChildTables.Add();
                }

                if (!string.IsNullOrEmpty(ChildTable))
                {
                    v_udoMD.ChildTables.TableName = ChildTable;
                    v_udoMD.ChildTables.Add();
                }
                if (!string.IsNullOrEmpty(ChildTable1))
                {
                    v_udoMD.ChildTables.TableName = ChildTable1;
                    v_udoMD.ChildTables.Add();
                }
                if (!string.IsNullOrEmpty(ChildTable2))
                {
                    v_udoMD.ChildTables.TableName = ChildTable2;
                    v_udoMD.ChildTables.Add();
                }
                if (!string.IsNullOrEmpty(ChildTable3))
                {
                    v_udoMD.ChildTables.TableName = ChildTable3;
                    v_udoMD.ChildTables.Add();
                }
                if (!string.IsNullOrEmpty(ChildTable4))
                {
                    v_udoMD.ChildTables.TableName = ChildTable4;
                    v_udoMD.ChildTables.Add();
                }
                if (!string.IsNullOrEmpty(ChildTable5))
                {
                    v_udoMD.ChildTables.TableName = ChildTable5;
                    v_udoMD.ChildTables.Add();
                }
                if (!string.IsNullOrEmpty(ChildTable6))
                {
                    v_udoMD.ChildTables.TableName = ChildTable6;
                    v_udoMD.ChildTables.Add();
                }
                if (!string.IsNullOrEmpty(ChildTable7))
                {
                    v_udoMD.ChildTables.TableName = ChildTable7;
                    v_udoMD.ChildTables.Add();
                }
                if (!string.IsNullOrEmpty(ChildTable8))
                {
                    v_udoMD.ChildTables.TableName = ChildTable8;
                    v_udoMD.ChildTables.Add();
                }
                if (!string.IsNullOrEmpty(ChildTable9))
                {
                    v_udoMD.ChildTables.TableName = ChildTable9;
                    v_udoMD.ChildTables.Add();
                }

                if (LogOption == SAPbobsCOM.BoYesNoEnum.tYES)
                {
                    v_udoMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES;
                    v_udoMD.LogTableName = "A" + UDOHTableName;
                }
                v_udoMD.ObjectType = UDOType;
                for (Int16 i = 0; i <= FindField.GetLength(0) - 1; i++)
                {
                    if (i > 0)
                        v_udoMD.FindColumns.Add();
                    v_udoMD.FindColumns.ColumnAlias = FindField[i, 0];
                    v_udoMD.FindColumns.ColumnDescription = FindField[i, 1];
                }

                if (v_udoMD.Add() == 0)
                {
                    functionReturnValue = true;
                    if (Global.oCompany.InTransaction)
                        Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    Global.oApplication.StatusBar.SetText("Successfully Registered UDO >" + UDOCode + ">" + UDOName + " >" + Global.oCompany.GetLastErrorDescription(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                }
                else
                {
                    Global.oApplication.StatusBar.SetText("Failed to Register UDO >" + UDOCode + ">" + UDOName + " >" + Global.oCompany.GetLastErrorDescription(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                    //MessageBox.Show(Global.oCompany.GetLastErrorDescription)
                    functionReturnValue = false;
                }
                System.Runtime.InteropServices.Marshal.ReleaseComObject(v_udoMD);
                v_udoMD = null;
                GC.Collect();
                if (ActionSuccess == false & Global.oCompany.InTransaction)
                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
            }
            catch (Exception ex)
            {
                if (Global.oCompany.InTransaction)
                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
            }
            return functionReturnValue;
        }

        public static bool RegisterUDOForDefaultForm(string UDOCode, string UDOName, SAPbobsCOM.BoUDOObjType UDOType, string[,] FindField, string UDOHTableName, string UDODTableName = "", string ChildTable = "", SAPbobsCOM.BoYesNoEnum LogOption = SAPbobsCOM.BoYesNoEnum.tNO)
        {
            bool functionReturnValue = false;
            bool ActionSuccess = false;
            try
            {
                functionReturnValue = false;
                SAPbobsCOM.UserObjectsMD v_udoMD = default(SAPbobsCOM.UserObjectsMD);
                v_udoMD = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD);
                v_udoMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tYES;
                v_udoMD.CanClose = SAPbobsCOM.BoYesNoEnum.tYES;
                v_udoMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES;
                v_udoMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tYES;
                v_udoMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES;
                v_udoMD.CanLog = SAPbobsCOM.BoYesNoEnum.tNO;
                v_udoMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tYES;
                v_udoMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES;
                v_udoMD.Code = UDOCode;
                v_udoMD.Name = UDOName;
                v_udoMD.TableName = UDOHTableName;

                if (!string.IsNullOrEmpty(UDODTableName))
                {
                    v_udoMD.ChildTables.TableName = UDODTableName;
                    v_udoMD.ChildTables.Add();
                }

                if (!string.IsNullOrEmpty(ChildTable))
                {
                    v_udoMD.ChildTables.TableName = ChildTable;
                    v_udoMD.ChildTables.Add();
                }
                if (LogOption == SAPbobsCOM.BoYesNoEnum.tYES)
                {
                    v_udoMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES;
                    v_udoMD.LogTableName = "A" + UDOHTableName;
                }
                v_udoMD.ObjectType = UDOType;
                for (Int16 i = 0; i <= FindField.GetLength(0) - 1; i++)
                {
                    if (i > 0)
                        v_udoMD.FindColumns.Add();
                    v_udoMD.FindColumns.ColumnAlias = FindField[i, 0];
                    v_udoMD.FindColumns.ColumnDescription = FindField[i, 1];
                }
                for (Int16 i = 0; i <= FindField.GetLength(0) - 1; i++)
                {
                    if (i > 0)
                        v_udoMD.FormColumns.Add();
                    v_udoMD.FormColumns.FormColumnAlias = FindField[i, 0];
                    v_udoMD.FormColumns.FormColumnDescription = FindField[i, 1];
                }

                if (v_udoMD.Add() == 0)
                {
                    functionReturnValue = true;
                    if (Global.oCompany.InTransaction)
                        Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    Global.oApplication.StatusBar.SetText("Successfully Registered UDO >" + UDOCode + ">" + UDOName + " >" + Global.oCompany.GetLastErrorDescription(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                }
                else
                {
                    Global.oApplication.StatusBar.SetText("Failed to Register UDO >" + UDOCode + ">" + UDOName + " >" + Global.oCompany.GetLastErrorDescription(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                    //MessageBox.Show(Global.oCompany.GetLastErrorDescription)
                    functionReturnValue = false;
                }
                System.Runtime.InteropServices.Marshal.ReleaseComObject(v_udoMD);
                v_udoMD = null;
                GC.Collect();
                if (ActionSuccess == false & Global.oCompany.InTransaction)
                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
            }
            catch (Exception ex)
            {
                if (Global.oCompany.InTransaction)
                    Global.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
            }
            return functionReturnValue;
        }
        public static void DoOpenLinkedObjectForm(string FormUniqueID, string ActivateMenuItem, string FindItemUID, string FindItemUIDValue, string Series)
        {
            try
            {
                SAPbouiCOM.Form oForm = default(SAPbouiCOM.Form);
                bool Bool = false;

                for (int frm = 0; frm <= Global.oApplication.Forms.Count - 1; frm++)
                {
                    if (Global.oApplication.Forms.Item(frm).UniqueID == FormUniqueID)
                    {
                        oForm = Global.oApplication.Forms.Item(FormUniqueID);
                        oForm.Close();
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
                if (Bool == false)
                {
                    Global.oApplication.ActivateMenuItem(ActivateMenuItem);
                    oForm = Global.oApplication.Forms.ActiveForm;
                    oForm.Select();
                    oForm.Freeze(true);
                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE;
                    oForm.Items.Item(FindItemUID).Enabled = true;
                    oForm.Items.Item("88").Enabled = true;
                    SAPbouiCOM.ComboBox oCombox = oForm.Items.Item("88").Specific;
                    oCombox.Select(Series.ToString(), SAPbouiCOM.BoSearchKey.psk_ByValue);
                    oForm.Items.Item(FindItemUID).Specific.Value = FindItemUIDValue.ToString();
                    oForm.Items.Item("1").Click();
                    oForm.Mode = BoFormMode.fm_VIEW_MODE;
                    oForm.Freeze(false);
                }
            }
            catch (Exception ex)
            {
                //  oGfun.StatusBarErrorMsg("" + ex.Message);
            }
            finally
            {
            }
        }
        public static void DoOpenLinkedObjectFormUDO(string FormUniqueID, string ActivateMenuItem, string FindItemUID, string FindItemUIDValue, string Series)
        {
            try
            {
                SAPbouiCOM.Form oForm = default(SAPbouiCOM.Form);
                bool Bool = false;

                for (int frm = 0; frm <= Global.oApplication.Forms.Count - 1; frm++)
                {
                    if (Global.oApplication.Forms.Item(frm).UniqueID == FormUniqueID)
                    {
                        oForm = Global.oApplication.Forms.Item(FormUniqueID);
                        oForm.Close();
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
                if (Bool == false)
                {
                    Global.oApplication.ActivateMenuItem(ActivateMenuItem);
                    oForm = Global.oApplication.Forms.ActiveForm;
                    oForm.Select();
                    oForm.Freeze(true);
                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE;
                    oForm.Items.Item(FindItemUID).Enabled = true;
                    oForm.Items.Item("33").Enabled = true;
                    SAPbouiCOM.ComboBox oCombox = oForm.Items.Item("33").Specific;
                    oCombox.Select(Series.ToString(), SAPbouiCOM.BoSearchKey.psk_ByValue);
                    oForm.Items.Item(FindItemUID).Specific.Value = FindItemUIDValue.ToString();
                    oForm.Items.Item("1").Click();
                    oForm.Mode = BoFormMode.fm_VIEW_MODE;
                    oForm.Freeze(false);
                }
            }
            catch (Exception ex)
            {
                //  oGfun.StatusBarErrorMsg("" + ex.Message);
            }
            finally
            {
            }
        }

        public static void DoOpenLinkedMasterObjectForm(string FormUniqueID, string ActivateMenuItem, string FindItemUID, string FindItemUIDValue)
        {
            try
            {
                SAPbouiCOM.Form oForm = default(SAPbouiCOM.Form);
                bool Bool = false;
                for (int frm = 0; frm <= Global.oApplication.Forms.Count - 1; frm++)
                {
                    if (Global.oApplication.Forms.Item(frm).UniqueID == FormUniqueID)
                    {
                        oForm = Global.oApplication.Forms.Item(FormUniqueID);
                        oForm.Close();
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
                if (Bool == false)
                {
                    Global.oApplication.ActivateMenuItem(ActivateMenuItem);
                    oForm = Global.oApplication.Forms.ActiveForm;
                    oForm.Select();
                    oForm.Freeze(true);
                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE;
                    oForm.Items.Item(FindItemUID).Enabled = true;
                    //oForm.Items.Item("22").Enabled = true;
                    //SAPbouiCOM.ComboBox oCombox = oForm.Items.Item("22").Specific;
                    //oCombox.Select(Series.ToString(), SAPbouiCOM.BoSearchKey.psk_ByValue);
                    oForm.Items.Item(FindItemUID).Specific.Value = FindItemUIDValue.ToString();
                    oForm.Items.Item("1").Click();
                    oForm.Mode = BoFormMode.fm_VIEW_MODE;
                    oForm.Freeze(false);
                }
            }
            catch (Exception ex)
            {
                //  oGfun.StatusBarErrorMsg("" + ex.Message);
            }
            finally
            {
            }
        }

        #endregion
        #region // combox box load and clear
        public static void Clear_Combo(SAPbouiCOM.ComboBox oComboBox)
        {
            try
            {
                int intCount = oComboBox.ValidValues.Count;
                // Remove the Combo Box Value Based On Count ...
                if (intCount > 0)
                {
                    while (intCount > 0)
                    {
                        //If oComboBox.Value(intCount - 1).ToString.Trim <> "" Then _
                        //  Try
                        oComboBox.ValidValues.Remove(intCount - 1, SAPbouiCOM.BoSearchKey.psk_Index);
                        // Catch ex As Exception
                        // MsgBox(ex)

                        //  Finally
                        intCount = intCount - 1;
                        //  End Try

                    }

                }
            }
            catch (Exception ex)
            {


            }
        }
        public static void SetComboBoxValueRefresh(SAPbouiCOM.ComboBox oComboBox, string strQry)
        {
            try
            {
                SAPbobsCOM.Recordset rsetValidValue = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                Clear_Combo(oComboBox);
                rsetValidValue.DoQuery(strQry);
                rsetValidValue.MoveFirst();

                if (rsetValidValue.RecordCount > 0)
                {
                    for (int j = 0; j <= rsetValidValue.RecordCount - 1; j++)
                    {
                        try
                        {
                            oComboBox.ValidValues.Add(rsetValidValue.Fields.Item(0).Value, rsetValidValue.Fields.Item(1).Value);
                            rsetValidValue.MoveNext();
                        }
                        catch (Exception ex)
                        {
                            //  MsgBox(ex)
                        }

                    }
                }
                else
                {
                    oComboBox.ValidValues.Add("", "");
                    oComboBox.Select("", BoSearchKey.psk_ByValue);

                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
            }

        }


        public static void SetComboBoxValueDefineNew(SAPbouiCOM.ComboBox oComboBox, string strQry)
        {
            try
            {
                SAPbobsCOM.Recordset rsetValidValue = Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                Clear_Combo(oComboBox);
                rsetValidValue.DoQuery(strQry);
                rsetValidValue.MoveFirst();

                if (rsetValidValue.RecordCount > 0)
                {

                    oComboBox.ValidValues.Add("", "");
                    for (int j = 0; j <= rsetValidValue.RecordCount - 1; j++)
                    {
                        try
                        {
                            oComboBox.ValidValues.Add(rsetValidValue.Fields.Item(0).Value, rsetValidValue.Fields.Item(1).Value);
                            rsetValidValue.MoveNext();
                        }
                        catch (Exception ex)
                        {
                            //  MsgBox(ex)
                        }

                    }

                    oComboBox.ValidValues.Add("Define New", "Define New");
                }
                else
                {
                    oComboBox.ValidValues.Add("", "");
                    oComboBox.ValidValues.Add("Define New", "Define New");
                    oComboBox.Select("", BoSearchKey.psk_ByValue);

                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
            }

        }
        public static void setComboBoxValue(SAPbouiCOM.ComboBox oComboBox, string strQry)
        {
            try
            {
                if (oComboBox.ValidValues.Count == 0)
                {
                    SAPbobsCOM.Recordset rsetValidValue = DoQuery(strQry);
                    rsetValidValue.MoveFirst();
                    for (int j = 0; j <= rsetValidValue.RecordCount - 1; j++)
                    {
                        try
                        {
                            string val = rsetValidValue.Fields.Item(0).Value;
                            string desc = rsetValidValue.Fields.Item(1).Value;
                            rsetValidValue.MoveNext();
                            oComboBox.ValidValues.Add(val, desc);
                        }
                        catch (Exception ex)
                        {
                        }

                    }
                }
                // If oComboBox.ValidValues.Count > 0 Then oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index)
            }
            catch (Exception ex)
            {

            }
            finally
            {
            }

        }

        public static void NoObjectTableOpen(SAPbouiCOM.ComboBox oComboBox, string tablename)
        {
            try
            {
                string DefineNew = oComboBox.Selected.Value;
                if (DefineNew == "Define New")
                {

                    int i = 0;
                    string udoname = null;
                    for (i = 0; i <= Global.oApplication.Menus.Item("51200").SubMenus.Count - 1; i++)
                    {
                        udoname = Global.oApplication.Menus.Item("51200").SubMenus.Item(i).String;
                        if ((udoname.IndexOf(" - ")) != -1)
                        {
                            udoname = udoname.Remove(udoname.IndexOf(" - "));
                        }
                        if (udoname == tablename)
                        {
                            Global.oApplication.Menus.Item("51200").SubMenus.Item(i).Activate();
                            string DN_Str_Reason_FormId = Global.oApplication.Forms.ActiveForm.TypeEx;
                            //string DN_bl_Reason_FormId = true;
                            break; // TODO: might not be correct. Was : Exit For
                        }
                    }
                    //oComboBox.Select("", SAPbouiCOM.BoComboDisplayType.cdt_Value);
                    oComboBox.Select("", BoSearchKey.psk_ByValue);
                }
            }
            catch (Exception ex)
            {

                // Global.oApplication.SetStatusBarMessage(""+ex.ToString()+"", SAPbouiCOM.BoMessageTime.bmt_Short, true);

            }
        }

        public static void NoObjectTableOpenMatrix(SAPbouiCOM.ComboBox oComboBox, string tablename)
        {
            try
            {
                //string DefineNew = oComboBox.Selected.Value;
                //if (DefineNew == "Define New")
                //{

                int i = 0;
                string udoname = null;
                for (i = 0; i <= Global.oApplication.Menus.Item("51200").SubMenus.Count - 1; i++)
                {
                    udoname = Global.oApplication.Menus.Item("51200").SubMenus.Item(i).String;
                    if ((udoname.IndexOf(" - ")) != -1)
                    {
                        udoname = udoname.Remove(udoname.IndexOf(" - "));
                    }
                    if (udoname == tablename)
                    {
                        Global.oApplication.Menus.Item("51200").SubMenus.Item(i).Activate();
                        string DN_Str_Reason_FormId = Global.oApplication.Forms.ActiveForm.TypeEx;
                        //string DN_bl_Reason_FormId = true;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
                //oComboBox.Select("", SAPbouiCOM.BoComboDisplayType.cdt_Value);
                oComboBox.Select("", BoSearchKey.psk_ByValue);
                // }
            }
            catch (Exception ex)
            {

                // Global.oApplication.SetStatusBarMessage(""+ex.ToString()+"", SAPbouiCOM.BoMessageTime.bmt_Short, true);

            }
        }
        public static void StandardTableOpen(SAPbouiCOM.ComboBox oComboBox, string tablename)
        {
            try
            {
                string DefineNew = oComboBox.Selected.Value;
                if (DefineNew == "Define New")
                {
                    Global.oApplication.ActivateMenuItem("11265");


                    //Global.oApplication.Menus.Item("705").SubMenus.Item(i).Activate();
                    //string DN_Str_Reason_FormId = Global.oApplication.Forms.ActiveForm.TypeEx;
                    oComboBox.Select("", BoSearchKey.psk_ByValue);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public static void setNoObjectComboBoxValue(SAPbouiCOM.ComboBox oComboBox, string strQry)
        {

            try
            {
                //If oComboBox.ValidValues.Count = 0 Then
                SAPbobsCOM.Recordset rsetValidValue = DoQuery(strQry);
                rsetValidValue.MoveFirst();
                for (int j = 0; j <= rsetValidValue.RecordCount - 1; j++)
                {
                    oComboBox.ValidValues.Add(rsetValidValue.Fields.Item(0).Value, rsetValidValue.Fields.Item(1).Value);
                    //  MsgBox(rsetValidValue.Fields.Item(1).Value)

                    rsetValidValue.MoveNext();
                }
                // End If
                // If oComboBox.ValidValues.Count > 0 Then oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index)
            }
            catch (Exception ex)
            {
                Global.oApplication.StatusBar.SetText("Combo box load Error" + ex.ToString() + "" + Global.oCompany.GetLastErrorDescription(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);

            }
            finally
            {
            }

        }


        #endregion

        #region
        public static void GeoCode(string address)
        {
            //to Read the Stream
            StreamReader sr = null;

            //The Google Maps API Either return JSON or XML. We are using XML Here
            //Saving the url of the Google API 
            string url = String.Format("http://maps.googleapis.com/maps/api/geocode/xml?address=" +
            address + "&sensor=false");

            //to Send the request to Web Client 
            WebClient wc = new WebClient();
            try
            {
                sr = new StreamReader(wc.OpenRead(url));
            }
            catch (Exception ex)
            {
                Global.oApplication.SetStatusBarMessage("" + ex.ToString() + "", SAPbouiCOM.BoMessageTime.bmt_Short, true);
            }

            try
            {
                XmlTextReader xmlReader = new XmlTextReader(sr);
                bool latread = false;
                bool longread = false;

                while (xmlReader.Read())
                {
                    xmlReader.MoveToElement();
                    switch (xmlReader.Name)
                    {
                        case "lat":

                            if (!latread)
                            {
                                xmlReader.Read();
                                Global.Latitude = xmlReader.Value.ToString();
                                latread = true;

                            }
                            break;
                        case "lng":
                            if (!longread)
                            {
                                xmlReader.Read();
                                Global.Longitude = xmlReader.Value.ToString();
                                longread = true;
                            }

                            break;
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception("An Error Occured" + ex.Message);
            }
        }
        #endregion

    }
}
