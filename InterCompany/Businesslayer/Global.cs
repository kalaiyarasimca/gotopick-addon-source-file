﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrderTracking.BussinessLayer
{
    class Global
    {
        public static SAPbobsCOM.Company oCompany;
        public static SAPbobsCOM.Company oCompany1;
        public static SAPbobsCOM.Company oCompany2;
        public static SAPbobsCOM.Company oCompany3;
        public static SAPbouiCOM.Application oApplication;
        public static string _strCon = "";
        public static int count = 0;
        public static string oServerType;
        public static int v_RetVal;
        public static String addonName;
        public static string Longitude;
        public static string Latitude;
        public static bool bubblevalue = true;
        public static string MrpOkMode = "";
        public static string AddonVersion = "1";
        public static string SAPConnectionString = "";
        public static string IntegrationConnectionString = "";
        public static string DBType = "";
        public static int PPType = 0;


      //  public static string OITM = "ItemCode;ItemName;UgpEntry;ItmsGrpCod;ItemType;BuyUnitMsr;NumInBuy;SalUnitMsr;NumInSale;InvntryUom;GLMethod;ReorderQty;MinLevel;MixLevel;InvntItem;SellItem;PrchseItem;FirmCode;SWW;ManBtchNum;MngMethod;ItemClass;GSTRelevnt;Excisable;MatType;ChapterID;GstTaxCtg;NotifyASN;ProAssNum;frozenFor;frozenTo;frozenFrom;validFor;validTo;validFrom;EvalSystem;ByWh;BHeight1;BWidth1;BLength1;BVolume;BVolUnit;BWeight1;U_PLTime";
        public static string OITM = "ItemCode;ItemName;UgpEntry;ItmsGrpCod;BuyUnitMsr;SalUnitMsr;InvntryUom;InvntItem;SellItem;PrchseItem;ManBtchNum;MngMethod;frozenFor;frozenTo;frozenFrom;validFor;validTo;validFrom";
        //public static string OCRD = "CardCode;CardName;CardType;CardFName;GroupCode;Currency;Phone1;Phone2;Cellular;Fax;E_Mail;IntrntSite;validFor;validTo;validFrom;frozenFor;frozenTo;frozenFrom;CntctPrsn;GroupNum;IntrstRate;DebPayAcct;DpmIntAct;DflAccount;DatevAcct;CreateDate;U_Source;U_SSource;U_Type;U_Budget;U_RStatus;ShipToDef;BillToDef;WTLiable;CrtfcateNo;ExpireDate;NINum;AccCritria;TypWTReprt;WTTaxCat;ThreshOver;SurOver;ConCerti"; //
        public static string OCRD = "CardCode;CardName;CardType;CardFName;GroupCode;Phone1;Phone2;Cellular;Fax;E_Mail;IntrntSite;validFor;validTo;validFrom;frozenFor;frozenTo;frozenFrom;DebPayAcct;CreateDate;ShipToDef;BillToDef"; //DpmIntAct;DflAccount;
        public static string OCPR = "CardCode;Name;FirstName;MiddleName;LastName;Title;Position;Address;Tel1;Tel2;Cellolar;Fax;E_MailL;EmlGrpCode;Pager;Notes1;Notes2;Password;BirthPlace;BirthDate;Profession;BirthCity;BlockComm;Active";
        public static string CRD1 = "CardCode;AdresType;Address;Street;Block;ZipCode;City;County;Country;State;Address2;Address3;StreetNo;Building;TaxOffice;LineNum";
        public static string CRD7 = "CardCode;Address;AddrType;taxId0;taxId1;taxId2;taxId3;taxId4;taxId5;taxId6;taxId7;taxId8;taxId9;taxId10;taxId11;taxId12;taxId13;ECCNo;CERegNo;CERange;CEDivis;CEComRate"; //;CardCode;Address;AddrType;
        public static string CRD71 = "CardCode;Address;AddrType;taxId0;taxId1;taxId2;taxId3;taxId4;taxId5;taxId6;taxId7;taxId8;taxId9;taxId10;taxId11;taxId12;taxId13;ECCNo;CERegNo;CERange;CEDivis;CEComRate"; //;
        public static string OACT = "AcctCode;AcctName;FatherNum;Finanse;LocManTran"; // ;Postable;Fixed;Protected;AcctCurr;;LocManTran;RevalMatch;Groups;ValidFor;validTo;validFrom;frozenFor;frozenTo;frozenFrom

    }
}
