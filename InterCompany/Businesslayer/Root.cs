﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OrderTracking.BussinessLayer;
using OrderTracking.ApplicationLayer;

namespace OrderTracking.Bussiness_Layer
{
    class Root
    {

        public SAPbouiCOM.EventFilters oFilters;
        public SAPbouiCOM.EventFilter oFilter;
        public SAPbouiCOM.Form frmItemMaster;
        public SAPbouiCOM.Form frmBPMaster;
        public SAPbouiCOM.Form frmSalesReturn;
        public SAPbouiCOM.Form frmActiveform;
        public SAPbouiCOM.Form frmMRP;
        public SAPbouiCOM.Form frmPurchaseOrder;
        public SAPbouiCOM.Form frmSalesOrder;
        public SAPbouiCOM.Form frmDelivery;
         public SAPbouiCOM.Form frmInvoice;
        private static Root instance;
        public static Root Instance
        {
            get
            {
                if (instance == null) instance = new Root();
                return instance;
            }
        }
        public Root()
        {
            //Global.oApplication.StatusBar.SetText("1", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);

            GFun.ApplicationSetUp();
            //Global.oApplication.StatusBar.SetText("2", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);

            GFun.ConnectCompany();
            //Global.oApplication.StatusBar.SetText("3", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);

            TableCreation Table = new TableCreation();
            Table.CreationDetails();

            //Global.oApplication.StatusBar.SetText("OUT", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
            //  GFun.LoadXMLForm("Menu.xml");

            Global g = new Global();
            Global.oApplication.MenuEvent += new SAPbouiCOM._IApplicationEvents_MenuEventEventHandler(SBO_Application_MenuEvent);
            Global.oApplication.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
            Global.oApplication.FormDataEvent += new SAPbouiCOM._IApplicationEvents_FormDataEventEventHandler(SBO_Application_FormDataEvent);
            Global.oApplication.AppEvent += new SAPbouiCOM._IApplicationEvents_AppEventEventHandler(SBO_Application_AppEvent);
            SetFilters();
        }
        private void SBO_Application_AppEvent(SAPbouiCOM.BoAppEventTypes EventType)
        {

            switch (EventType)
            {
                case SAPbouiCOM.BoAppEventTypes.aet_ShutDown:
                    System.Windows.Forms.Application.Exit();
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_CompanyChanged:
                    System.Windows.Forms.Application.Exit();
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_LanguageChanged:
                    System.Windows.Forms.Application.Exit();
                    break;
            }
        }

        private void SBO_Application_MenuEvent(ref SAPbouiCOM.MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                frmActiveform = Global.oApplication.Forms.ActiveForm;           
            }
            catch (Exception ex)
            {
               // ex.ToString();
            }

        }
        private void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            try
            {
                frmActiveform = Global.oApplication.Forms.ActiveForm;
            }
            catch (Exception ex)
            {

            }
            BubbleEvent = true;

            if (pVal.FormTypeEx == "139")
            {
                Global.bubblevalue = SalesOrder.Instance.ItemEvent(pVal);
                BubbleEvent = Global.bubblevalue;
            }           
        }


        private void SBO_Application_FormDataEvent(ref SAPbouiCOM.BusinessObjectInfo BusinessObjectInfo, out bool BubbleEvent)
        {
            BubbleEvent = true;

            //try
            //{
            //    if (BusinessObjectInfo.FormTypeEx == "139")
            //        //  Global.bubblevalue = true;
            //        Global.bubblevalue = SalesOrder.Instance.FormDataEvent(BusinessObjectInfo, BubbleEvent);
            //}       

            BubbleEvent = Global.bubblevalue;
        }

        private void SetFilters()
        {

            // Create a new EventFilters object
            oFilters = new SAPbouiCOM.EventFilters();
            // add an event type to the container
            // this method returns an EventFilter object

            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_MENU_CLICK);          

            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE);
           

            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD);
            oFilter.AddEx("139");
           

            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_LOAD);
            oFilter.AddEx("139");
            
            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_CLICK);
            // assign the form type on which the event would be processed
            oFilter.AddEx("139");

            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);
            // assign the form type on which the event would be processed
            oFilter.AddEx("139");

            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST);
            // assign the form type on which the event would be processed

            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_LOST_FOCUS);
            // assign the form type on which the event would be processed

            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT);

            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD);
            
            Global.oApplication.SetFilter(oFilters);

        }
    }
}
