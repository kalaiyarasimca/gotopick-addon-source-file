﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OrderTracking.BussinessLayer;
using OrderTracking.Bussiness_Layer;
namespace OrderTracking.ApplicationLayer
{
    class SalesOrder
    {

        private static SalesOrder instance;
        SAPbouiCOM.Item oItem = default(SAPbouiCOM.Item);
        SAPbouiCOM.StaticText Ostatic;
        SAPbouiCOM.ComboBox OComboBox;
        SAPbouiCOM.Button oButton;
        SAPbouiCOM.EditText OEditText;
        public static SalesOrder Instance
        {
            get
            {
                if (instance == null) instance = new SalesOrder();

                return instance;
            }
        }
        public void LoadForm()
        {
            try
            {

                Root.Instance.frmSalesOrder = Global.oApplication.Forms.GetForm("139", 1);
                
            }
            catch (Exception ex)
            {

            }
            finally
            {
                Root.Instance.frmSalesOrder.Freeze(false);
            }



            // Root.Instance.frmSalesOrder.Freeze(false);
        }
        internal bool ItemEvent(SAPbouiCOM.ItemEvent pVal)
        {
            bool Bubblevalue = true;
            try
            {
                Root.Instance.frmActiveform = Global.oApplication.Forms.ActiveForm;
                switch ((pVal.EventType))
                {
                    case SAPbouiCOM.BoEventTypes.et_FORM_LOAD:
                        if (pVal.Before_Action == false)
                        {
                            Global.oApplication.StatusBar.SetText("Order IN", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);

                            Root.Instance.frmSalesOrder = Global.oApplication.Forms.GetForm("139", pVal.FormTypeCount);

                            //Global.oApplication.StatusBar.SetText(Root.Instance.frmSalesOrder.Title, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);

                            if (Root.Instance.frmSalesOrder.Title == "Sales Order")
                            {
                                Global.oApplication.StatusBar.SetText("Order IN1", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);

                                oItem = Root.Instance.frmSalesOrder.Items.Add("GoToPick", SAPbouiCOM.BoFormItemTypes.it_BUTTON);
                                oItem.Top = Root.Instance.frmSalesOrder.Items.Item("2").Top;
                                oItem.Left = Root.Instance.frmSalesOrder.Items.Item("2").Left + 65;
                                oItem.Height = Root.Instance.frmSalesOrder.Items.Item("2").Height;
                                oItem.Width = Root.Instance.frmSalesOrder.Items.Item("2").Width + 7;
                                oItem.Visible = true;
                                oButton = (SAPbouiCOM.Button)oItem.Specific;
                                oButton.Caption = "Go To Pick";

                                //Global.oApplication.StatusBar.SetText("Order IN", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
                            }

                        }
                        break;

                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        if (pVal.Before_Action == false & pVal.ItemUID == "GoToPick")
                        {
                            Root.Instance.frmSalesOrder = Global.oApplication.Forms.ActiveForm;
                            if (Root.Instance.frmSalesOrder.Mode == SAPbouiCOM.BoFormMode.fm_OK_MODE)
                            {
                                string DocNum = Root.Instance.frmSalesOrder.Items.Item("8").Specific.value;
                                string Series = Root.Instance.frmSalesOrder.Items.Item("88").Specific.value;
                                string NumAtCard = Root.Instance.frmSalesOrder.Items.Item("14").Specific.value;

                                SAPbobsCOM.Recordset RSORDR;
                                RSORDR = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                string ORDQry = "";
                                try
                                {
                                    if (Global.oServerType == "dst_HANADB")
                                    {
                                        ORDQry = "select IFNULL(r.\"U_EA_GTPWHS\",'') \"EA_GTPWHS\",r.\"LineNum\",r.\"ItemCode\",r.\"Quantity\",IFNULL(\"U_EA_GTP\",0) \"GTP\",IFNULL(\"U_EA_PQ\",0) \"PQ\", (IFNULL(\"U_EA_GTP\",0) + IFNULL(\"U_EA_PQ\",0)) \"SumQty\" ,o.\"DocEntry\" from \"ORDR\" o ";
                                        ORDQry = ORDQry + "inner join \"RDR1\" r on r.\"DocEntry\"=o.\"DocEntry\" ";
                                        ORDQry = ORDQry + "where  r.\"Quantity\" > (IFNULL(\"U_EA_GTP\",0) + IFNULL(\"U_EA_PQ\",0)) and \"U_IsPick\"='Y' and r.\"LineStatus\"='O' and \"WddStatus\"<>'W'  and \"CANCELED\"='N' and o.\"DocNum\"=" + DocNum;
                                    }
                                    else
                                    {
                                        ORDQry = "select isnull(r.U_EA_GTPWHS,'') EA_GTPWHS,r.LineNum,r.ItemCode,r.Quantity,isnull(U_EA_GTP,0) GTP,isnull(U_EA_PQ,0) PQ, (isnull(U_EA_GTP,0) + isnull(U_EA_PQ,0)) SumQty,o.DocEntry from ORDR o ";
                                        ORDQry = ORDQry + "inner join RDR1 r on r.DocEntry=o.DocEntry ";
                                        ORDQry = ORDQry + "where  r.Quantity > (isnull(U_EA_GTP,0) + isnull(U_EA_PQ,0)) and U_IsPick='Y'  and r.LineStatus='O' and WddStatus<>'W' and CANCELED='N' and o.DocNum=" + DocNum;
                                    }

                                    //Global.oApplication.StatusBar.SetText("Query RDR", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
                                    //Global.oApplication.StatusBar.SetText(ORDQry, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);

                                    RSORDR.DoQuery(ORDQry);
                                    if (RSORDR.RecordCount > 0)
                                    {
                                        //Global.oApplication.StatusBar.SetText("RDR Result", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
                                        for (int i = 0; i <= RSORDR.RecordCount - 1; i++)
                                        {

                                            string ItemCode = RSORDR.Fields.Item("ItemCode").Value.ToString();
                                            double SumQty = 0, OrdQuantity = 0,Quantity=0;
                                            string LineNum = RSORDR.Fields.Item("LineNum").Value.ToString();
                                            string DocEntry = RSORDR.Fields.Item("DocEntry").Value.ToString();
                                            string GOTOPWHS = RSORDR.Fields.Item("EA_GTPWHS").Value.ToString();

                                            Quantity = 0;

                                            SumQty = double.Parse(RSORDR.Fields.Item("SumQty").Value.ToString());
                                            OrdQuantity = double.Parse(RSORDR.Fields.Item("Quantity").Value.ToString());
                                            Quantity =OrdQuantity-SumQty;

                                            SAPbobsCOM.Recordset RSStock = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                            string StockQry = "";   

                                            if (Global.oServerType == "dst_HANADB")
                                            {
                                                StockQry = "select \"ItemCode\",\"OnHand\",\"OverAll\",(\"OnHand\"-\"OverAll\") \"Balance\" from ( ";
                                                StockQry = StockQry + "select \"ItemCode\",\"OnHand\",(select IFNULL(sum(IFNULL(cast(b.\"U_EA_GTP\" as integer),0)),0) \"SumQty\" from \"RDR1\" b inner join \"ORDR\" a on a.\"DocEntry\"=b.\"DocEntry\" where b.\"LineStatus\"='O' and \"WddStatus\"<>'W' and a.\"CANCELED\"='N' and b.\"ItemCode\"='" + ItemCode + "' ) \"OverAll\" from \"OITW\" ";
                                                StockQry = StockQry + "where \"ItemCode\"='" + ItemCode + "' and \"WhsCode\"='" + GOTOPWHS + "' ";
                                                StockQry = StockQry + ") \"OBJ\" where \"OnHand\" - \"OverAll\" >0";

                                            }
                                            else
                                            {
                                                StockQry = "select ItemCode,OnHand,OverAll,(OnHand-OverAll) Balance from ( ";
                                                StockQry = StockQry + "select ItemCode,OnHand,(select ISNULL(sum(isnull(b.U_EA_GTP,0) ),0) SumQty from RDR1 b inner join ORDR a on a.DocEntry=b.DocEntry where and b.LineStatus='O' and WddStatus<>'W' and a.CANCELED='N' and b.ItemCode='" + ItemCode + "' ) OverAll from OITW ";
                                                StockQry = StockQry + "where ItemCode='" + ItemCode + "' and WhsCode='" + GOTOPWHS + "' ";
                                                StockQry = StockQry + ") OBJ where OnHand - OverAll >0";
                                            }

                                            Global.oApplication.StatusBar.SetText(StockQry, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);

                                            //Global.oApplication.StatusBar.SetText("Query Stock", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);

                                            RSStock.DoQuery(StockQry);
                                            if (RSStock.RecordCount > 0)
                                            {
                                                for (int j = 0; j <= RSStock.RecordCount - 1; j++)
                                                {
                                                    double Balance = 0,UpdateQty=0;
                                                    Balance = double.Parse(RSStock.Fields.Item("Balance").Value.ToString());

                                                    string UpdateQry="";
                                                    
                                                    if (Balance >= Quantity)
                                                    {
                                                        UpdateQty = Math.Floor(Quantity);
                                                    }
                                                    else
                                                    {
                                                        UpdateQty = Math.Floor(Balance);
                                                    }

                                                    if (Global.oServerType == "dst_HANADB")
                                                    {
                                                        UpdateQry = "Update \"RDR1\" set \"U_EA_GTP\"= IFNULL(\"U_EA_GTP\",'0') + " + UpdateQty + " WHERE \"ItemCode\"='"+ItemCode+"' and \"DocEntry\"='" + DocEntry + "' and \"LineNum\"='" + LineNum + "'";
                                                    }
                                                    else
                                                    {
                                                        UpdateQry = "Update RDR1 set U_EA_GTP= isnull(U_EA_GTP,'0') + " + UpdateQty + " WHERE ItemCode='" + ItemCode + "' and DocEntry='" + DocEntry + "' and LineNum='" + LineNum + "'";
                                                    }

                                                    //Global.oApplication.StatusBar.SetText(UpdateQry, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);

                                                    //Global.oApplication.StatusBar.SetText("Query Update", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);

                                                    if (UpdateQry != "")
                                                    {
                                                        SAPbobsCOM.Recordset RSUpdate = (SAPbobsCOM.Recordset)Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                                        RSUpdate.DoQuery(UpdateQry);
                                                        RSUpdate = null;
                                                        Global.oApplication.StatusBar.SetText("Successfully Updated", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                                                    }


                                                    RSStock.MoveNext();
                                                }
                                            }
                                            RSStock = null;




                                            RSORDR.MoveNext(); 
                                        }
                                    }
                                    RSORDR = null;
                                }
                                catch (Exception ex)
                                {
                                    Global.oApplication.StatusBar.SetText("Go To Pick Update Failed" + ex.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
                                    Bubblevalue = false;
                                }
                            }
                        }
                        break;

                    default:
                        break;
                }

            }
            catch (Exception)
            {
            }

            return Bubblevalue;

        }

        internal bool MenuEvent(SAPbouiCOM.MenuEvent pVal, bool BubbleEvent)
        {
            bool Bubblevalue = true;
            try
            {

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return Bubblevalue;
        }

        internal bool FormDataEvent(SAPbouiCOM.BusinessObjectInfo BusinessObjectInfo, bool BubbleEvent)
        {
            bool Bubblevalue = true;
            try
            {
                switch (BusinessObjectInfo.EventType)
                {

                    case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD:
                        if (BusinessObjectInfo.BeforeAction == false)
                        {
                            try
                            {


                            }


                            catch (Exception)
                            {
                                return false;
                            }
                        }
                        break;

                    default:
                        break;
                }

            }
            catch (Exception)
            {
            }

            return Bubblevalue;
        }

    }

}
